const express = require('express')
const router = express.Router()
const multer = require('multer')
const upload = multer({ storage: multer.memoryStorage() })
const apicache = require("apicache").options({
    headers: {
      'cache-control': 'no-cache',
    },
    respectCacheControl: true
}).middleware

// Require controllers
const loginController = require('./controllers/login.controller')
const emailController = require('./controllers/email.controller')
const smsController = require('./controllers/sms.controller')
const usuariosController = require('./controllers/usuarios.controller')
const lovController = require('./controllers/lov.controller')
const rolesController = require('./controllers/roles.controller')
const utilController = require('./controllers/util.controller')
const promocionesController = require('./controllers/promociones.controller')
const maquinasController = require('./controllers/maquinas.controller')
const chatController = require('./controllers/chat.controller')
const archivosController = require('./controllers/archivos.controller')
const ConfiguracionController = require('./controllers/configuracion.controller')
const pagoController = require('./controllers/pago.controller')
const metodoPagoController = require('./controllers/metodosPago.controller')
const facturasController = require('./controllers/facturas.controller')

// Require middlewares
const securityMiddleware = require('./middlewares/security.middleware')

// loginController - Metodos
router.post('/login', loginController.login)
router.post('/registro', loginController.registro)
router.post('/restablecimiento', loginController.restablecimiento)

// emailController - Metodos
router.post('/email', securityMiddleware.guard, emailController.email)
router.post('/email/verificacion', securityMiddleware.guard, emailController.verificacion)

// smsController - Metodos
router.post('/sms/verificacion', securityMiddleware.guard, smsController.verificacion)
router.post('/sms/confirmacion', securityMiddleware.guard, smsController.confirmacion)

// usuariosController - Metodos
router.get('/usuario/', securityMiddleware.guard, usuariosController.getUsuario)
router.get('/usuario/:id', securityMiddleware.guard, usuariosController.getUsuario)
router.post('/usuario/:id', securityMiddleware.guard, usuariosController.postUsuario)
router.delete('/usuario/:id', securityMiddleware.guard, usuariosController.deleteUsuario)

// lovController - Metodos
router.get('/lov/:tipo', securityMiddleware.guard, lovController.lovPorTipo)

// rolesController - Metodos
router.get('/rol', securityMiddleware.guard, rolesController.getRoles)

// utilController - Metodos
router.get('/util/timestamp', securityMiddleware.guard, utilController.getTimestamp)
router.post('/util/contacto', securityMiddleware.guard, utilController.postMensajeContacto)

// promocionesController - Metodos
router.get('/maquinas/promociones/:criterio', securityMiddleware.guard, promocionesController.getPromociones)
router.put('/maquinas/promociones/:id', securityMiddleware.guard, promocionesController.putPromocion)
router.delete('/maquinas/promociones/:id', securityMiddleware.guard, promocionesController.deletePromocion)
router.post('/maquinas/promociones', securityMiddleware.guard, promocionesController.postPromocion)

// Maquinas - Metodos
router.get('/maquinas/galeria', securityMiddleware.guard, maquinasController.getMaquinasGaleria)
router.get('/maquinas/categorias', securityMiddleware.guard, maquinasController.getCategoria)
router.get('/maquinas/categorias/:categoria', securityMiddleware.guard, maquinasController.getMaquinasCategoria)
router.get('/maquinas/disponibilidad/:id', securityMiddleware.guard, maquinasController.getDisponibilidadMaquina)
router.get('/maquinas/entradas-salidas/:id', securityMiddleware.guard, maquinasController.getEntradasSalidas)
router.post('/maquinas/entradas-salidas', securityMiddleware.guard, maquinasController.postEntradaSalida)
router.post('/maquinas/rentas', securityMiddleware.guard, maquinasController.postRenta)
router.post('/maquinas/', securityMiddleware.guard, maquinasController.postMaquina)
router.get('/maquinas/:id', securityMiddleware.guard, maquinasController.getMaquinas)
router.put('/maquinas/delete/:id', securityMiddleware.guard, maquinasController.estatusMaquina)
router.put('/maquinas/:id', securityMiddleware.guard, maquinasController.updateMaquina)
router.get('/maquinas/rentas/:id', securityMiddleware.guard, maquinasController.getRentas)
router.get('/maquinas/categorias/rentas/:idCategoria', securityMiddleware.guard, maquinasController.getRentasXCategoria)

// Chat - Metodos
router.get('/chat/mensajes/:id', securityMiddleware.guard, chatController.getConversacionMensajes)
router.post('/chat/mensajes', securityMiddleware.guard, chatController.postConversacionMensaje)
router.post('/chat', securityMiddleware.guard, chatController.postConversacion)
router.get('/chat/historial/:id', securityMiddleware.guard, chatController.getConversaciones)
router.get('/chat/:id', securityMiddleware.guard, chatController.getConversacion)
router.get('/chat/hash/:id', securityMiddleware.guard, chatController.getConversacionHash)
router.get('/chat/alertas/:rolClave', securityMiddleware.guard, apicache('5 minutes'), chatController.getAlertas)
router.post('/chat/alertas', securityMiddleware.guard, chatController.postAlerta)
router.get('/chat/alertas/notificaciones/count', securityMiddleware.guard, apicache('5 minutes'), chatController.countNotificaciones)

// Archivo - metodos
router.post('/archivo', upload.single('file'), archivosController.subeArchivo)
router.get('/archivo/:id', archivosController.getArchivo)

// Configuracion - metodos
router.get('/configuracion/:clave', ConfiguracionController.getConfiguracion)

// Motor de pagos - metodos
router.post('/motor/pago', securityMiddleware.guard, pagoController.calculaPago)
router.post('/motor/pago/stripe', securityMiddleware.guard, pagoController.pagoStripe)
router.post('/motor/pago/stripe/confirma', securityMiddleware.guard, pagoController.confirmaPagoStripe)
router.post('/motor/pago/paypal/confirma', securityMiddleware.guard, pagoController.confirmaPagoPaypal)

// Metodos de pago - metodos
router.post('/metodoPago', securityMiddleware.guard, metodoPagoController.postPaymentMethod)
router.get('/metodoPago/:customerKey', securityMiddleware.guard, metodoPagoController.listPaymentMethods)
router.get('/metodoPago/retrieve/:id', securityMiddleware.guard, metodoPagoController.retrievePaymentMethod)
router.post('/metodoPago/update/:id', securityMiddleware.guard, metodoPagoController.updatePaymentMethod)
router.delete('/metodoPago/detach/:id', securityMiddleware.guard, metodoPagoController.detachPaymentMethod)

// Facturas - metodos
router.post('/facturacion', securityMiddleware.guard, upload.array('archivos', 2), facturasController.postFacturas)
router.get('/facturacion/:idUsuario', securityMiddleware.guard, facturasController.getFacturas)

module.exports = router