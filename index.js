const app = require('./app');
const puerto = process.env.PORT || 80;
const ws = require('./websockets');

// Iniciar servidor
const server = app.listen(puerto, () => {
  console.log(`RSI API iniciado en puerto ${puerto} version ${process.env.APP_VERSION}`);
});

// Inicializar escucha para websockets
// con la ruta /websockets
ws.useWebsocketsOnExpress(server);