const ConfiguracionModule = require("../model/modules/configuracion.module")

exports.getConfiguracion = async(req, res) => {
    const modulo = new ConfiguracionModule();
    const clave = req.params.clave;
  
    if (!clave) {
        res.statusCode = 400
        res.send('Bad request');
        return;
    } else if (!modulo) {
        res.statusCode = 500
        res.send('Ha ocurrido un error interno, por favor contacte al administrador.');
        return;
    }
  
    const resultado = await modulo.getConfiguracion(clave);
  
    if (resultado == null) {
        res.statusCode = 500
        res.send('No se han encontrado resultados');
        return;
    }
  
    res.send(resultado)
}