
const pagosService = require('../services/pagos.service');

exports.calculaPago = async (req, res) => {
    const entidad = req.body;

    // Verificar consistencia de parametros y de modulo
    if (!entidad || !entidad.ID_MAQUINA) {
        res.statusCode = 400
        res.send('Bad request');
        return;
    }

    // Procesa con modulo el request
    const resultado = await pagosService.calculaPago(entidad);

    // Verifica resultado
    if (!resultado) {
        res.send('Verifica que las promociones sean validas o contacta al administrador del sistema.');
        return;
    }

    // Retorna resultado de busqueda
    res.send(resultado)
}

exports.pagoStripe = async (req, res) => {
    const body = req.body;

    // Verificar consistencia de parametros y de modulo
    if (!body) {
        res.statusCode = 400
        res.send('Bad request');
        return;
    }

    const token = body.token;
    const idCustomer = body.idCustomer;
    const idPaymentMethod = body.idPaymentMethod;
    const entidad = body.entidad;

    if (!entidad) {
        res.statusCode = 400
        res.send('Bad request');
        return;
    }

    // Procesa con modulo el request
    const resultado = await pagosService.pagaStripe(token, idCustomer, idPaymentMethod, entidad);

    // Retorna resultado de busqueda
    res.send(resultado)
}

exports.confirmaPagoStripe = async (req, res) => {
    const body = req.body;

    // Verificar consistencia de parametros y de modulo
    if (!body) {
        res.statusCode = 400
        res.send('Bad request');
        return;
    }

    const id = body.id;
    const entidad = body.entidad;

    if (!id || !entidad) {
        res.statusCode = 400
        res.send('Bad request');
        return;
    }

    // Procesa con modulo el request
    const resultado = await pagosService.confirmaPagoStripe(id, entidad);

    // Retorna resultado de busqueda
    res.send(resultado)
}

exports.confirmaPagoPaypal = async (req, res) => {
    const body = req.body;

    // Verificar consistencia de parametros y de modulo
    if (!body) {
        res.statusCode = 400
        res.send('Bad request');
        return;
    }

    const entidad = body.entidad;

    if (!entidad) {
        res.statusCode = 400
        res.send('Bad request');
        return;
    }

    // Procesa con modulo el request
    const resultado = await pagosService.confirmaPagoPaypal(entidad);

    // Retorna resultado de busqueda
    res.send(resultado)
}