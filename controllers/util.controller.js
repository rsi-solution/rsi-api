const UtilModule = require('../model/modules/util.module')
const { RespuestaApi } = require('../shared/interfaces/respuesta-api.interface')

exports.getTimestamp = async (req, res) => {
    const module = new UtilModule()
    const resultado = await module.getTimestamp()

    if (!resultado) res.statusCode = 500

    res.send(resultado)
}

exports.postMensajeContacto = async (req, res) => {
    const modulo = new UtilModule();
    let resultado = new RespuestaApi()
    let resultadoPersistencia = null
    const entidad = req.body

    if (entidad) {
        resultadoPersistencia = await modulo.postMensajeContacto(entidad)

        if (!resultadoPersistencia) {
            resultado.estatus = 'ERROR'
            resultado.mensaje = 'Error al insertar el mensaje de contacto, contacte al administrador'
        }
    } else {
        resultado.estatus = 'ERROR'
        resultado.mensaje = 'Bad request'
        res.statusCode = 400
    }

    res.send(resultado)
}