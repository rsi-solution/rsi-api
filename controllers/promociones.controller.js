const PromocionesModule = require('../model/modules/promociones.module')
const { RespuestaApi } = require('../shared/interfaces/respuesta-api.interface')

exports.getPromociones = async (req, res) => {
    const modulo = new PromocionesModule()
    const criterio = req.params.criterio
    const page = (req.query.page) ? parseInt(req.query.page) : 1

    // Verificar consistencia de parametros y de modulo
    if (!criterio) {
        res.statusCode = 400
        res.send('Bad request');
        return;
    } else if (!modulo) {
        res.statusCode = 500
        res.send('Ha ocurrido un error interno, por favor contacte al administrador.');
        return;
    }

    // Procesa con modulo el request
    const resultado = await modulo.getPromociones(criterio, page);

    // Verifica resultado
    if (resultado == null) {
        res.statusCode = 500
        res.send('Ha ocurrido un error interno, por favor contacte al administrador.');
        return;
    }

    res.send(resultado)
}

exports.putPromocion = async (req, res) => {
    const modulo = new PromocionesModule()
    let resultado = new RespuestaApi()
    let resultadoPersistencia = false
    const entidad = req.body
    const id = req.params.id

    if (entidad && id) {
        resultadoPersistencia = await modulo.putPromocion(entidad, id)

        if (!resultadoPersistencia) {
            resultado.estatus = 'ERROR'
            resultado.mensaje = 'Error al actualizar la promocion, contacte al administrador'
            res.statusCode = 500
        }
    } else {
        resultado.estatus = 'ERROR'
        resultado.mensaje = 'Bad request'
        res.statusCode = 400
    }

    res.send(resultado)
}

exports.deletePromocion = async (req, res) => {
    const modulo = new PromocionesModule()
    let resultado = new RespuestaApi()
    let resultadoPersistencia = false
    const id = req.params.id

    if (id) {
        resultadoPersistencia = await modulo.deletePromocion(id)

        if (!resultadoPersistencia) {
            resultado.estatus = 'ERROR'
            resultado.mensaje = 'Error al actualizar la promocion, contacte al administrador'
            res.statusCode = 500
        }
    } else {
        resultado.estatus = 'ERROR'
        resultado.mensaje = 'Bad request'
        res.statusCode = 400
    }

    res.send(resultado)
}

exports.postPromocion = async (req, res) => {
    const modulo = new PromocionesModule()
    let resultado = new RespuestaApi()
    let resultadoPersistencia = null
    const entidad = req.body

    if (entidad) {
        resultadoPersistencia = await modulo.postPromocion(entidad)

        if (!resultadoPersistencia) {
            resultado.estatus = 'ERROR'
            resultado.mensaje = 'Error al insertar la promocion, contacte al administrador'
            res.statusCode = 500
        } else {
            resultado.mensaje = resultadoPersistencia;
        }
    } else {
        resultado.estatus = 'ERROR'
        resultado.mensaje = 'Bad request'
        res.statusCode = 400
    }

    res.send(resultado)
}