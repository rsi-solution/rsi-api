const RolesModule = require("../model/modules/roles.module")

exports.getRoles = async (req, res) => {
    const module = new RolesModule()
    const resultado = await module.getRoles()

    res.send(resultado)
}