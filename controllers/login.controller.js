const jwt = require("jsonwebtoken");
const sha256 = require("js-sha256");

const accessService = require("../services/access.service");
const incidentService = require("../services/incident.service");
const {
  RespuestaApi,
} = require("../shared/interfaces/respuesta-api.interface");
const { log } = require("../services/logger.service");

const LoginModule = require("../model/modules/login.module");

exports.login = async (req, res) => {
  let respuesta = {
    estatus: "OK",
    mensaje: null,
    token: null,
    info: {}
  };

  try {
    if (!req.body) {
      res.statusCode = 400;
      res.send();
      return;
    }

    const body = req.body;

    const email = body.email;
    const llave = body.llave;
    const fcmToken = body.fcm_token;

    // Validar combinaciones
    if (email && llave) {
      // Hashear password
      const hashedPassword = sha256(llave);
      const loginModule = new LoginModule();

      // Verifica usuario y contrase;a en persistencia
      const resultadoPersistencia = await loginModule.loginUsuario(
        email,
        hashedPassword,
        fcmToken
      );

      // Genera token si todo es correcto
      if (resultadoPersistencia) {
        const payload = resultadoPersistencia;
        const key = process.env.JWT_SECRET_KEY;

        const token = jwt.sign(
          payload,
          key,
          {
            expiresIn: '3 days',
          }
        );

        respuesta.token = token;
        respuesta.info = resultadoPersistencia;

      } else {
        respuesta.estatus = "ERROR";
        respuesta.mensaje = "El usuario o contraseña es incorrecto";

        log.log("info", `Acceso incorrecto: ${JSON.stringify(body)}`);
      }
    } else {

      respuesta.estatus = "ERROR";
      respuesta.mensaje = "El correo electronico y contraseña son obligatorios.";
    }
  } catch (error) {
    respuesta.estatus = "ERROR";
    respuesta.mensaje = "Error al iniciar sesion, contacte al administrador";
    incidentService.nuevoIncidente(
      101002,
      "login.controller.js - login",
      error
    );
    res.statusCode = 500
  }

  accessService.nuevoAcceso(req, res);
  res.send(respuesta);
};

exports.registro = async (req, res) => {
  let respuesta = new RespuestaApi();
  const loginModule = new LoginModule();
  let body = req.body;

  if (!body) {
    res.statusCode = 400;
    res.send();
    return;
  }

  if (body.llave) body.llave = sha256(body.llave);

  const resultado = await loginModule.registroUsuario(body);

  if (resultado) {
    respuesta.estatus = "ERROR";
    respuesta.mensaje = resultado;
  }

  res.send(respuesta);
};

exports.restablecimiento = async (req, res) => {
  let respuesta = new RespuestaApi();
  const body = req.body;
  const loginModule = new LoginModule();

  try {
    if (!body) {
      res.statusCode = 400;
      res.send();
      return;
    }

    let usuario = body.usuario;
    let nuevaLlave = body.nuevaLlave;

    // Validar combinaciones
    if (usuario && nuevaLlave) {
      // Hashear passwords
      if (usuario.llave) usuario.llave = sha256(usuario.llave);
      if (nuevaLlave) nuevaLlave = sha256(nuevaLlave);

      // Restablecer password en persistencia
      const resultadoPersistencia = await loginModule.restableceLlave(
        usuario,
        nuevaLlave
      );

      if (!resultadoPersistencia) {
        respuesta.estatus = "ERROR";
        respuesta.mensaje =
          "Por favor verifique que los datos sean correctos";
      }
    } else {

      respuesta.estatus = "ERROR";
      respuesta.mensaje = "El usuario y la contrase;a nueva son obligatorias.";
    }
  } catch (error) {
    respuesta.estatus = "ERROR";
    respuesta.mensaje =
      "Error al restablecer al usuario, contacte al administrador";
    incidentService.nuevoIncidente(
      101003,
      "login.controller.js - restablecimiento",
      error
    );
    res.statusCode = 500
  }

  res.send(respuesta);
};
