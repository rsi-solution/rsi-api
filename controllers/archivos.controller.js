const {UploadFileStrategyManager} = require('../shared/classes/upload-file-strategy-manager')
const sharp = require('sharp');

const IMAGE_MIME_TYPES = [
    'image/jpeg', 'image/png', 'image/webp'
];

exports.subeArchivo = async (req, res) => {
    let respuesta = {
        estatus: 'OK',
        mensaje: 'Tu archivo se ha cargado éxitosamente',
        url: null,
        filename: null
    };
    let file = req.file;
    const mimetype = req.body.mimetype;
    const randomname = Math.random().toString(16).substr(2, 8);

    if (file && mimetype) {
        if (isMimeTypeImage(mimetype)) {
            await compressImage(file);
        }

        const strategyManager = new UploadFileStrategyManager();
        const strategy = strategyManager.getS3StorageStrategy();

        const entidad = await strategy.subeArchivo(file, randomname, mimetype, req, true);

        if (entidad) {
            respuesta.url = entidad.url;
            respuesta.filename = entidad.filename;
        } else {
            respuesta.estatus = 'ERROR';
            respuesta.mensaje = 'Error interno al subir el archivo. Contacte al administrador.';
        }
    } else {
        respuesta.estatus = 'ERROR';
        respuesta.mensaje = 'Falta el archivo a subir'
        res.statusCode = 400;
    }

    res.send(respuesta);
}

exports.getArchivo = async (req, res) => {
    const strategyManager = new UploadFileStrategyManager();
    const strategy = strategyManager.getDatabaseUploadStrategy();

    const id = req.params.id;
    let respuesta;
    let mimetype = 'text/plain';

    if (id) {
        const resultado = await strategy.getArchivo(id);

        if (resultado) {
            respuesta = resultado.file;
            mimetype = resultado.mimetype;
        }
    } else {
        res.statusCode = 400;
    }

    res.contentType(mimetype);
    res.send(respuesta);
}

const isMimeTypeImage = mimetype => {
    return IMAGE_MIME_TYPES.includes(mimetype.toLowerCase());
}

const compressImage = async file => {
    const newBuffer = await sharp(file.buffer)
        .jpeg({ quality: 60, progressive: true })
        .withMetadata()
        .toBuffer();

    file.buffer = newBuffer;
}