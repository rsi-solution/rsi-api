const { RespuestaApi } = require('../shared/interfaces/respuesta-api.interface')

exports.verificacion = (req, res) => {
  const respuesta = new RespuestaApi();

  const body = req.body;
  const telefono = body.telefono;

  smsService
    .suscribir(telefono)
    .then((resultado) => {
      respuesta.mensaje = resultado;
      res.send(respuesta);
    })
    .catch((error) => {
      respuesta.estatus = "ERROR";
      respuesta.mensaje = error;
      res.send(respuesta);
    });
};

exports.confirmacion = (req, res) => {
  const respuesta = new RespuestaApi();

  const body = req.body;
  const token = body.token;

  smsService
    .confirmarSuscripcion(token)
    .then((resultado) => {
      respuesta.mensaje = resultado;
      res.send(respuesta);
    })
    .catch((error) => {
      respuesta.estatus = "ERROR";
      respuesta.mensaje = error;
      res.send(respuesta);
    });
};
