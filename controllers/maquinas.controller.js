const MaquinasModule = require('../model/modules/maquinas.module');
const { RespuestaApiEntidad } = require('../shared/interfaces/respuesta-api-entidad.interface');
const { RespuestaApi } = require('../shared/interfaces/respuesta-api.interface');

exports.getMaquinas = async (req, res) => {
    const modulo = new MaquinasModule();
    const criterio = req.params.id // id de busqueda (id)
    const estatus = req.query.estatus
    const exactMatch = (req.query.exact === 'true')
    const page = (req.query.page) ? parseInt(req.query.page) : 1

    let resultado = [];

    if (!criterio) {
        res.statusCode = 400
        res.send('Bad request');
        return;
    } else if (!modulo) {
        res.statusCode = 500
        res.send('Ha ocurrido un error interno, por favor contacte al administrador.');
        return;
    }

    // Procesa con modulo el request
    resultado = await modulo.getMaquinas(criterio, estatus, exactMatch, page)

    // Verifica resultado
    if (resultado == null) { // Si no hay resultados
        res.statusCode = 500
        res.send('No se ha encontrado la maquinaria.');
        return;
    }

    // Retorna resultado de busqueda
    res.send(resultado)
}

exports.getDisponibilidadMaquina = async (req, res) => {
    const modulo = new MaquinasModule()
    const criterio = req.params.id

    // Verificar consistencia de parametros y de modulo
    if (!criterio) {
        res.statusCode = 400
        res.send('Bad request');
        return;
    } else if (!modulo) {
        res.statusCode = 500
        res.send('Ha ocurrido un error interno, por favor contacte al administrador.');
        return;
    }

    // Procesa con modulo el request
    const resultado = await modulo.getDisponibilidadMaquina(criterio)

    // Verifica resultado
    if (resultado == null) { // Si no hay resultados
        res.statusCode = 500
        res.send('No se ha encontrado la maquinaria.');
        return;
    }

    // Retorna resultado de busqueda
    res.send(resultado)
}

exports.getCategoria = async (req, res) => {
    const modulo = new MaquinasModule();

    if (!modulo) {
        res.statusCode = 500
        res.send('Ha ocurrido un error interno, por favor contacte al administrador.');
        return;
    }

    const resultado = await modulo.getCategoria();

    if (resultado == null) {
        res.statusCode = 500
        res.send('No hay categorias disponibles');
        return;
    }

    res.send(resultado)
}

exports.getMaquinasCategoria = async (req, res) => {
    const criterio = req.params.categoria
    const modulo = new MaquinasModule();
    const estatus = req.query.estatus
    const page = (req.query.page) ? parseInt(req.query.page) : 1

    if (!criterio) {
        res.statusCode = 400
        res.send('Bad request');
        return;
    } else if (!modulo) {
        res.statusCode = 500
        res.send('Ha ocurrido un error interno, por favor contacte al administrador.');
        return;
    }
    
    // Procesa con modulo el request
    const resultado = await modulo.getMaquinasCategoria(criterio, estatus, page)

    // Verifica resultado
    if (resultado == null) { // Si no hay resultados
        res.statusCode = 500
        res.send('No se ha encontrado la maquinaria.');
        return;
    }

    // Retorna resultado de busqueda
    res.send(resultado)
}

exports.getEntradasSalidas = async (req, res) => {
    const modulo = new MaquinasModule()
    const criterio = req.params.id
    const page = (req.query.page) ? parseInt(req.query.page) : 1

    // Verificar consistencia de parametros y de modulo
    if (!criterio) {
        res.statusCode = 400
        res.send('Bad request');
        return;
    } else if (!modulo) {
        res.statusCode = 500
        res.send('Ha ocurrido un error interno, por favor contacte al administrador.');
        return;
    }

    // Procesa con modulo el request
    const resultado = await modulo.getEntradasSalidas(criterio, page);

    // Verifica resultado
    if (resultado == null) {
        res.statusCode = 500
        res.send('Ha ocurrido un error interno, por favor contacte al administrador.');
        return;
    }

    res.send(resultado)
}

exports.postEntradaSalida = async (req, res) => {
    const modulo = new MaquinasModule()
    let resultado = new RespuestaApi()
    let resultadoPersistencia = null
    const entidad = req.body

    if (entidad) {
        resultado = await modulo.postEntradaSalida(entidad)
    } else {
        resultado.estatus = 'ERROR'
        resultado.mensaje = 'Bad request'
        res.statusCode = 400
    }

    res.send(resultado)
}

exports.getMaquinasGaleria = async (req, res) => {
    const modulo = new MaquinasModule();
    const page = (req.query.page) ? parseInt(req.query.page) : 1
    
    if (!modulo) {
        res.statusCode = 500
        res.send('Ha ocurrido un error interno, por favor contacte al administrador.');
        return;
    }
    
    const resultado = await modulo.getMaquinasGaleria(page);

    if (resultado == null) {
        res.statusCode = 500
        res.send('No hay imágenes disponibles');
        return;
    }
    
    res.send(resultado)
}

exports.postMaquina = async (req, res) => {
    const modulo = new MaquinasModule();
    let respuesta = new RespuestaApi()
    const entidad = req.body

    if(!entidad){
        res.statusCode = 400;
        res.send("Bad request");
        return;
    }

    const resultado = await modulo.postMaquina(entidad)

    if (resultado) {
        respuesta.estatus = "ERROR";
        respuesta.mensaje = resultado;
    }

    res.send(respuesta)
}

exports.estatusMaquina = async (req, res) => {
    const modulo = new MaquinasModule();
    let resultado = new RespuestaApi();
    let resultadoPersistencia = false
    const id = req.params.id
    const body = req.body

    if(id && body){
        resultadoPersistencia = await modulo.estatusMaquina(id, body)

        if (!resultadoPersistencia) {
            resultado.estatus = 'ERROR'
            resultado.mensaje = 'Error al eliminar la maquina, contacte al administrador'
        }
    } else {
        resultado.estatus = 'ERROR'
        resultado.mensaje = 'Bad request'
        res.statusCode = 400
    }

    res.send(resultado)
}

exports.updateMaquina = async(req, res) => {
    const modulo = new MaquinasModule();
    let respuesta = new RespuestaApi();
    let resultado = false;
    const id = req.params.id
    const body = req.body;

    if (body) {
        resultado = await modulo.updateMaquina(body, id)

        if (resultado) {
            respuesta.estatus = 'ERROR';
            respuesta.mensaje = resultado;
        }
    } else {
        res.statusCode = 400;
        respuesta.estatus = 'ERROR';
        respuesta.mensaje = 'Bad request';
    }

    res.send(respuesta)
}

exports.postRenta = async (req, res) => {
    const modulo = new MaquinasModule()
    let resultado = new RespuestaApiEntidad()
    const entidad = req.body

    if (entidad) {
        resultado = await modulo.postRenta(entidad)
    } else {
        resultado.estatus = 'ERROR'
        resultado.mensaje = 'Bad request'
        res.statusCode = 400
    }

    res.send(resultado)
}

exports.getRentas = async (req, res) => {
    const modulo = new MaquinasModule()
    const criterio = req.params.id
    const page = (req.query.page) ? parseInt(req.query.page) : 1
    const consolidaPagos = (req.query.consolidaPagos) ? req.query.consolidaPagos : false;
    const columnaCriterio = (req.query.columna) ? req.query.columna : 'USUARIO_ID';

    // Verificar consistencia de parametros y de modulo
    if (!criterio) {
        res.statusCode = 400
        res.send('Bad request');
        return;
    }

    // Procesa con modulo el request
    const resultado = await modulo.getRentas(criterio, consolidaPagos, columnaCriterio, page);

    // Verifica resultado
    if (resultado == null) {
        res.statusCode = 500
        res.send('Ha ocurrido un error interno, por favor contacte al administrador.');
        return;
    }

    res.send(resultado)
}

exports.getRentasXCategoria = async (req, res) => {
    const modulo = new MaquinasModule()

    const idCategoria = req.params.idCategoria
    const page = (req.query.page) ? parseInt(req.query.page) : 1
    const consolidaPagos = (req.query.consolidaPagos) ? req.query.consolidaPagos : false;
    const modo = (req.query.modo) ? req.query.modo : '0';

    // Verificar consistencia de parametros y de modulo
    if (!idCategoria) {
        res.statusCode = 400
        res.send('Bad request');
        return;
    }

    // Procesa con modulo el request
    const resultado = await modulo.getRentasXCategoria(idCategoria, modo, page, consolidaPagos)

    // Verifica resultado
    if (resultado == null) {
        res.statusCode = 500
        res.send('Ha ocurrido un error interno, por favor contacte al administrador.');
        return;
    }

    res.send(resultado)
}