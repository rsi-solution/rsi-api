const UsuariosModule = require('../model/modules/usuarios.module')
const { RespuestaApi } = require('../shared/interfaces/respuesta-api.interface')
const sha256 = require("js-sha256");

exports.getUsuario = async (req, res) => {
    const usuariosModule = new UsuariosModule()
    const id = req.params.id
    const exactMatch = (req.query.exact === 'true')
    const page = (req.query.page) ? parseInt(req.query.page) : 1
    const columna = (req.query.columna) ? req.query.columna : 'email'
    let resultado = []
    let comportamientoEspecial
    
    // Intenta parsear objeto de comportamiento especial
    try {
        if (req.query.cx)
            comportamientoEspecial = JSON.parse(req.query.cx);

        resultado = await usuariosModule.getUsuario(id, exactMatch, page, columna, comportamientoEspecial)

    } catch (error) {
        res.statusCode = 400;
        resultado = [];
    }

    res.send(resultado)
}

exports.postUsuario = async (req, res) => {
    const usuariosModule = new UsuariosModule()
    let resultado = new RespuestaApi()
    let resultadoPersistencia = false
    const body = req.body
    const id = req.params.id

    if (body && id) {
        resultadoPersistencia = await usuariosModule.updateUsuario(body, id)

        if (!resultadoPersistencia) {
            resultado.estatus = 'ERROR'
            resultado.mensaje = 'Error al actualizar el usuario, contacte al administrador'
        }
    } else {
        resultado.estatus = 'ERROR'
        resultado.mensaje = 'Bad request'
        res.statusCode = 400
    }

    res.send(resultado)
}

exports.deleteUsuario = async (req, res) => {
    const usuariosModule = new UsuariosModule()
    let resultado = new RespuestaApi()
    let resultadoPersistencia = false
    const id = req.params.id

    if (id) {
        resultadoPersistencia = await usuariosModule.deleteUsuario(id)

        if (!resultadoPersistencia) {
            resultado.estatus = 'ERROR'
            resultado.mensaje = 'Error al actualizar el usuario, contacte al administrador'
        }
    } else {
        resultado.estatus = 'ERROR'
        resultado.mensaje = 'Bad request'
        res.statusCode = 400
    }

    res.send(resultado)
}