const LovModule = require("../model/modules/lov.module");

exports.lovPorTipo = async (req, res) => {
  if (!req.params && !req.params.tipo) {
    res.statusCode = 400;
    res.send();
    return;
  }
  const tipo = req.params.tipo;

  const lovModule = new LovModule();
  let resultado = await lovModule.getLovPorTipo(tipo);

  res.send(resultado);
};