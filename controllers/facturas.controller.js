const ChatModule = require('../model/modules/chat.module');
const FacturasModule = require('../model/modules/facturas.module');
const MaquinasModule = require('../model/modules/maquinas.module');
const {UploadFileStrategyManager} = require('../shared/classes/upload-file-strategy-manager');
const { RespuestaApiEntidad } = require('../shared/interfaces/respuesta-api-entidad.interface');
const { RespuestaApi } = require('../shared/interfaces/respuesta-api.interface');

const MAX_FILE_SIZE = 1000000; // 2MB

/**
 *  Metodo encargado de validar que tengamos un archivo XML y uno PDF,
 * ademas de validar el tama;o de los archivos.
 * Asi como regresar los mismos.
 * 
 * @param {Blob[]} archivos Archivo xml y pdf
 * @param {number} idUsuario Identificador interno del usuario
 * @returns Archivo factura y xml validados
 */
const getArchivosValidados = (archivos = []) => {
    let resultado = new RespuestaApiEntidad();

    const archivoPdf = archivos.find( archivo => archivo.mimetype === 'application/pdf');
    const archivoXml = archivos.find( archivo => archivo.mimetype === 'application/xml' || archivo.mimetype === 'text/xml');

    if (archivoPdf && archivoXml) {

        if (archivoPdf.size <= MAX_FILE_SIZE && archivoXml.size <= MAX_FILE_SIZE) {

            resultado.estatus = 'OK';
            resultado.entidad = {
                pdf: archivoPdf,
                xml: archivoXml
            };

        } else {
            resultado.estatus = 'ERROR';
            resultado.mensaje = 'El tamaño de los archivos no puede ser mayor a 2MB';
        }
    } else {
        resultado.estatus = 'ERROR';
        resultado.mensaje = 'Se tiene que subir un archivo XML y PDF de la factura';
    }

    return resultado;
}

exports.postFacturas = async (req, res) => {
    let respuesta = new RespuestaApi();
    
    const archivos = req.files;
    const idUsuario = req.body.idUsuario;
    const idRenta = req.body.idRenta;

    if (archivos && archivos.length == 2 && idUsuario && idRenta) {

        // Valida y obtiene archivos PDF y XML validados
        const respuestaArchivosValidados = getArchivosValidados(archivos);

        if (respuestaArchivosValidados.estatus === 'OK' && respuestaArchivosValidados.entidad) {

            const facturasModule = new FacturasModule();
            const chatModule = new ChatModule();
            const maquinaModule = new MaquinasModule();
            const strategyManager = new UploadFileStrategyManager();
            const strategy = strategyManager.getS3StorageStrategy();

            let entidadFactura = {
                ID_USUARIO: parseInt(idUsuario),
                ID_ARCHIVO_PDF: -99,
                ID_ARCHIVO_XML: -99,
                ID_RENTA: parseInt(idRenta),
                ESTATUS: 'AC'
            };

            // Inicializa cliente
            await strategy.extraeCliente(true);
    
            // Sube archivo PDF
            let file = respuestaArchivosValidados.entidad.pdf;
            let entidadArchivo = await strategy.subeArchivo(file, file.originalname, file.mimetype, req, false);
    
            if (entidadArchivo && entidadArchivo.id) {
                // Complementa factura
                entidadFactura.ID_ARCHIVO_PDF = entidadArchivo.id;

                // Sube archivo XML
                file = respuestaArchivosValidados.entidad.xml;
                entidadArchivo = await strategy.subeArchivo(file, file.originalname, file.mimetype, req, false);
        
                if (entidadArchivo && entidadArchivo.id) {
                    // Complementa factura
                    entidadFactura.ID_ARCHIVO_XML = entidadArchivo.id;

                    // Sube factura
                    const resultadoPersistenciaFactura = await facturasModule.postFactura(entidadFactura, strategy.client, false);

                    if (resultadoPersistenciaFactura.estatus === 'OK') {
                        respuesta.estatus = 'OK';
                        respuesta.mensaje = 'Esta factura se verá reflejada en el perfil del usuario.';
                    } else {
                        respuesta.estatus = resultadoPersistenciaFactura.estatus;
                        respuesta.mensaje = 'Error al subir la factura. Intentalo nuevamente o contacta a soporte.';
                    }
                    
                } else {
                    respuesta.estatus = 'ERROR';
                    respuesta.mensaje = 'Error al subir archivo XML. Intentalo nuevamente o contacta a soporte.';
                }
                
            } else {
                respuesta.estatus = 'ERROR';
                respuesta.mensaje = 'Error al subir archivo PDF. Intentalo nuevamente o contacta a soporte.';
            }
    
            // Procesar transaccion
            if (respuesta.estatus === 'OK')
                await strategy.commit();
            else
                await strategy.rollback();

            // Libera cliente
            await strategy.releaseClient(false);

        } else {
            respuesta.estatus = respuestaArchivosValidados.estatus;
            respuesta.mensaje = respuestaArchivosValidados.mensaje;
        }
    } else {
        respuesta.estatus = 'ERROR';
        respuesta.mensaje = 'Debe subir un archivo PDF y otro XML para la factura. Por favor verifique.'
    }

    res.send(respuesta);
}

exports.getFacturas = async (req, res) => {
    const modulo = new FacturasModule()
    const idUsuario = req.params.idUsuario
    const page = (req.query.page) ? parseInt(req.query.page) : 1
    let resultado = new RespuestaApiEntidad()

    if (idUsuario) {
        resultado = await modulo.getFacturas(idUsuario, page, true)
    } else {
        resultado.estatus = 'ERROR'
        resultado.mensaje = 'Ha ocurrido un error inesperado, contacte a soporte.'
    }

    res.send(resultado)
}