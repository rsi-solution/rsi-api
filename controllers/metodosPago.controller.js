const pagosService = require('../services/pagos.service');
const { RespuestaApi } = require('../shared/interfaces/respuesta-api.interface');

exports.postPaymentMethod = async(req, res) => {
    const body = req.body;

    const customer = body.customer;
    const token = body.token

   const resultado = await pagosService.postMetodoPago(customer, token)

   res.send(resultado.id);
}

exports.updatePaymentMethod = async(req, res) => {
   const idPaymentMethod = req.params.id;
   const body = req.body;

   const resultado = await pagosService.updatePaymentMethod(idPaymentMethod, body)

   res.send(resultado);
}

exports.detachPaymentMethod = async (req, res) => {
   let resultado = new RespuestaApi();

   const idPaymentMethod = req.params.id

   if(!idPaymentMethod){
      resultado.estatus = 'ERROR'
      resultado.mensaje = 'Bad request'
      res.statusCode = 400
   }
   
   result = await pagosService.detachPaymentMethod(idPaymentMethod);
   
   if(!result.id){
      resultado.estatus = 'ERROR'
      resultado.mensaje = 'No se ha podido eliminar el método de pago'

   }

   res.send(resultado);
}

exports.retrievePaymentMethod = async(req, res) => {
   const idPaymentMethod = req.params.id;

   res.send( await pagosService.retrievePaymentMethod(idPaymentMethod) );
}

exports.listPaymentMethods = async(req, res) => {
   const customerKey = req.params.customerKey

   if(!customerKey){
      res.statusCode = 400
      res.send('Bad request');
      return;
   }

   const resultado = await pagosService.listPaymentMethods(customerKey);

   if (resultado == null) {
      res.statusCode = 500
      res.send('No se han encontrado resultados');
      return;
  }

   res.send(resultado)
}