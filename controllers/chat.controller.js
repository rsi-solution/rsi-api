const ChatModule = require('../model/modules/chat.module')
const { RespuestaApi } = require('../shared/interfaces/respuesta-api.interface')
const apicache = require('apicache').options({
    headers: {
      'cache-control': 'no-cache',
    },
    respectCacheControl: true
});
const UsuariosModule = require('../model/modules/usuarios.module');
const {log} = require('../services/logger.service');
const firebaseService = require('../services/firebase.service');

exports.getConversacionMensajes = async (req, res) => {
    const modulo = new ChatModule()
    const id = req.params.id
    const hash = (req.query.hash) ? req.query.hash : null
    const page = (req.query.page) ? parseInt(req.query.page) : 1

    // Verificar consistencia de parametros y de modulo
    if (!id) {
        res.statusCode = 400
        res.send('Bad request');
        return;
    } else if (!modulo) {
        res.statusCode = 500
        res.send('Ha ocurrido un error interno, por favor contacte al administrador.');
        return;
    }

    // Procesa con modulo el request
    const resultado = await modulo.getConversacionMensajes(id, page, hash);

    // Verifica resultado
    if (resultado == null) {
        res.statusCode = 500
        res.send('Ha ocurrido un error interno, por favor contacte al administrador.');
        return;
    }

    res.send(resultado)
}

exports.getConversaciones = async (req, res) => {
    const modulo = new ChatModule()
    const id = req.params.id
    const page = (req.query.page) ? parseInt(req.query.page) : 1

    // Verificar consistencia de parametros y de modulo
    if (!id) {
        res.statusCode = 400
        res.send('Bad request');
        return;
    } else if (!modulo) {
        res.statusCode = 500
        res.send('Ha ocurrido un error interno, por favor contacte al administrador.');
        return;
    }

    // Procesa con modulo el request
    const resultado = await modulo.getConversaciones(id, page)

    // Verifica resultado
    if (resultado == null) {
        res.statusCode = 500
        res.send('Ha ocurrido un error interno, por favor contacte al administrador.');
        return;
    }

    res.send(resultado)
}

exports.getConversacionHash = async (req, res) => {
    const modulo = new ChatModule()
    const id = req.params.id

    // Verificar consistencia de parametros y de modulo
    if (!id) {
        res.statusCode = 400
        res.send('Bad request');
        return;
    } else if (!modulo) {
        res.statusCode = 500
        res.send('Ha ocurrido un error interno, por favor contacte al administrador.');
        return;
    }

    // Procesa con modulo el request
    const resultado = await modulo.getConversacionHash(id);

    // Verifica resultado
    if (resultado == null) {
        res.statusCode = 500
        res.send('Ha ocurrido un error interno, por favor contacte al administrador.');
        return;
    }

    res.send(resultado)
}

exports.getConversacion = async (req, res) => {
    const modulo = new ChatModule()
    const id = req.params.id

    // Verificar consistencia de parametros y de modulo
    if (!id) {
        res.statusCode = 400
        res.send('Bad request');
        return;
    } else if (!modulo) {
        res.statusCode = 500
        res.send('Ha ocurrido un error interno, por favor contacte al administrador.');
        return;
    }

    // Procesa con modulo el request
    const resultado = await modulo.getConversacion(id);

    // Verifica resultado
    if (resultado == null) {
        res.statusCode = 500
        res.send('Ha ocurrido un error interno, por favor contacte al administrador.');
        return;
    }

    res.send(resultado)
}

exports.postConversacion = async (req, res) => {
    const modulo = new ChatModule()
    let resultado = new RespuestaApi()
    let resultadoPersistencia = false
    const entidad = req.body

    if (entidad) {
        resultadoPersistencia = await modulo.postConversacion(entidad)

        if (!resultadoPersistencia) {
            resultado.estatus = 'ERROR'
            resultado.mensaje = 'Error al actualizar la promocion, contacte al administrador'
            res.statusCode = 500
        } else {
            resultado.mensaje = resultadoPersistencia
        }
    } else {
        resultado.estatus = 'ERROR'
        resultado.mensaje = 'Bad request'
        res.statusCode = 400
    }

    res.send(resultado)
}

exports.postConversacionMensaje = async (req, res) => {
    const modulo = new ChatModule()
    let resultado = new RespuestaApi()
    let resultadoPersistencia = false
    const entidad = req.body

    if (entidad) {
        resultadoPersistencia = await modulo.postConversacionMensaje(entidad)

        if (!resultadoPersistencia) {
            resultado.estatus = 'ERROR'
            resultado.mensaje = 'Error al actualizar la promocion, contacte al administrador'
            res.statusCode = 500
        }
    } else {
        resultado.estatus = 'ERROR'
        resultado.mensaje = 'Bad request'
        res.statusCode = 400
    }

    res.send(resultado)
}

exports.countNotificaciones = async (req, res) => {
    const modulo = new ChatModule()

    // Verificar consistencia de parametros y de modulo
    if (!modulo) {
        res.statusCode = 500
        res.send('Ha ocurrido un error interno, por favor contacte al administrador.');
        return;
    }

    // Procesa con modulo el request
    const resultado = await modulo.countNotificaciones();
    
    // Verifica resultado
    if (resultado == null) {
        res.statusCode = 500
        res.send('Ha ocurrido un error interno, por favor contacte al administrador.');
        return;
    }

    res.send(resultado)
}

exports.getAlertas = async (req, res) => {
    req.apicacheGroup = 'alertas';

    const modulo = new ChatModule()
    const rolClave = (req.params.rolClave) ? req.params.rolClave : null
    const page = (req.query.page) ? parseInt(req.query.page) : 1
    const modo = (req.query.modo) ? req.query.modo : 'ALERTAS';

    // Verificar consistencia de parametros y de modulo
    if (!modulo) {
        res.statusCode = 500
        res.send('Ha ocurrido un error interno, por favor contacte al administrador.');
        return;
    }

    // Procesa con modulo el request
    const resultado = await modulo.getAlertas(rolClave, page, modo)

    // Verifica resultado
    if (resultado == null) {
        res.statusCode = 500
        res.send('Ha ocurrido un error interno, por favor contacte al administrador.');
        return;
    }

    res.send(resultado)
}

exports.postAlerta = async (req, res) => {
    const modulo = new ChatModule()
    let resultado = new RespuestaApi()
    let resultadoPersistencia = false
    const entidad = req.body

    if (entidad) {
        resultadoPersistencia = await modulo.postAlerta(entidad)

        if (!resultadoPersistencia) {
            resultado.estatus = 'ERROR'
            resultado.mensaje = 'Error al actualizar la alerta, contacte al administrador'
            res.statusCode = 500
        }

        apicache.clear('alertas');

        notificaAlerta(entidad.ROL_CLAVE, entidad.CONTENIDO);
    } else {
        resultado.estatus = 'ERROR'
        resultado.mensaje = 'Bad request'
        res.statusCode = 400
    }

    res.send(resultado)
}

const notificaAlerta = async (rol, contenido) => {
    console.log('notificando', rol, contenido);

    let fcmTokens = [];
    const module = new UsuariosModule();

    if (rol === 'CLIENTE' || rol === 'EMP') {
        fcmTokens.push(await module.getUsuariosFcmTokensByRole(rol, true));
    } else if (rol === 'ADMIN' || !rol) {
        fcmTokens.push(await module.getUsuariosFcmTokensByRole('CLIENTE', false));
        fcmTokens.push(await module.getUsuariosFcmTokensByRole('EMP', false));
        await module.client.release(false);
    } else {
        log.error(`Rol invalido para notificar ${rol} => ${contenido}`);
        return;
    }

    // Quita arrays vacios
    fcmTokens = fcmTokens.filter(fcmToken => fcmToken !== null);

    if (fcmTokens.length > 0) {
        log.info(`Notificando a ${fcmTokens.length} usuarios por alerta: ${contenido.substring(0, 20)}...`)

        const payload = {
            data: {
                tipo: "NEW_ALERT"
            },
            notification: {
                body: `Nueva alerta`,
                title: "RSI"
            }
        };

        fcmTokens.forEach(fcmToken => firebaseService.enviaPushNotificationFCM(fcmToken, payload));
    } else {
        log.info(`No hay FCM tokens para notificar la alerta: ${contenido.substring(0, 20)}...`)
    }
}