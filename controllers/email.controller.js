const emailService = require('../services/email.service')
const { RespuestaApi } = require('../shared/interfaces/respuesta-api.interface')

exports.email = (req, res) => {
  const respuesta = new RespuestaApi();

  const body = req.body;
  const to = body.to;
  const content = body.content;
  const subject = body.subject;

  emailService
    .enviaEmail(to, "jorgeefernandezz@gmail.com", subject, content)
    .then((resultado) => {
      respuesta.mensaje = resultado;
      res.send(respuesta);
    })
    .catch((error) => {
      respuesta.estatus = "ERROR";
      respuesta.mensaje = error;
      res.send(respuesta);
    });
};

exports.verificacion = (req, res) => {
  const respuesta = new RespuestaApi();

  const body = req.body;
  const email = body.email;

  emailService
    .verificaEmail(email)
    .then((resultado) => {
      respuesta.mensaje = resultado;
      res.send(respuesta);
    })
    .catch((error) => {
      respuesta.estatus = "ERROR";
      respuesta.mensaje = error;
      res.send(respuesta);
    });
};
