const accessService = require("../services/access.service");
const securityValidatorFactory = require("./factory/security-validator-factory");

exports.guard = async (req, res, next) => {
  const token = req.headers.authorization;

  if (token) {
    accessService.nuevoAcceso(req, res);

    const validator = securityValidatorFactory.buildSecurityValidator(req, res, token)

    // Comprueba resultado de validacion de seguridad
    if (await validator.validate() === true) {
      next();
    } else {
      res.statusCode = 403;
      accessService.nuevoAcceso(req, res);
      res.send("Forbidden");
    }
  } else {
    res.statusCode = 400;
    accessService.nuevoAcceso(req, res);
    res.send("Bad request");
  }
};