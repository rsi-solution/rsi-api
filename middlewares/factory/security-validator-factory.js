const JwtValidator = require("./jwt-validator");
const HashedLinkValidator = require("./hashed-link-validator");

exports.buildSecurityValidator = (req, res, token) => {
    if (req.params.id && req.url.includes('/maquinas/' + req.params.id)) {
        return new HashedLinkValidator(token, [req.params.id]);
    } else {
        return new JwtValidator(token);
    }
}