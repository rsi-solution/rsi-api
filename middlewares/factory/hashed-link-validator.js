const { sha256 } = require("js-sha256");
const JwtValidator = require("./jwt-validator");

module.exports = class HashedLinkValidator {
    #token;
    #toHashStrings = [];

    constructor(token, toHashStrings) {
        this.#token = token;
        this.#toHashStrings = toHashStrings;
    }

    async validate() {
        return new Promise((resolve, reject) => {

          const key = process.env.APP_SECRET_KEY;
          const toHash = this.#toHashStrings.reduce((a, b) => a.concat(b)) + key;
          const hash = sha256(toHash);

          if (hash !== this.#token) {
            resolve(new JwtValidator(this.#token).validate());
          } else {
            resolve(true);
          }
        });
    }
}