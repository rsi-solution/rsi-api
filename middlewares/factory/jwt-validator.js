const jwt = require("jsonwebtoken");

module.exports = class JwtValidator {
    #token;

    constructor(token) {
        this.token = token;
    }

    async validate() {
        const key = process.env.JWT_SECRET_KEY;

        return new Promise((resolve, reject) => {
          jwt.verify(
            this.token,
            key,
            { algorithm: 'RS256' },
            (err, decoded) => {
        
              if (!err) {
                resolve(true);
              } else {
                resolve(false);
              }
            });
        });
    }
}