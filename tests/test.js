
/*
    NOTA: Si deseas ejecutar en otro ambiente de base de datos,
    solo cambia la URL de conexion desde package.json
    ATENCION: Para deployment por medio de pipeline de jira, tiene que ser:
    postgres://test_user:test_user_password@localhost:5432/pipelines
*/
const request = require('supertest');
const app = require('../app');
const assert = require('assert');
const {log} = require('../services/logger.service');
const {pool} = require('../db/pool');

log.setLevel('warn');

// Variables de estado
let tokenAdmin;
let idUsuarioAdmin = 0;
let idUsuarioCliente = 0;
let idConversacion;

describe ('Registro usuarios', async () => {

    it('con datos faltantes', async () => {
        const data = {
            telefono: "6141234567",
            nombre_completo: "Administracion RSI",
            foto_uri: null,
            estatus: "AC",
            created_by: "anon",
        };

        const response = await request(app).post('/v1/registro').send(data);
        assert.ok( response.body.estatus === 'ERROR' );
    });

    it('exitoso - rol ADMIN', async () => {
        const data = {
            email: "jx32@live.com.mx",
            telefono: "6141234567",
            nombre_completo: "Administracion RSI",
            llave: "jorge2100",
            foto_uri: null,
            estatus: "AC",
            created_by: "anon",
            roles: [{
                clave: 'ADMIN',
                descripcion: 'Administrador'
            }]
        };

        const response = await request(app).post('/v1/registro').send(data);
        assert.ok( response.body.estatus === 'OK' );
    });

    it('exitoso - rol CLIENTE', async () => {
        const data = {
            email: "contacto@ctu.com",
            telefono: "6141234555",
            nombre_completo: "CTU",
            llave: "jorge2100",
            foto_uri: null,
            estatus: "AC",
            created_by: "anon",
            roles: [{
                clave: 'CLIENTE',
                descripcion: 'Cliente'
            }]
        };

        const response = await request(app).post('/v1/registro').send(data);
        assert.ok( response.body.estatus === 'OK' );
    });

});

describe('Login', () => {

    it('email inexistente', async () => {
        const data = {
            email: 'xxxxx@test.com',
            llave: null
        };

        const response = await request(app).post('/v1/login').send(data);
        assert.ok( response.body.estatus === 'ERROR' );
    });

    it('password incorrecto', async () => {
        const data = {
            email: 'jx32@live.com.mx',
            llave: 'xxxxxxx'
        };

        const response = await request(app).post('/v1/login').send(data);
        assert.ok( response.body.estatus === 'ERROR' );
    });

    it('login correcto ADMIN', async () => {
        const data = {
            email: 'jx32@live.com.mx',
            llave: 'jorge2100'
        };

        const response = await request(app).post('/v1/login').send(data);

        try {

            if (response.body.info)
                idUsuarioAdmin = parseInt(response.body.info.id);
            else
                assert.fail('La informacion del usuario no existe');

            if (response.body.token)
                tokenAdmin = response.body.token;
            else
                assert.fail('No se ha devuelto ningun token');

        } catch (error) {
            assert.fail('La informacion del usuario no contiene un ID valido');
        }

        assert.ok( response.body.estatus === 'OK' &&
                    response.body.token &&
                    response.body.info );
    });

    it('login correcto CLIENTE', async () => {
        const data = {
            email: 'contacto@ctu.com',
            llave: 'jorge2100'
        };

        const response = await request(app).post('/v1/login').send(data);

        try {

            if (response.body.info)
                idUsuarioCliente = parseInt(response.body.info.id);
            else
                assert.fail('La informacion del usuario no existe');

        } catch (error) {
            assert.fail('La informacion del usuario no contiene un ID valido');
        }

        assert.ok( response.body.estatus === 'OK' &&
                    response.body.token &&
                    response.body.info );
    });

});

describe('Chat', () => {

    describe('Conversacion', () => {

        it('nueva conversacion', async () => {

            const data = {
                ID_USUARIO_CREADOR: idUsuarioAdmin,
                ID_USUARIO_RECEPTOR: idUsuarioCliente,
                ESTATUS: "AC"
            }

            const response = await request(app)
                                    .post('/v1/chat')
                                    .set('authorization', tokenAdmin)
                                    .send(data);

            idConversacion = response.body.mensaje;

            assert.ok( response.body.estatus === 'OK'
                        && response.body.mensaje );
        });

    });

    describe('Mensaje', () => {

        it('nuevo mensaje', async () => {

            const data = {
                ID_CONVERSACION: idConversacion,
                ID_USUARIO_CREADOR: idUsuarioAdmin,
                ID_USUARIO_RECEPTOR: idUsuarioCliente,
                TIPO_CONTENIDO: "TO",
                CONTENIDO: "Hello, nice to meet you CTU!",
                ESTATUS: "AC"
            }

            const response = await request(app)
                                    .post('/v1/chat/mensajes')
                                    .set('authorization', tokenAdmin)
                                    .send(data);

            assert.ok( response.body.estatus === 'OK' );
        });

        it('mensaje sin receptor', async () => {

            const data = {
                ID_CONVERSACION: idConversacion,
                ID_USUARIO_CREADOR: idUsuarioAdmin,
                TIPO_CONTENIDO: "TO",
                CONTENIDO: "Mensaje fallido!",
                ESTATUS: "AC"
            }

            const response = await request(app)
                                    .post('/v1/chat/mensajes')
                                    .set('authorization', tokenAdmin)
                                    .send(data);

            assert.ok( response.body.estatus === 'ERROR' );
        });

    });

});