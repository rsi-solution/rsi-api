// Requerir modulos
const express = require("express");
const cors = require("cors");
const swaggerJsDoc = require("swagger-jsdoc")
const swaggerUi = require("swagger-ui-express")
const compression = require("compression")

const swaggerOptions = {
  swaggerDefinition: {
    info: {
      "title": "RSI API",
      "description": "API de RSI",
      "contact": {
        "name": "RSI support",
        "url": "http://www.example.com/support",
        "email": "support@example.com"
      },
      "version": "1.0.0",
      "servers": [
        "http://localhost:3000"
      ]
    }
  },

  apis: ["v1Router.js"]
}

// Agregar .env a entorno
require("@dotenvx/dotenvx").config();

// Inicializar modulos express
const app = express();

// Inicializar swagger docs
const swaggerDocs = swaggerJsDoc(swaggerOptions)
app.use('/api', swaggerUi.serve, swaggerUi.setup(swaggerDocs, { explorer: true }))

// Requerir rutas
const v1Router = require('./v1Router')

// Usar rutas
app.use(express.json());
app.use(cors());
app.use(compression());

app.use('/v1', v1Router)

module.exports = app