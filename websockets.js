const WebSocket = require('ws');
const ChatModule = require('./model/modules/chat.module');
const incidentService = require("./services/incident.service");
const firebaseService = require("./services/firebase.service");
const { log } = require("./services/logger.service");
const queryString = require('query-string');
const UsuariosModule = require('./model/modules/usuarios.module');

const server = new WebSocket.Server({ noServer: true, path: '/websockets' });
let socketStore = {};

/*
  Se inicializa procesamiento de mensajes nuevos,
  interpretando la entidad enviada y reenviando el mensaje
  al socket receptor, asi como almacena en persistencia el mensaje.
*/
server.on('connection', (websocketConnection, connectionRequest) => {
    websocketConnection.on('message', async (message) => {
      
      try {
        const body = JSON.parse(message);

        if (body) {
          const idConversacion = body.ID_CONVERSACION;
          const idUsuarioRecipiente = body.ID_USUARIO_RECEPTOR;
          const tipoContenido = body.TIPO_CONTENIDO;
  
          // Verifica consistencia de informacion
          if (idConversacion && idUsuarioRecipiente && body.CONTENIDO && tipoContenido !== 'TY') {

            const chatModule = new ChatModule();
            const userModule = new UsuariosModule();

            // Persistencia mensaje
            const resultadoPersistencia = await chatModule.postConversacionMensaje(body);

            if (!resultadoPersistencia) {
              log.warn('El mensaje no se persistio en base de datos', JSON.stringify(body));
            }

            // Encontrar web-socket del usuario receptor
            const ws = socketStore[idConversacion][idUsuarioRecipiente];
        
            if (ws) {
              log.info('messaged -> ', idUsuarioRecipiente, ':', body.CONTENIDO);
              
              // Envia mensaje al socket del usuario recipiente
              ws.send(message.toString());
            }

            // Se envia push notification al usuario
            const { fcm_token, nombre_completo } = await userModule.getUsuarioXId(idUsuarioRecipiente);

            log.info('-> notif to ', idUsuarioRecipiente, ' with fcm_token ', fcm_token);

            if (fcm_token) {

              const payload = {
                data: {
                    tipo: "NEW_MESSAGE",
                    idConversacion: idConversacion.toString()
                },
                notification: {
                    body: `Mensaje de ${nombre_completo}`,
                    title: "RSI"
                }
              };

              await firebaseService.enviaPushNotificationFCM(fcm_token, payload);

            } else {
              // No recibe notificacion, debido a que no ha ingreado por primera vez
            }

            websocketConnection.send(JSON.stringify({
              estatus: 'OK'
            }));

          } else if (idConversacion && idUsuarioRecipiente  && tipoContenido === 'TY') {
            // Notifica al usuario recipiente que se encuentra escribiendo
            let socket = socketStore[idConversacion][idUsuarioRecipiente];

            if (socket) {
              socket.send(JSON.stringify({
                estatus: 'STATUS',
                mensaje: 'typing'
              }));
            }

          } else if (idConversacion && idUsuarioRecipiente  && tipoContenido === 'NT') {
            // Notifica al usuario recipiente que NO se encuentra escribiendo
            let socket = socketStore[idConversacion][idUsuarioRecipiente];

            if (socket) {
              socket.send(JSON.stringify({
                estatus: 'STATUS',
                mensaje: 'online'
              }));
            } else {
              websocketConnection.send(JSON.stringify({
                estatus: 'STATUS',
                mensaje: 'offline'
              }));
            }

          } else {
  
            websocketConnection.send(JSON.stringify({
              estatus: 'ERROR',
              mensaje: 'Error al enviar, intente nuevamente'
            }));
          }
        } else {
  
          websocketConnection.send(JSON.stringify({
            estatus: 'ERROR',
            mensaje: 'Error al enviar, intente nuevamente'
          }));
        }
      } catch (error) {
        
        incidentService.nuevoIncidente(
          102000,
          "websocket on message",
          "Error al enviar mensaje por socket: " + error
        );

        console.log(error)

        websocketConnection.send(JSON.stringify({
          estatus: 'ERROR',
          mensaje: 'Error al enviar, intente nuevamente más tarde'
        }));
      }

    });
});

/**
 *  Metodo encargado de escuchar conexiones y mensajes
 * de sockets, asi como almacenarlos en la store de sockets.
 * 
 * @param {*} expressServer Servidor iniciado de express
 */
const useWebsocketsOnExpress = (expressServer) => {

  expressServer.on('upgrade', (request, socket, head) => {
    server.handleUpgrade(request, socket, head, (websocket) => {

      const queryParams = queryString.parseUrl(request.url);
      const idConversacion = queryParams?.query?.idConversacion;
      const idUsuario = queryParams?.query?.idUsuario;
      const idUsuarioReceptor = queryParams?.query?.idUsuarioReceptor;

      if (queryParams && idConversacion && idUsuario && idUsuarioReceptor) {

        websocket.isAlive = true;
        websocket.on('pong', () => {
          websocket.isAlive = true;
        });
  
        server.emit('connection', websocket, request);

        /* Almacena en el store el websocket nuevo.
        Ejemplo de estructura de almacenamiento:
        {
          <ID_CONVERSACION>: {
            <ID_USUARIO_1>: { <web-socket> }
            <ID_USUARIO_2>: { <web-socket> }
          },
          ...
        }
        */
        if (!socketStore[idConversacion]) socketStore[idConversacion] = {};
        socketStore[idConversacion][idUsuario] = websocket;

        if (socketStore[idConversacion][idUsuarioReceptor]) {
          // Notifica a la contraparte de conexion
          let socket = socketStore[idConversacion][idUsuarioReceptor];
          socket.send(JSON.stringify({
            estatus: 'STATUS',
            mensaje: 'online'
          }));
          // Notifica al nuevo sujeto de la conexion
          websocket.send(JSON.stringify({
            estatus: 'STATUS',
            mensaje: 'online'
          }));
        } else {
          // Notifica al nuevo sujeto de la conexion
          websocket.send(JSON.stringify({
            estatus: 'STATUS',
            mensaje: 'offline'
          }));
        }
      } else {

        // Si existen parametros inconsistentes, cierra socket
        websocket.terminate();
      }
    });
  });

  // Verifica los sockets vivos
  setInterval(() => {

    if (socketStore) {
      const conversacionesKeys = Object.keys(socketStore);

      // Recorrer keys de conversaciones
      conversacionesKeys.forEach( conversacionKey => {
        // Obtener y recorrer keys de IDs de usuarios
        let usuariosIdKeys = Object.keys(socketStore[conversacionKey]);
        let notificaDesconexionAUsuarios = false;

        usuariosIdKeys.forEach( usuarioIdKey => {
          // Obtener socket del store
          let socket = socketStore[conversacionKey][usuarioIdKey];

          // Verifica si socket no esta vivo y se termina
          if (socket && socket.isAlive === false) {
            socket.terminate();
            delete socketStore[conversacionKey][usuarioIdKey];
            notificaDesconexionAUsuarios = true;

          } else if (socket && socket.isAlive === true) {

            // Se establece socket vivo como falso y se realiza ping
            // para que con el pong, se reviva autonomamente
            socket.isAlive = false;
            socket.ping(() => {});
          }
        });

        if (notificaDesconexionAUsuarios) {
          usuariosIdKeys = Object.keys(socketStore[conversacionKey]);

          usuariosIdKeys.forEach( usuarioIdKey => {

            let socket = socketStore[conversacionKey][usuarioIdKey];
            socket.send(JSON.stringify({
              estatus: 'STATUS',
              mensaje: 'offline'
            }));
          });
        }
      });
    }

  }, 1000);//5000);
}

exports.useWebsocketsOnExpress = useWebsocketsOnExpress;