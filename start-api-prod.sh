echo "[PRODUCTION] Attempting to update database"
cd BaseDeDatos
liquibase update
cd ..
echo "[PRODUCTION] Attempting to start API"
npm start -- --env-file=.env.production -- node index.js