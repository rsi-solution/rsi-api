# README #
RSI-API

## Pre-requisites
- Install Docker desktop (If planning to use a local database)
- Install NodeJS v20.11.0+
- Install Liquibase 4.26.0
- A AWS account

## Preparing the cloud services (AWS)
Working on it

## Configuring your environment
Working on it

## Installing dependencies
- *(Skip this step if your database is remote, not local)* Install the database performing `docker-compose compose up`, then you could start/stop the db container using a terminal or using Docker desktop
- Execute `./install.sh` (Linux/MacOS) or `./install.bat` (Windows using Powershell) to install the necessary dependencies
- That's It!, see Running the API section for next steps

## Database setup
- You have to put your database credentials into `url` in ./BaseDeDatos/liquibase.properties file
- Liquibase will update your database automatically on each app start based on the existing changelogs on BaseDeDatos folder

## Running the API
- Production:
  - Linux/MacOS: `./restart-service.sh`
    - *It will run the API as a service and pull the most recent changes from the repository*
- Local:
  - Linux/MacOS: `./start-api.sh`
  - Windows: `./start-api.bat`