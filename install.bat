echo WARNING: This installer assumes that you have node.js installed on your local machine!
node --version
npm --version

echo Installing global dependencies
npm install pm2 -g
npm install @dotenvx/dotenvx -g

echo Installing PM2 dependencies
pm2 install pm2-logrotate

echo Installing app dependencies
npm install

echo All set!