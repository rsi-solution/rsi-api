const incidentService = require("../../services/incident.service");
const {log} = require("../../services/logger.service");

const aws = require("aws-sdk");
const UtilModule = require("../../model/modules/util.module");
aws.config.update({
  region: process.env.AWS_REGION,
});
const s3 = new aws.S3({apiVersion: '2006-03-01'});

module.exports = class S3StorageStrategy {

    urlS3;
    utilService = new UtilModule();
    archivosBucket = [];

  /**
   *  Metodo que almacena un archivo en la base de datos
   * y regresa la entidad de archivo que incluye ya el ID y el URL
   * 
   * @param {*} file Archivo subido
   * @param {string} filename Nombre del archivo
   * @param {string} mimetype Mime-type del archivo
   * @param {*} req Request Http
   * @param {boolean} doCommit Realiza commit en BD
   * @returns Url del archivo
   */
   async subeArchivo(file, filename, mimetype, req, doCommit = true) {
    return new Promise( (resolve, reject) => {
        
        const uploadParams = {Bucket: process.env.S3_BUCKET, Key: filename, Body: file.buffer, ContentType: mimetype};

        s3.upload(uploadParams, async (err, data) => {
            // Procesa error en la subida del archivo
            if (err) {
                
                incidentService.nuevoIncidente(
                    -101000,
                    "s3.js - subeArchivo",
                    `${err}
                    ${filename}`
                );
    
                resolve(null);
            } else {
                // Obtiene URL del bucket
                const bucketUrl = data.Location;

                // Agrega archivo al array para luego ser proceso en caso de rollback
                this.archivosBucket.push(data);
                
                // Almacena archivo en base de datos
                const resultado = await this.utilService.subeArchivo(null, filename, mimetype, req, doCommit, bucketUrl);
                resolve(resultado);
            }
        });
    });
   }

   async commit() {
    await this.utilService.commit();
    this.archivosBucket = [];
   }

   async rollback() {
    await this.utilService.rollback();

    // Elimina objetos del bucket, ya que se deshara transaccion
    if (this.archivosBucket.length > 0) {
        let objetos = [];

        // Extrae llaves de todos los objetos
        for (let index = 0; index < this.archivosBucket.length; index++) {
            const archivo = this.archivosBucket[index];
            objetos.push ({ Key: archivo.Key });
        }

        // Genera las opciones con bucket y objetos a eliminar
        const options = {
            Bucket: this.archivosBucket[0].Bucket,
            Delete: {
                Objects: objetos,
                Quiet: false
            }
        };

        // Elimina objetos del bucket
        const resDeletion = await s3.deleteObjects(options).promise();

        log.info(`Se eliminaron ${resDeletion.Deleted.length}/${objetos.length} archivos del bucket S3 debido a Rollback.`);
    }
   }

   async releaseClient(destruyeCliente) {
    await this.utilService.releaseClient(destruyeCliente);
    this.archivosBucket = [];
   }

   async extraeCliente(iniciaTransaccion = false) {
    await this.utilService.extraeCliente(iniciaTransaccion);
   }

   get client() {
    return this.utilService.client;
   }

}