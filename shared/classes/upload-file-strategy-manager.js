const UtilModule = require('../../model/modules/util.module');
const S3StorageStrategy = require('./s3');

/**
 * Clase encargada de gestionar las estrategias posibles
 * para subir archivos.
 * 
 * Cada clase de estrategia debera cumplir lo siguiente:
 *  - Debera tener el metodo:
 *      subeArchivo (file: Buffer, filename: string, mimetype: string, req, doCommit: boolean)
 *      commit() <- Confirma los cambios
 *      rollback() <- Deshace los cambios
 *      releaseClient(destruyeCliente: boolean) <- Libera los recursos
 * 
 *  - Debera regresar la siguiente estructura:
        {
          id: number,
          filename: string,
          mimetype: string,
          url: string
        }
 * 
 *  - En caso de error debera regresar null
 */
exports.UploadFileStrategyManager = class {
    constructor(){}

    getDatabaseUploadStrategy () {
        const modulo = new UtilModule();
        return modulo;
    }

    getS3StorageStrategy () {
        return new S3StorageStrategy();
    }
}