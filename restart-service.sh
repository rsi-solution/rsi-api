echo "---- UPDATING APPLICATION"
git checkout master
git fetch
git pull
echo "---- INSTALLING NEW NODE MODULES"
npm install
echo "---- RESTARTING PM2 SERVICES"
pm2 stop all
pm2 start start-api-prod.sh --time
echo "---- DONE!"
