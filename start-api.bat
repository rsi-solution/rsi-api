@echo off
echo [LOCAL] Attempting to update database
cd BaseDeDatos
start liquibase update
cd ..
echo [LOCAL] Attempting to start API
nodemon index.js -- -no-ssl