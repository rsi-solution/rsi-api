// Instanciar
const { CommonModule } = require("./CommonModule");
// Para manejo de errores
const incidentService = require("../../services/incident.service");
const { RespuestaApiEntidad } = require("../../shared/interfaces/respuesta-api-entidad.interface");
const {RespuestaApi} = require('../../shared/interfaces/respuesta-api.interface');
const FacturasModule = require("./facturas.module");
const ChatModule = require("./chat.module");

module.exports = class MaquinasModule extends CommonModule {
  /**
   * Método encargado de obtener información de maquinarias
   * 
   * @param id Id de la mauqina a buscar
   * @param releaseClient Indica si se libera el cliente al final de la transaccion. Default si (true).
   * @returns Array con la maquinaria
   */
  async getMaquinas(id, estatus = 'AC', exactMatch = true, page = 1, releaseClient = true) {
    const OFFSET_MULTIPLICADOR = parseInt(process.env.FILAS_X_PAGINA);
    const offsetAlto = page * OFFSET_MULTIPLICADOR;
    let offsetBajo = offsetAlto - OFFSET_MULTIPLICADOR;
    let resultadosArray = []; // Guarda info. maquinaria buscada

    let consulta = `
        SELECT 
            "ID",
            "NOMBRE",
            "CATEGORIA",
            "MARCA",
            "ALTURA",
            "CANTIDAD_DISPONIBLES",
            "CANTIDAD_RENTADAS",
            "FOTO_URI",
            "NOTAS",
            "COSTO_X_HORA",
            "ESTATUS",
            "CAMPO1",
            "CAMPO2",
            "CAMPO3",
            "CAMPO4",
            "CAMPO5",
            "CAMPO6",
            "CAMPO7",
            "CAMPO8",
            "CAMPO9",
            "CREATED_ON",
            "CREATED_BY",
            "MODIFIED_ON",
            "MODIFIED_BY",
            "DESCRIPCION",
            "MODELO",
            "NUMERO_SERIE",
            "ANIO",
            "LLANTAS",
            "FUNCIONAMIENTO",
            "CAPACIDAD",
            "LLANTA_LLANTA",
            "LLANTA_CANASTILLA",
            "ANCHO",
            "CANASTILLA",
            "ANCHO_S_EXTENDER",
            "COSTO_X_DIA",
            "COSTO_X_SEMANA",
            "COSTO_X_MES",
            "COSTO_X_FIN_SEMANA",
            "COSTO_X_CATORCENA"
        FROM "MAQUINAS"
        WHERE 
          "ID" = $1
          AND
          "ESTATUS" = $4
        LIMIT CAST($2 AS BIGINT)
        OFFSET CAST($3 AS BIGINT);
        `;
 
    if (!exactMatch) {
      consulta = `
        SELECT 
          "ID",
          "NOMBRE" ,
          "CATEGORIA" ,
          "MARCA" ,
          "ALTURA" ,
          "CANTIDAD_DISPONIBLES",
          "CANTIDAD_RENTADAS",
          "FOTO_URI",
          "NOTAS",
          "COSTO_X_HORA",
          "ESTATUS",
          "CAMPO1",
          "CAMPO2",
          "CAMPO3",
          "CAMPO4",
          "CAMPO5",
          "CAMPO6",
          "CAMPO7",
          "CAMPO8",
          "CAMPO9",
          "CREATED_ON",
          "CREATED_BY",
          "MODIFIED_ON",
          "MODIFIED_BY",
          "DESCRIPCION",
          "MODELO",
          "NUMERO_SERIE",
          "ANIO",
          "LLANTAS",
          "FUNCIONAMIENTO",
          "CAPACIDAD",
          "LLANTA_LLANTA",
          "LLANTA_CANASTILLA",
          "ANCHO",
          "CANASTILLA",
          "ANCHO_S_EXTENDER",
          "COSTO_X_DIA",
          "COSTO_X_SEMANA",
          "COSTO_X_MES"
        FROM "MAQUINAS"
        WHERE
          ("ESTATUS" = $4)
          AND
          (
          UPPER("NOMBRE") LIKE '%' || UPPER($1) || '%'
          OR 
          UPPER("MARCA") LIKE '%' || UPPER($1) || '%'
          OR
          UPPER("MODELO") LIKE '%' || UPPER($1) || '%'
          OR
          UPPER("ID") LIKE '%' || UPPER($1) || '%'
          )
        ORDER BY "NOMBRE" ASC
        LIMIT CAST($2 AS BIGINT)
        OFFSET CAST($3 AS BIGINT);
      `;
    }

    try {
      await this.extraeCliente(); // Extrae un cliente para ejecutar queries

      const resultado = await this.client.query(consulta, [
        id,
        offsetAlto,
        offsetBajo,
        estatus
      ]);

      if (resultado.rowCount > 0) // Si hay resultados
        resultadosArray = resultado.rows;

    } catch (err) {

      incidentService.nuevoIncidente(
        101000,
        "maquinas.module.js - getMaquinas",
        `${err} ${consulta} ${id}`
      );

      resultadosArray = null;

    } finally {

      // Procesa transaccion en base a resultado
      if (releaseClient) this.client.release(false);
    }

    return resultadosArray;
  }

  /**
   * Método encargado de obtener categorias de las maquinarias
   * 
   * @returns Array con las categorias
   */
  async getCategoria() {
    let resultadosArray = []; // Guarda info. maquinaria buscada

    let consulta = `
          select 
            "VALOR" as valor,
            "DESCRIPCION" as descripcion,
            "ESTATUS" as estatus,
            "FOTO_URI" as foto_uri,
            "CREATED_ON" as created_on, 
            "CREATED_BY" as created_by,
            "MODIFIED_ON" as modified_on,
            "MODIFIED_BY" as modified_by
          from
            "CATEGORIAS_MAQUINAS" 
        `;


    try {
      await this.extraeCliente(); // Extrae un cliente para ejecutar queries

      const resultado = await this.client.query(consulta);

      if (resultado.rowCount > 0)// Si hay resultados
        resultadosArray = resultado.rows;

    } catch (err) {

      incidentService.nuevoIncidente(
        101000,
        "maquinas.module.js - getCategoria",
        `${err}
                ${consulta}`
      );

      resultadosArray = null;

    } finally {

      // Procesa transaccion en base a resultado
      this.client.release(false);
    }

    return resultadosArray;
  }

  /**
   *  Metodo encargado de obtener las maquinas disponibles.
   * 
   * @param criterio - Criterio de busqueda
   * @returns Objeto con numero de maquinas disponibles
   */
  async getDisponibilidadMaquina(criterio, releaseClient = true) {
    let resultadosArray = {};

    // Consultas SQL
    const consulta = `
        SELECT "CANTIDAD_DISPONIBLES", "CANTIDAD_RENTADAS"
        FROM "MAQUINAS"
        WHERE "ID" = $1;
      `;

    try {
      await this.extraeCliente();

      const resultado = await this.client.query(consulta, [
        criterio
      ]);

      if (resultado.rowCount > 0)
        resultadosArray = resultado.rows[0];

    } catch (err) {

      incidentService.nuevoIncidente(
        101000,
        "maquinas.module.js - getDisponibilidadMaquina",
        `${err}
        ${consulta} ${page}`
      );
      resultadosArray = null;

    } finally {

      // Liberar cliente del pool
      if (releaseClient) this.client.release(false);
    }

    return resultadosArray;
  }

  /**
   *  Metodo encargado de obtener una entrada-salidad de maquina
   * via coincidencia
   * 
   * @param criterio - Criterio de busqueda
   * @param page - Pagina actual
   * @returns Array de entidades encontradas. Null en caso de error
   */
  async getEntradasSalidas(criterio, page = 1) {
    const OFFSET_MULTIPLICADOR = parseInt(process.env.FILAS_X_PAGINA);
    const offsetAlto = page * OFFSET_MULTIPLICADOR;
    const offsetBajo = offsetAlto - OFFSET_MULTIPLICADOR;

    let resultadosArray = [];

    // Consultas SQL
    const consulta = `
        SELECT MAES."ID_MAQUINA"
            ,MAQU."NOMBRE" AS "NOMBRE_MAQUINA"
            ,MAES."CANTIDAD"
            ,MAES."NOTAS"
            ,MAES."FECHA_MOVIMIENTO"
        FROM "MAQUINAS_ENTRADAS_SALIDAS" MAES
        JOIN "MAQUINAS" MAQU ON ( MAQU."ID" = MAES."ID_MAQUINA" )
        WHERE MAES."ID_MAQUINA" = $1
        ORDER BY MAES."FECHA_MOVIMIENTO" DESC
        LIMIT CAST($2 AS BIGINT)
        OFFSET CAST($3 AS BIGINT);
      `;

    try {
      await this.extraeCliente();

      const resultado = await this.client.query(consulta, [
        criterio,
        offsetAlto,
        offsetBajo
      ]);

      if (resultado.rowCount > 0)
        resultadosArray = resultado.rows;

    } catch (err) {

      incidentService.nuevoIncidente(
        101000,
        "maquinas.module.js - getEntradasSalidas",
        `${err}
        ${consulta} ${page}`
      );
      resultadosArray = null;

    } finally {

      // Liberar cliente del pool
      this.client.release(false);
    }

    return resultadosArray;
  }

  

  /**
   *  Metodo encargado de insertar una nueva entidad
   * de entrada-salida de maquina.
   * 
   * @param entidad - Entidad de base de datos
   * @param id - Identificador interno del registro
   * @returns Verdadero si se inserto correctamente
   */
  async postEntradaSalida(entidad) {
    const consulta = `
        INSERT INTO "MAQUINAS_ENTRADAS_SALIDAS"
                ("ID_MAQUINA"
                ,"CANTIDAD"
                ,"NOTAS"
                ,"FECHA_MOVIMIENTO")
            VALUES
                ($1
                ,$2
                ,$3
                ,CURRENT_TIMESTAMP);
        `;

    const consultaRentaVencida = `
        SELECT "is_renta_vencida" ($1) as "RESULTADO"
    `;

    let resultado = new RespuestaApi();

    try {
      await this.extraeCliente(true);

      // Actualiza datos de usuario
      const resultadoPersistencia = await this.client.query(consulta, [
        entidad.ID_MAQUINA,
        entidad.CANTIDAD,
        entidad.NOTAS
      ]);

      if (resultadoPersistencia.rowCount > 0) {

        const cantidad = parseInt(entidad.CANTIDAD);
        const modo = (cantidad < 0) ? 'salida' : (cantidad > 0) ? 'entrada' : '';
        let isSuccess = false;

        if (modo === 'salida') {
          // Actualiza estatus de la renta
          isSuccess = await this.actualizaEstatusRenta(entidad.ID_RENTA, 'NE', 'LE', false, false);

          if (!isSuccess) {
            resultado.estatus = 'ERROR';
            resultado.mensaje = 'La renta no se encuentra disponible para su salida.';
          }

        } else if (modo === 'entrada') {
          // Verifica que la cantidad de maquinas rentadas actualmente
          const { CANTIDAD_RENTADAS } = await this.getDisponibilidadMaquina(entidad.ID_MAQUINA, false);

          if (CANTIDAD_RENTADAS > 0) {
            // Actualiza cantidad de maquinas disponibles y rentadas
            isSuccess = await this.putMaquinaEntrada(entidad.ID_MAQUINA, Math.abs(cantidad), false);
            if (!isSuccess) throw new Error('No ha sido posible actualizar el inventario');
            
            const isRentaVencida = await this.client.query(consultaRentaVencida, [entidad.ID_RENTA]);

            // Actualiza estatus de la renta en funcion de si la renta se encuentra vencida
            isSuccess = await this.actualizaEstatusRenta(entidad.ID_RENTA, 
                                                        (isRentaVencida.rows[0].RESULTADO) ? 'ET' : 'EN',
                                                        'NE',
                                                        true,
                                                        false);

          if (!isSuccess) {
            resultado.estatus = 'ERROR';
            resultado.mensaje = 'La renta no se encuentra disponible para su entrada.';
          }

          } else {
            resultado.estatus = 'ERROR';
            resultado.mensaje = 'No es posible realizar la operacion. No hay maquinas rentadas.';
          }
        }

      } else {
        throw new Error('Ha ocurrido un error al insertar la entidad');
      }

    } catch (err) {

      incidentService.nuevoIncidente(
        101000,
        "maquinas.module.js - postEntradaSalida",
        `${err}
        ${consulta} ${JSON.stringify(entidad)}`
      );

      resultado.estatus = 'ERROR';
      resultado.mensaje = 'Ha ocurrido un error desconocido. Contacte a soporte';

    } finally {

      // Procesa transaccion en base a resultado
      if (resultado.estatus === 'OK')
        await this.commit();
      else
        await this.rollback();

      // Libera cliente del pool
      this.client.release(false);
    }

    return resultado;
  }

  /**
   *  Metodo que actualiza la cantidad de maquinas disponibles
   * en inventario de una maquina.
   * 
   * @param {string} id Identificador interno de la maquina
   * @param {number} cantidadDisponible Cantidad de maquinas disponibles
   * @deprecated Use en su lugar putMaquinaSalida o putMaquinaEntrada
   */
  async actualizaCantidadDisponible(id, cantidadDisponible, doCommit = true) {
    const consulta = `
          UPDATE "MAQUINAS"
          SET "CANTIDAD_DISPONIBLES" = $2
          WHERE "ID" = $1;
        `;

    let isSuccess = true;

    try {
      await this.extraeCliente(doCommit);

      const resultado = await this.client.query(consulta, [
        id,
        cantidadDisponible
      ]);

      if (resultado.rowCount < 1)
        throw new Error('No se ha eliminado la promocion');

    } catch (err) {
      isSuccess = false;

      incidentService.nuevoIncidente(
        101000,
        "maquinas.module.js - actualizaCantidadDisponible",
        `${err}
        ${consulta} ${id}`
      );
    } finally {

      // Procesa transaccion en base a resultado
      if (doCommit) {
        if (isSuccess)
          await this.commit();
        else
          await this.rollback();

        // Libera cliente del pool
        this.client.release(false);
      }
    }

    return isSuccess;
  }

  /**
   *  Metodo que actualiza el estatus de una renta, dada una maquina.
   * 
   * @param {string} id Identificador interno de la renta / Folio
   * @param {string} estatus Estatus al cual cambiara la renta
   * @param {string} criterioEstatus Estatus en el que tiene que estar la renta
   */
   async actualizaEstatusRenta(id, estatus, criterioEstatus, isFinalizada = false, doCommit = true) {
    const consulta = `
          UPDATE "MAQUINAS_RENTAS"
          SET
            "ESTATUS" = $2,
            "FECHA_ENTREGA" = CASE '${isFinalizada}'
                            WHEN 'true' THEN current_timestamp
                            ELSE null
                          END
          WHERE "ID" = $1
          AND "ESTATUS" = $3;
        `;

    let isSuccess = true;

    try {
      await this.extraeCliente(doCommit);

      const resultado = await this.client.query(consulta, [id, estatus, criterioEstatus]);

      if (resultado.rowCount < 1)
        throw new Error('No se ha podido actualizar el estatus');

    } catch (err) {
      isSuccess = false;

      incidentService.nuevoIncidente(
        101000,
        "maquinas.module.js - actualizaEstatusRenta",
        `${err}
        ${consulta} ${id}`
      );
    } finally {

      // Procesa transaccion en base a resultado
      if (doCommit) {
        if (isSuccess)
          await this.commit();
        else
          await this.rollback();

        // Libera cliente del pool
        this.client.release(false);
      }
    }

    return isSuccess;
  }
  
  /**
   *  Metodo que resta -1 a cantidad disponible y suma +1 la cantidad rentada.
   * 
   * @param {string} id Identificador interno de la maquina
   */
   async putMaquinaSalida(id, cantidad = 1, doCommit = true) {
    const consulta = `
          UPDATE "MAQUINAS"
          SET
            "CANTIDAD_DISPONIBLES" = "CANTIDAD_DISPONIBLES" - $2,
            "CANTIDAD_RENTADAS" = "CANTIDAD_RENTADAS" + $2
          WHERE "ID" = $1;
        `;

    let isSuccess = true;

    try {
      await this.extraeCliente(doCommit);

      const resultado = await this.client.query(consulta, [id, cantidad]);

      if (resultado.rowCount < 1)
        throw new Error('No se ha podido actualizar la cantidad');

    } catch (err) {
      isSuccess = false;

      incidentService.nuevoIncidente(
        101000,
        "maquinas.module.js - putMaquinaSalida",
        `${err}
        ${consulta} ${id}`
      );
    } finally {

      // Procesa transaccion en base a resultado
      if (doCommit) {
        if (isSuccess)
          await this.commit();
        else
          await this.rollback();

        // Libera cliente del pool
        this.client.release(false);
      }
    }

    return isSuccess;
  }

  /**
   *  Metodo que resta -1 a cantidad rentada y suma +1 la cantidad disponible
   * 
   * @param {string} id Identificador interno de la maquina
   */
   async putMaquinaEntrada(id, cantidad = 1, doCommit = true) {
    const consulta = `
          UPDATE "MAQUINAS"
          SET
            "CANTIDAD_DISPONIBLES" = "CANTIDAD_DISPONIBLES" + $2,
            "CANTIDAD_RENTADAS" = "CANTIDAD_RENTADAS" - $2
          WHERE "ID" = $1;
        `;

    let isSuccess = true;

    try {
      await this.extraeCliente(doCommit);

      const resultado = await this.client.query(consulta, [id, cantidad]);

      if (resultado.rowCount < 1)
        throw new Error('No se ha podido actualizar la cantidad');

    } catch (err) {
      isSuccess = false;

      incidentService.nuevoIncidente(
        101000,
        "maquinas.module.js - putMaquinaEntrada",
        `${err}
        ${consulta} ${id}`
      );
    } finally {

      // Procesa transaccion en base a resultado
      if (doCommit) {
        if (isSuccess)
          await this.commit();
        else
          await this.rollback();

        // Libera cliente del pool
        this.client.release(false);
      }
    }

    return isSuccess;
  }

  /**
   * Método encargado de obtener categorias de las maquinarias
   * 
   * @returns Array con las categorias
   */
  async getMaquinasCategoria(categoria, estatus = 'AC', page = 1) {
    const OFFSET_MULTIPLICADOR = parseInt(process.env.FILAS_X_PAGINA);
    const offsetAlto = page * OFFSET_MULTIPLICADOR;
    let offsetBajo = offsetAlto - OFFSET_MULTIPLICADOR;

    let resultadosArray = []; // Guarda info. maquinaria buscada

    const consulta = `
        SELECT 
            "ID",
            "NOMBRE",
            "CATEGORIA",
            "MARCA",
            "ALTURA",
            "CANTIDAD_DISPONIBLES",
            "CANTIDAD_RENTADAS",
            "FOTO_URI",
            "NOTAS",
            "COSTO_X_HORA",
            "ESTATUS",
            "CAMPO1",
            "CAMPO2",
            "CAMPO3",
            "CAMPO4",
            "CAMPO5",
            "CAMPO6",
            "CAMPO7",
            "CAMPO8",
            "CAMPO9",
            "CREATED_ON",
            "CREATED_BY",
            "MODIFIED_ON",
            "MODIFIED_BY",
            "DESCRIPCION",
            "MODELO",
            "NUMERO_SERIE",
            "ANIO",
            "LLANTAS",
            "FUNCIONAMIENTO",
            "CAPACIDAD",
            "LLANTA_LLANTA",
            "LLANTA_CANASTILLA",
            "ANCHO",
            "CANASTILLA",
            "ANCHO_S_EXTENDER",
            "COSTO_X_DIA",
            "COSTO_X_SEMANA",
            "COSTO_X_MES"
        FROM "MAQUINAS"
        WHERE 
          "CATEGORIA" = $1
          AND
          "ESTATUS" = $4
        ORDER BY "NOMBRE"
        LIMIT CAST($2 AS BIGINT)
        OFFSET CAST($3 AS BIGINT);
        `;
    try {
      await this.extraeCliente(); // Extrae un cliente para ejecutar queries

      const resultado = await this.client.query(consulta, [
        categoria,
        offsetAlto,
        offsetBajo,
        estatus
      ]);

      if (resultado.rowCount > 0) // Si hay resultados
        resultadosArray = resultado.rows;

    } catch (err) {

      incidentService.nuevoIncidente(
        101000,
        "maquinas.module.js - getMaquinasCategoria",
        `${err}
                ${consulta} ${categoria}`
      );

      resultadosArray = null;

    } finally {

      // Procesa transaccion en base a resultado
      this.client.release(false);
    }

    return resultadosArray;
  }

  /**
   * Método encargado de obtener imagenes de maquinas
   * 
   * @returns Array con las categorias
   */
  async getMaquinasGaleria(page = 1) {
    const OFFSET_MULTIPLICADOR = parseInt(process.env.FILAS_X_PAGINA);
    const offsetAlto = page * OFFSET_MULTIPLICADOR;
    let offsetBajo = offsetAlto - OFFSET_MULTIPLICADOR;

    let resultadosArray = []; // Guarda info. maquinaria buscada

    const consulta = `
        SELECT 
            "ID",
            "NOMBRE",
            "FOTO_URI"
        FROM "MAQUINAS"
        LIMIT CAST($1 AS BIGINT)
        OFFSET CAST($2 AS BIGINT);
        `;
    try {
      await this.extraeCliente(); // Extrae un cliente para ejecutar queries

      const resultado = await this.client.query(consulta, [
        offsetAlto,
        offsetBajo
      ]);

      if (resultado.rowCount > 0) // Si hay resultados
        resultadosArray = resultado.rows;

    } catch (err) {

      incidentService.nuevoIncidente(
        101000,
        "maquinas.module.js - getMaquinasGaleria",
        `${err}
                ${consulta}`
      );

      resultadosArray = null;

    } finally {

      // Procesa transaccion en base a resultado
      this.client.release(false);
    }

    return resultadosArray;
  }

  /**
  * Método encargado de insertar/crear maquinaria en DB
  *
  * @param entidad Información a insertar en el DB
  * @returns Null si todo es correcto, mensaje si ocurrio un error
  */
  async postMaquina(entidad) {

    const consulta = `
        INSERT INTO "MAQUINAS"
          ("ID",
          "NOMBRE",
          "CATEGORIA",
          "MARCA",
          "ALTURA",
          "CANTIDAD_DISPONIBLES",
          "CANTIDAD_RENTADAS",
          "FOTO_URI",
          "NOTAS",
          "COSTO_X_HORA",
          "ESTATUS",
          "CREATED_ON",
          "CREATED_BY",
          "DESCRIPCION",
          "MODELO",
          "NUMERO_SERIE",
          "ANIO",
          "LLANTAS",
          "FUNCIONAMIENTO",
          "CAPACIDAD",
          "LLANTA_LLANTA",
          "LLANTA_CANASTILLA",
          "ANCHO",
          "CANASTILLA",
          "ANCHO_S_EXTENDER",
          "COSTO_X_DIA",
          "COSTO_X_SEMANA",
          "COSTO_X_MES",
          "COSTO_X_FIN_SEMANA",
          "COSTO_X_CATORCENA"
          )
        VALUES
        ( $1,
          $2,
          $3,
          $4,
          $5,
          $6,
          $7,
          $8,
          $9,
          $10,
          $11,
          CURRENT_TIMESTAMP,
          $12,
          $13,
          $14,
          $15,
          $16,
          $17,
          $18,
          $19,
          $20, 
          $21,
          $22,
          $23,
          $24,
          $25,
          $26,
          $27,
          $28,
          $29
        );
      `;

    let resultMessage = null;

    try {
      await this.extraeCliente(true);

      const resultado = await this.client.query(consulta, [
        entidad.ID,
        entidad.NOMBRE,
        entidad.CATEGORIA,
        entidad.MARCA,
        entidad.ALTURA,
        entidad.CANTIDAD_DISPONIBLES,
        entidad.CANTIDAD_RENTADAS,
        entidad.FOTO_URI,
        entidad.NOTAS,
        entidad.COSTO_X_HORA,
        entidad.ESTATUS,
        entidad.CREATED_BY,
        entidad.DESCRIPCION,
        entidad.MODELO,
        entidad.NUMERO_SERIE,
        entidad.ANIO,
        entidad.LLANTAS,
        entidad.FUNCIONAMIENTO,
        entidad.CAPACIDAD,
        entidad.LLANTA_LLANTA,
        entidad.LLANTA_CANASTILLA,
        entidad.ANCHO,
        entidad.CANASTILLA,
        entidad.ANCHO_S_EXTENDER,
        entidad.COSTO_X_DIA,
        entidad.COSTO_X_SEMANA,
        entidad.COSTO_X_MES,
        entidad.COSTO_X_FIN_SEMANA,
        entidad.COSTO_X_CATORCENA
      ]);

      if (!resultado.rowCount > 0) {
        resultMessage = 'Ocurrio un error al registrar la maquina';
      }

    } catch (err) {
      incidentService.nuevoIncidente(
        101001,
        "maquinas.module.js - postMaquina",
        `${err}
          ${consulta} ${JSON.stringify(entidad)}`
      );

      if (err.code === "23505")
        resultMessage = "La maquina que intenta registrar ya existe. Verifique los datos"
      else
        resultMessage = "Error al registrar la maquina, contacte al administrador"

    } finally {

      if (!resultMessage)
        await this.commit();
      else
        await this.rollback();

      this.client.release(false);
    }

    return resultMessage;
  }

  /**
   * Método para eliminar una maquina
   * 
   * @param id id de la maquina a eliminar
   * @returns Verdadero si la eliminacion fue correcta
   */
  async estatusMaquina(id, body) {

    const consulta = `
      UPDATE "MAQUINAS"
      SET
        "ESTATUS" = $2
      WHERE "ID" = $1
    `;

    let isSuccess = true;

    try {
      await this.extraeCliente(true);

      let resultado = await this.client.query(consulta, [
        id,
        body.estatus
      ]);

      if (resultado.rowCount < 1) {
        throw new Error('No se ha eliminado la maquina')
      }
    } catch (err) {
      isSuccess = false;

      incidentService.nuevoIncidente(
        101000,
        "maquinas.module.js - estatusMaquina",
        `${err}
        ${consulta} ${id}`
      );
    } finally {

      if (isSuccess)
        await this.commit();
      else
        await this.rollback();

      this.client.release(false);
    }

    return isSuccess;

  }

  /**
   * Método encargado de actualizar la información de una maquina
   * dado su id o nombre
   * 
   * @param maquina entidad de la maquina
   * @param id id de la maquina
   * @returns Null si todo es correcto, mensaje si ocurrio un error
   */
  async updateMaquina(maquina, id) {
    const consulta = `
      UPDATE "MAQUINAS"
      SET
        "NOMBRE"            = $1,
        "CATEGORIA"         = $2,
        "MARCA"             = $3,
        "ALTURA"            = $4,
        "FOTO_URI"          = $5,
        "MODIFIED_ON"       = CURRENT_TIMESTAMP,
        "MODIFIED_BY"       = $6,
        "DESCRIPCION"       = $7,
        "MODELO"            = $8,
        "NUMERO_SERIE"      = $9,
        "ANIO"              = $10,
        "LLANTAS"           = $11,
        "FUNCIONAMIENTO"    = $12,
        "CAPACIDAD"         = $13,
        "LLANTA_LLANTA"     = $14,
        "LLANTA_CANASTILLA" = $15,
        "ANCHO"             = $16,
        "CANASTILLA"        = $17,
        "ANCHO_S_EXTENDER"  = $18,
        "COSTO_X_DIA"       = $19,
        "COSTO_X_SEMANA"    = $20,
        "COSTO_X_MES"       = $21,
        "COSTO_X_FIN_SEMANA"  = $22,
        "COSTO_X_CATORCENA"   = $23
      WHERE "ID" = $24
    `;

    let isSuccess = true;

    let resultMessage = null;

    try {
      await this.extraeCliente(true);

      let resultado = await this.client.query(consulta, [
        maquina.NOMBRE,
        maquina.CATEGORIA,
        maquina.MARCA,
        maquina.ALTURA,
        maquina.FOTO_URI,
        maquina.MODIFIED_BY,
        maquina.DESCRIPCION,
        maquina.MODELO,
        maquina.NUMERO_SERIE,
        maquina.ANIO,
        maquina.LLANTAS,
        maquina.FUNCIONAMIENTO,
        maquina.CAPACIDAD,
        maquina.LLANTA_LLANTA,
        maquina.LLANTA_CANASTILLA,
        maquina.ANCHO,
        maquina.CANASTILLA,
        maquina.ANCHO_S_EXTENDER,
        maquina.COSTO_X_DIA,
        maquina.COSTO_X_SEMANA,
        maquina.COSTO_X_MES,
        maquina.COSTO_X_FIN_SEMANA,
        maquina.COSTO_X_CATORCENA,
        id
      ]);

      if(resultado.rowCount < 1 )
        throw new Error('No se ha actualizado la entidad');

    } catch (err) {

      isSuccess = false;

      incidentService.nuevoIncidente(
        101001,
        "maquinas.module.js - updateMaquina",
        `${err}
        ${consulta} ${id} ${JSON.stringify(maquina)}`
      );

      if (err.code === "23505")
        resultMessage = "La información que intenta actualizar ya existe. Verifique los datos"
      else
        resultMessage = "Error al actualizar la maquina, contacte al administrador"

    } finally {
      if (isSuccess)
        await this.commit();
      else
        await this.rollback();

      this.client.release(false);
    }

    return resultMessage;
  }

  /**
   *  Metodo encargado de insertar una nueva entidad
   * de renta y la regresa ya complementada.
   * 
   * @param entidad - Entidad de base de datos
   * @param id - Identificador interno del registro
   * @returns Respuesta API con la entidad final
   */
  async postRenta(entidad, doCommit = true) {
    let consulta = `
        INSERT INTO "MAQUINAS_RENTAS"
            ("ID"
            ,"ID_MAQUINA"
            ,"FECHA_INICIO"
            ,"FECHA_FIN"
            ,"NOMBRE_COMPLETO"
            ,"CALLE"
            ,"NUMERO_EXTERIOR"
            ,"NUMERO_INTERIOR"
            ,"COLONIA"
            ,"CIUDAD"
            ,"NOTAS"
            ,"ESTATUS"
            ,"USUARIO_ID"
            ,"PAYMENT_INTENT"
            ,"ID_PROMOCION"
            ,"NECESITA_FACTURA")
          VALUES
            (NEXTVAL('SEQ_MAQUINAS_RENTAS')
            ,$1
            ,@FECHA_INICIO@
            ,@FECHA_FIN@
            ,$2
            ,$3
            ,$4
            ,$5
            ,$6
            ,$7
            ,$8
            ,$9
            ,$10
            ,$11
            ,$12
            ,$13)
            RETURNING "ID", "FECHA_FIN";
        `;

    let resultadoType = new RespuestaApiEntidad();

    try {
      await this.extraeCliente(true);

      const resMaquinasDisponibles = await this.getDisponibilidadMaquina(entidad.ID_MAQUINA, false);

      if (resMaquinasDisponibles.CANTIDAD_DISPONIBLES) {

        // Verifica que existan mas de 0 maquinas disponibles para realizar la renta
        let maquinasDisponibles = parseInt(resMaquinasDisponibles.CANTIDAD_DISPONIBLES);
        if (maquinasDisponibles > 0) {

          // Actualiza cantidad disponible
          const isSuccess = await this.putMaquinaSalida(entidad.ID_MAQUINA, 1, false);

          if (isSuccess) {
            let fechaFin = entidad.FECHA_FIN;
            let fechaInicio = entidad.FECHA_INICIO;
            const duracion = entidad.DURACION;

            // Si no hay fecha inicio, se calcula en base a la duracion
            if (!fechaInicio) {
              if (duracion) {
                const fechaInicioObject = await this.calculaFechaInicioRenta(duracion, false);
                fechaInicio = `'${fechaInicioObject.FECHA_INICIO}'`;
              } else {
                throw new Error('Al no existir FECHA_INICIO, debe haber un campo DURACION');
              }
            }

            // Si no hay fecha fin, se calcula en base a la duracion (son los dias que se suman)
            if (!fechaFin) {
              if (duracion) fechaFin = `"calcula_fecha_fin_renta" ('${duracion}', ${fechaInicio})`;
              else throw new Error('Al no existir FECHA_FIN, debe haber un campo DURACION');
            } else {
              fechaFin = `TO_TIMESTAMP('${fechaFin}', 'YYYY-MM-DD HH24:MI')`;
            }

            // Reemplaza bind variable de fecha fin
            consulta = this.reemplazaBindVariable2(consulta, 'FECHA_INICIO', fechaInicio);
            consulta = this.reemplazaBindVariable2(consulta, 'FECHA_FIN', fechaFin);

            // Actualiza entidad
            const resultado = await this.client.query(consulta, [
              entidad.ID_MAQUINA,
              entidad.NOMBRE_COMPLETO,
              entidad.CALLE,
              entidad.NUMERO_EXTERIOR,
              entidad.NUMERO_INTERIOR,
              entidad.COLONIA,
              entidad.CIUDAD,
              entidad.NOTAS,
              entidad.ESTATUS,
              entidad.USUARIO_ID,
              entidad.PAYMENT_INTENT,
              (entidad.CODIGOS_PROMOCION && entidad.CODIGOS_PROMOCION.length > 0) ? entidad.CODIGOS_PROMOCION[0] : undefined,
              entidad.NECESITA_FACTURA
            ]);

            if (resultado.rowCount > 0) {

              // Instancia modulo de Chat con cliente heredado
              const chatModule = new ChatModule();
              chatModule.client = this.client;

              // Complementa entidad
              entidad.ID = resultado.rows[0].ID;
              entidad.FECHA_FIN = resultado.rows[0].FECHA_FIN;

              // Persiste pagos, si es que hay
              if (entidad.PAGOS && entidad.PAGOS.length > 0) {

                let totalPagado = 0.0;

                for (let index = 0; index < entidad.PAGOS.length; index++) {
                  const pago = entidad.PAGOS[index];
                  const resultadoPostPago = await this.postPago(pago, entidad.ID, false);

                  if (!resultadoPostPago) throw new Error('Error al persistir el pago de la renta');

                  totalPagado += pago.importe;
                }

                // Si es necesario facturar, se postea una alerta
                if (entidad.NECESITA_FACTURA === 'S') {
                  const data = {
                      idRenta: parseInt(entidad.ID),
                      idUsuario: parseInt(entidad.USUARIO_ID)
                  };

                  const notificacion = {
                      TITULO: 'Facturación',
                      CUERPO: `Se ha solicitado una factura por $${ totalPagado } para el folio de renta ${ entidad.ID }`,
                      DATA: data,
                      TIPO: 'FACTURA'
                  };

                  const entidadAlerta = {
                      TIPO_CONTENIDO: 'NO', // NO = Notificacion
                      ID_USUARIO_CREADOR: parseInt(entidad.USUARIO_ID),
                      CONTENIDO: notificacion,
                      ROL_CLAVE: 'ADMIN',
                      ESTATUS: 'AC'
                  };

                  await chatModule.postAlerta(entidadAlerta, false);
                }
              }

              // Define en resultado la entidad final
              resultadoType.entidad = entidad;

            } else {
              throw new Error('No ha sido posible insertar la entidad');
            }
          } else {
            throw new Error('No ha sido posible actualizar la cantidad disponible para la maquina dada');
          }
        } else {
          // No quedan maquinas disponibles para la renta
          resultadoType.mensaje = 'En este momento, no tenemos este equipo disponible para la renta. Intenta otro día o selecciona otro equipo.';
          resultadoType.estatus = 'ERROR';
        }
      } else {
        throw new Error('La cantidad disponible no ha devuelto una entidad consistente');
      }

    } catch (err) {

      incidentService.nuevoIncidente(
        101000,
        "maquinas.module.js - postRenta",
        `${err}
        ${consulta} ${JSON.stringify(entidad)}`
      );

      resultadoType.mensaje = 'Error al insertar la renta, contacte al administrador';
      resultadoType.estatus = 'ERROR';

    } finally {

      // Procesa transaccion en base a resultado
      if (doCommit) {
        if (resultadoType.estatus == 'OK')
          await this.commit();
        else
          await this.rollback();
      }

      try {
        // Libera cliente del pool
        this.client.release(false);
      } catch (error) {}
    }

    return resultadoType;
  }

  /**
   *  Metodo encargado de calcular la fecha inicio dada una duracion
   * 
   * @param duracion - Duracion de la renta
   * @returns Fecha fin con formato YYYY-MM-DD
   */
  async calculaFechaInicioRenta(duracion, releaseClient = true) {
    let resultadosArray = {};

    // Consultas SQL
    const consulta = `SELECT "calcula_fecha_inicio_renta" ($1) AS "FECHA_INICIO"`;

    try {
      await this.extraeCliente();

      const resultado = await this.client.query(consulta, [duracion]);

      if (resultado.rowCount > 0)
        resultadosArray = resultado.rows[0];

    } catch (err) {

      incidentService.nuevoIncidente(
        101000,
        "maquinas.module.js - calculaFechaInicioRenta",
        `${err}
        ${consulta}`
      );
      resultadosArray = null;

    } finally {

      // Liberar cliente del pool
      if (releaseClient) this.client.release(false);
    }

    return resultadosArray;
  }

  /**
   *  Metodo encargado de obtener un resumen de rentas dado un usuario
   * 
   * @param id - Identificador interno de la maquina
   * @param {boolean} consolidaPagos - Consolida pagos y adeudo en uno solo
   * @param page - Pagina actual
   * @returns Array de entidades encontradas. Null en caso de error
   */
  async getRentas(id, consolidaPagos, columnaCriterio = 'USUARIO_ID', page = 1, releaseClient = true) {
    let resultadosArray = [];

    // Consultas SQL
    const consulta = `
        SELECT MARE."ID"
          ,MARE."ID_MAQUINA"
          ,MARE."FECHA_INICIO"
          ,MARE."FECHA_FIN"
          ,MARE."NOMBRE_COMPLETO"
          ,MARE."CALLE"
          ,MARE."NUMERO_EXTERIOR"
          ,MARE."NUMERO_INTERIOR"
          ,MARE."COLONIA"
          ,MARE."CIUDAD"
          ,MARE."NOTAS"
          ,MARE."ESTATUS"
          ,MARE."USUARIO_ID"
          ,MARE."FECHA_ENTREGA"
          ,coalesce(LOV."descripcion", MARE."ESTATUS") as "ESTATUS_DESCRIPCION"
          ,MARE."ID_PROMOCION"
          ,MARE."NECESITA_FACTURA"
        FROM "MAQUINAS_RENTAS" MARE
        LEFT JOIN "LISTAS_VALORES" LOV ON (LOV."tipo_lov_clave" = 'ESTATUS_RENTA'
                          AND LOV."valor" = MARE."ESTATUS"
                          AND LOV."estatus" = 'AC')
        WHERE MARE."${columnaCriterio}" = $1
        AND MARE."FECHA_CREADO" BETWEEN CURRENT_TIMESTAMP - interval '3 month' AND CURRENT_TIMESTAMP
        ORDER BY MARE."FECHA_CREADO" DESC
      `;

    try {
      await this.extraeCliente();

      const resultado = await this.client.query(consulta, [
        id
      ]);

      if (resultado.rowCount > 0) {
        resultadosArray = resultado.rows;

        const facturasModule = new FacturasModule();
        facturasModule.client = this.client; // Pasa cliente del modulo actual, al de facturas

        // Llenar informacion extra por cada renta encontrada
        for (let index = 0; index < resultadosArray.length; index++) {
          // Castea a tipo interfaz para poder ser usado en Ionic
          resultadosArray[index].CODIGOS_PROMOCION = [];

          if (resultadosArray[index].ID_PROMOCION)
          resultadosArray[index].CODIGOS_PROMOCION.push(resultadosArray[index].ID_PROMOCION);

          // Consultar informacion de maquina
          const entidadMaquina = await this.getMaquinas(resultadosArray[index].ID_MAQUINA, 'AC', true, 1, false);
          resultadosArray[index].MAQUINA = entidadMaquina[0];

          // Consultar informacion de pagos
          const arrayPagos = await this.getPagos(resultadosArray[index].ID, false);

          if (arrayPagos && arrayPagos.length > 0) {
            // Verifica si hay que consolidar pagos
            if (consolidaPagos == 'true') {
              resultadosArray[index].PAGOS = this.consolidaPagos(arrayPagos);
            } else {
              resultadosArray[index].PAGOS = arrayPagos;
            }
          } else {
            resultadosArray[index].PAGOS = [];
          }

          // Consulta facturas de la renta
          const respuestaEntidadApi = await facturasModule.getFacturas(resultadosArray[index].ID, 1, false, 'ID_RENTA');

          if (respuestaEntidadApi.estatus === 'OK')   resultadosArray[index].FACTURAS = respuestaEntidadApi.entidad;
          else                                        resultadosArray[index].FACTURAS = [];
        }
      }


    } catch (err) {

      incidentService.nuevoIncidente(
        101000,
        "maquinas.module.js - getRentas",
        `${err}
        ${consulta} ${page}`
      );
      resultadosArray = null;

    } finally {

      // Liberar cliente del pool
      if (releaseClient) this.client.release(false);
    }

    return resultadosArray;
  }

  /**
   *  Metodo auxiliar encargado de consolidar todos los pagos en uno solo.
   * 
   * @param {*} pagosArray
   * @returns Dos pagos, uno con naturaleza A y otro C
   */
  consolidaPagos (pagosArray) {
    let arrayPendientesPago = [];
    let arrayPagados = [];
  
    arrayPendientesPago = pagosArray.filter( pago => pago.estatus === 'AC' );
    arrayPendientesPago = arrayPendientesPago.map( pago => (pago.naturaleza === 'C') ? parseFloat(pago.importe) : pago.importe * -1 );
    arrayPagados = pagosArray.filter( pago => pago.estatus === 'PA' );
    arrayPagados = arrayPagados.map( pago => (pago.naturaleza === 'C') ? parseFloat(pago.importe) : pago.importe * -1 );
  
    const totalPendientes = arrayPendientesPago.reduce( (previous, current) => previous + current, 0 ) ;
    const totalPagados = arrayPagados.reduce( (previous, current) => previous + current, 0 ) ;
  
    // Consolidar en un mismo array
    const arrayFinal = [];
  
    let entidad = {
      renta_id: 0,
      fecha: '',
      tipo_pago: '',
      concepto: 'Cargos',
      importe: totalPendientes,
      naturaleza: 'C',
      cantidad: 1,
      estatus: 'AC'
    };
    arrayFinal.push(entidad);
  
    entidad = {
      renta_id: 0,
      fecha: '',
      tipo_pago: '',
      concepto: 'Abonos',
      importe: totalPagados,
      naturaleza: 'A',
      cantidad: 1,
      estatus: 'PA'
    };
    arrayFinal.push(entidad);
  
    return arrayFinal;
  }

  /**
   *  Metodo encargado de obtener los pagos en funcion de una renta
   * 
   * @param id - Identificador de la renta
   * @param page - Pagina actual
   * @returns Array de entidades encontradas. Null en caso de error
   */
  async getPagos(id, releaseClient = true) {
    let resultadosArray = [];

    // Consultas SQL
    const consulta = `
        SELECT "id"
          ,"renta_id"
          ,"fecha"
          ,"folio_pago"
          ,"tipo_pago"
          ,"banco"
          ,"concepto"
          ,"importe"
          ,"naturaleza"
          ,"cantidad"
          ,"estatus"
        FROM "PAGOS"
        WHERE "renta_id" = $1
        ORDER BY "fecha" DESC
      `;

    try {
      await this.extraeCliente();

      const resultado = await this.client.query(consulta, [
        id
      ]);

      if (resultado.rowCount > 0)
        resultadosArray = resultado.rows;

    } catch (err) {

      incidentService.nuevoIncidente(
        101000,
        "maquinas.module.js - getPagos",
        `${err}
        ${consulta} ${page}`
      );
      resultadosArray = null;

    } finally {

      // Liberar cliente del pool
      if (releaseClient) this.client.release(false);
    }

    return resultadosArray;
  }

  /**
   *  Metodo encargado de insertar una nueva entidad de pago
   * 
   * @param entidad - Entidad de base de datos
   * @param id - Identificador interno del registro
   * @returns Verdadero si se inserto correctamente
   */
   async postPago(entidad, renta_id, releaseClient = true) {
    const consulta = `
      INSERT INTO "PAGOS"
          ("id"
          ,"renta_id"
          ,"fecha"
          ,"folio_pago"
          ,"tipo_pago"
          ,"banco"
          ,"concepto"
          ,"importe"
          ,"naturaleza"
          ,"cantidad"
          ,"estatus"
          ,"created_on"
          ,"created_by"
          ,"modified_on"
          ,"modified_by")
        VALUES
          (nextval('SEQ_PAGOS')
          ,$1
          ,TO_TIMESTAMP($2, 'YYYY-MM-DD HH24:MI')
          ,$3
          ,$4
          ,$5
          ,$6
          ,$7
          ,$8
          ,$9
          ,$10
          ,current_timestamp
          ,'anon'
          ,current_timestamp
          ,'anon');
        `;

    let isSuccess = true;

    try {
      await this.extraeCliente(true);

      // Actualiza datos de usuario
      const resultado = await this.client.query(consulta, [
        renta_id,
        entidad.fecha,
        entidad.folio_pago,
        entidad.tipo_pago,
        entidad.banco,
        entidad.concepto,
        entidad.importe,
        entidad.naturaleza,
        entidad.cantidad,
        entidad.estatus,
      ]);

      if (resultado.rowCount < 1)
        throw new Error('Error al insertar la entidad');

    } catch (err) {

      incidentService.nuevoIncidente(
        101000,
        "promocion.module.js - postPago",
        `${err}
        ${consulta} ${ JSON.stringify(entidad) }`
      );

      isSuccess = false;

    } finally {

      // Procesa transaccion en base a resultado
      if (releaseClient) {
        if (isSuccess)
          await this.commit();
        else
          await this.rollback();

        // Libera cliente del pool
        this.client.release(false);
      }
    }

    return isSuccess;
  }

  /**
   *  Metodo encargado de obtener las rentas dada una categoria y un modo
   * 
   * @param idCategoria - Identificador interno de la categoria
   * @param modo - Modo de consulta. 1: Rentas completadas, 2: Rentas pendientes de salida
   * @param page - Pagina actual
   * @param consolidaPagos Consolida los abonos y cargos en dos pagos
   * @returns Array de entidades encontradas. Null en caso de error
   */
   async getRentasXCategoria(idCategoria, modo, page = 1, consolidaPagos = false) {
    const OFFSET_MULTIPLICADOR = parseInt(process.env.FILAS_X_PAGINA);
    const offsetAlto = page * OFFSET_MULTIPLICADOR;
    const offsetBajo = offsetAlto - OFFSET_MULTIPLICADOR;
    let resultadosArray = [];

    // Consultas SQL
    let consulta = null;

    if (modo == '1') {
      // Consulta rentas completadas
      consulta = `
        SELECT MARE."ID"
          ,MARE."ID_MAQUINA"
          ,MARE."FECHA_INICIO"
          ,MARE."FECHA_FIN"
          ,MARE."NOMBRE_COMPLETO"
          ,MARE."CALLE"
          ,MARE."NUMERO_EXTERIOR"
          ,MARE."NUMERO_INTERIOR"
          ,MARE."COLONIA"
          ,MARE."CIUDAD"
          ,MARE."NOTAS"
          ,MARE."ESTATUS"
          ,MARE."USUARIO_ID"
          ,MARE."FECHA_ENTREGA"
          ,coalesce(LOV."descripcion", MARE."ESTATUS") as "ESTATUS_DESCRIPCION"
          ,MARE."ID_PROMOCION"
          ,MARE."NECESITA_FACTURA"
        FROM "MAQUINAS_RENTAS" MARE
        JOIN "MAQUINAS" MAQU ON (MAQU."ID" = MARE."ID_MAQUINA")
        LEFT JOIN "LISTAS_VALORES" LOV ON (LOV."tipo_lov_clave" = 'ESTATUS_RENTA'
                          AND LOV."valor" = MARE."ESTATUS"
                          AND LOV."estatus" = 'AC')
        WHERE MARE."ESTATUS" IN ('EN', 'ET')
        AND MAQU."CATEGORIA" = $1
        ORDER BY MARE."FECHA_INICIO" DESC
        LIMIT CAST($2 AS BIGINT)
        OFFSET CAST($3 AS BIGINT);
      `;

    } else if (modo == '2') {
      // Consulta rentas pendientes de salida
      consulta = `
        SELECT MARE."ID"
          ,MARE."ID_MAQUINA"
          ,MARE."FECHA_INICIO"
          ,MARE."FECHA_FIN"
          ,MARE."NOMBRE_COMPLETO"
          ,MARE."CALLE"
          ,MARE."NUMERO_EXTERIOR"
          ,MARE."NUMERO_INTERIOR"
          ,MARE."COLONIA"
          ,MARE."CIUDAD"
          ,MARE."NOTAS"
          ,MARE."ESTATUS"
          ,MARE."USUARIO_ID"
          ,MARE."FECHA_ENTREGA"
          ,coalesce(LOV."descripcion", MARE."ESTATUS") as "ESTATUS_DESCRIPCION"
          ,MARE."ID_PROMOCION"
          ,MARE."NECESITA_FACTURA"
        FROM "MAQUINAS_RENTAS" MARE
        JOIN "MAQUINAS" MAQU ON (MAQU."ID" = MARE."ID_MAQUINA")
        LEFT JOIN "LISTAS_VALORES" LOV ON (LOV."tipo_lov_clave" = 'ESTATUS_RENTA'
                          AND LOV."valor" = MARE."ESTATUS"
                          AND LOV."estatus" = 'AC')
        WHERE MARE."ESTATUS" = 'LE'
        AND MAQU."CATEGORIA" = $1
        ORDER BY MARE."FECHA_INICIO" DESC
        LIMIT CAST($2 AS BIGINT)
        OFFSET CAST($3 AS BIGINT);
      `;

    } else {
      return [];
    }

    try {
      await this.extraeCliente();

      const resultado = await this.client.query(consulta, [
        idCategoria,
        offsetAlto,
        offsetBajo
      ]);

      if (resultado.rowCount > 0) {
        resultadosArray = resultado.rows;

        const facturasModule = new FacturasModule();
        facturasModule.client = this.client; // Pasa cliente del modulo actual, al de facturas

        // Llenar informacion extra por cada renta encontrada
        for (let index = 0; index < resultadosArray.length; index++) {
          // Castea a tipo interfaz para poder ser usado en Ionic
          resultadosArray[index].CODIGOS_PROMOCION = [];

          if (resultadosArray[index].ID_PROMOCION)
          resultadosArray[index].CODIGOS_PROMOCION.push(resultadosArray[index].ID_PROMOCION);

          // Consultar informacion de maquina
          const entidadMaquina = await this.getMaquinas(resultadosArray[index].ID_MAQUINA, 'AC', true, 1, false);
          resultadosArray[index].MAQUINA = entidadMaquina[0];

          // Consultar informacion de pagos
          const arrayPagos = await this.getPagos(resultadosArray[index].ID, false);

          if (arrayPagos && arrayPagos.length > 0) {
            // Verifica si hay que consolidar pagos
            if (consolidaPagos == 'true') {
              resultadosArray[index].PAGOS = this.consolidaPagos(arrayPagos);
            } else {
              resultadosArray[index].PAGOS = arrayPagos;
            }
          } else {
            resultadosArray[index].PAGOS = [];
          }

          // Consulta las facturas
          const respuestaEntidadApi = await facturasModule.getFacturas(resultadosArray[index].ID, 1, false, 'ID_RENTA');

          if (respuestaEntidadApi.estatus === 'OK')   resultadosArray[index].FACTURAS = respuestaEntidadApi.entidad;
          else                                        resultadosArray[index].FACTURAS = [];
        }
      }


    } catch (err) {

      incidentService.nuevoIncidente(
        101000,
        "maquinas.module.js - getRentas",
        `${err}
        ${consulta} ${page}`
      );
      resultadosArray = null;

    } finally {

      // Liberar cliente del pool
      this.client.release(false);
    }

    return resultadosArray;
  }
}