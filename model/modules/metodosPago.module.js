const { CommonModule } = require("./CommonModule");
// Para manejo de errores
const incidentService = require("../../services/incident.service");
const { RespuestaApiEntidad } = require("../../shared/interfaces/respuesta-api-entidad.interface");
const { RespuestaApi } = require('../../shared/interfaces/respuesta-api.interface');

module.exports = class MetodosModule extends CommonModule {
    /**
     * Método para obtener métodos de pago de un usuario
     * 
     * @param entidad entidad con información a solicitar
     * @param exactMatch 
     * @returns 
     */
    async getMetodosPago(entidad) {
        let resultadosArray = [];

        let consulta = `
      SELECT
        "ID",
        "NOMBRE_COMPLETO",
        "NUMERO_TARJETA",
        "FECHA_VENCIMIENTO",
        "USUARIO_ID",
        "CREATED_ON"
      FROM 
        "METODOS_PAGO_USUARIOS"
      WHERE
        "USUARIO_ID" = $1
    `;

        try {
            await this.extraeCliente();

            const resultado = await this.client.query(consulta, [
                entidad
            ]);

            if (resultado.rowCount > 0)
                resultadosArray = resultado.rows;

        } catch (err) {
            incidentService.nuevoIncidente(
                101000,
                "usuarios.module.js - getMetodosPago",
                `${err} ${consulta}  ${entidad}`
            )

            resultadosArray = null;
        } finally {
            if (resultadosArray)
                await this.commit();
            else
                await this.rollback();

            this.client.release(false);
        }

        return resultadosArray;
    }

    /**
 * Método para guardar un nuevo método de pago
 * 
 * @param metodo entidad con información del método a guardar
 * @returns Null si todo es correcto, mensaje si ocurrio error
 * @deprecated Use stripe get customer payment methods
 */
    async postMetodoPago(id_usuario, metodo) {
        const consultaLimit = `
            SELECT COUNT("ID") FROM "METODOS_PAGO_USUARIOS" WHERE "USUARIO_ID" = $1
        `;

        const consulta = `
            INSERT INTO "METODOS_PAGO_USUARIOS"
              ( "ID", "NOMBRE_COMPLETO", "NUMERO_TARJETA", "FECHA_VENCIMIENTO", "USUARIO_ID", "CREATED_ON")
            VALUES
                (
                nextval('seq_met_pago'),
                $1,
                $2,
                $3,
                $4,
                CURRENT_TIMESTAMP
                );
            `;

        let resultMessage = null;

        try {
            await this.extraeCliente(true);

            let limite = await this.client.query(consultaLimit, [id_usuario]);

            if (limite.rows[0].count >= 3) {
                resultMessage = "Límite de métodos de pago alcanzados."
            } else {
                resultado = await this.client.query(consulta, [
                    metodo.nombre_completo,
                    metodo.numero_tarjeta,
                    metodo.fecha_vencimiento,
                    id_usuario
                ]);

                if (!resultado.rowCount > 0) {
                    resultMessage = "Error al registrar el método de pago, conatcte al administrador";
                }
            }



        } catch (err) {
            incidentService.nuevoIncidente(
                101000,
                "usuario.module.js - postMetodoPago",
                `${err} ${consulta} ${metodo}`
            );

            resultMessage = "Error al registrar el método de pago, contacte al administrador";

        } finally {
            // Procesa transaccion en base a resultado
            if (!resultMessage)
                await this.commit();
            else
                await this.rollback();

            // Libera cliente del pool
            this.client.release(false);
        }

        return resultMessage;
    }

    async deleteMetodo(usuario_id, id) {
        const consulta = `
            DELETE FROM "METODOS_PAGO_USUARIOS"
            WHERE 
                "ID" = $1
                AND
                "USUARIO_ID" = $2
        `;

        let isSuccess = true;

        try {
            await this.extraeCliente(true);

            let resultado = await this.client.query(consulta, [
                id,
                usuario_id
            ]);

            if (resultado.rowCount < 1)
                throw new Error('No se ha eliminado el método de pago');

        } catch (err) {
            isSuccess = false;

            incidentService.nuevoIncidente(
                101000,
                "metodosPago.module.js - deleteMetodo",
                `${err}
                ${consulta} ${id}`
            );

        } finally {
            if (isSuccess)
                await this.commit();
            else
                await this.rollback();

            // Libera cliente del pool
            this.client.release(false);
        }

        return isSuccess;
    }

    /**
     * Método encargado de actualizar la información de un método de pago
     * dado su id
     * 
     * @param entidad entidad del método de pago
     * @param id id del método
     */
    async putMetodoPago(entidad, usuario_id, id) {
        const consulta = `
                UPDATE "METODOS_PAGO_USUARIOS"
                SET
                    "NUMERO_TARJETA"        = $1,
                    "FECHA_VENCIMIENTO"     = $2,
                    "NOMBRE_COMPLETO"       = $3
                WHERE
                    "ID"    = $4
                    AND
                    "USUARIO_ID" = $5
            `;

        let isSuccess = true;

        let resultMessage = null;

        try {
            await this.extraeCliente(true);

            let resultado = await this.client.query(consulta, [
                entidad.NUMERO_TARJETA,
                entidad.FECHA_VENCIMIENTO,
                entidad.NOMBRE_COMPLETO,
                id,
                usuario_id
            ])

            if(resultado.rowCount < 1)
                throw new Error('No se ha actualizado la entidad');
        } catch (err) {
            isSuccess = false;

            incidentService.nuevoIncidente(
              101001,
              "maquinas.module.js - updateMaquina",
              `${err}
              ${consulta} ${id} ${usuario_id} ${JSON.stringify(entidad)}`
            );
        } finally {
            if (isSuccess)
            await this.commit();
          else
            await this.rollback();
    
          this.client.release(false);
        }

        return resultMessage;
    }
}