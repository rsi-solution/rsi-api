const { pool } = require('../../db/pool')

exports.CommonModule = class {
    client

    /**
     * Metodo encargado de generar un nuevo cliente dado un pool
     * 
     * @param pool Pool de clientes
     */
    async extraeCliente(iniciaTransaccion = false) {
        if (!this.client) {
            await pool.connect()
            .then( client => {
                this.client = client

                if (iniciaTransaccion) this.client.query('BEGIN');
            })
        }
    }

    /**
     *  Metodo encargado de confirmar los cambios actuales.
     * 
     * @returns Promesa del query COMMIT
     */
    async commit() {
        return this.client.query('COMMIT');
    }

    /**
     *  Metodo encargado de deshacer los cambios actuales.
     * 
     * @returns Promesa del query ROLLBACK
     */
    async rollback() {
        return this.client.query('ROLLBACK');
    }

    /**
     *  Metodo encargado de liberar el cliente actual
     */
    async releaseClient(destroyClient = false) {
        if (this.client) {
            await this.client.release(destroyClient);
            this.client = null;
        }
    }

    /**
     *  Metodo encargado de reemplazar una variable de enlace de estilo alternativo,
     * por el contenido dado.
     * 
     * @param {string} consulta Query de base de datos
     * @param {string} variable Variable de enlace alternativa
     * @param {string} contenido Contenido de la variable de enlace
     * @returns {string} Consulta con bind variable reemplaza.
     * @throws Propaga error al no encontrar la variable de reemplazo
     * @example Dada la consulta: SELECT #FECHA_NUEVA#
     *          el nombre de la variable es FECHA_NUEVA, por lo tanto:
     *          reemplazaBindVariable2 ('SELECT #FECHA_NUEVA#', 'FECHA_NUEVA', '01/01/2022');
     */
    reemplazaBindVariable2(consulta, variable, contenido) {
        const bindVariable = `@${ variable }@`;

        if (consulta.indexOf(consulta, bindVariable) > -1) {
            return consulta.replace(bindVariable, contenido);
        } else {
            throw new Error('No existe la bind variable ' + bindVariable);
        }
    }
}