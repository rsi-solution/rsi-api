const { CommonModule } = require("./CommonModule");
const incidentService = require("../../services/incident.service");

module.exports = class RolesModule extends CommonModule {

  /**
   *  Metodo encargado de regresar todos los roles en un
   * array.
   * 
   * @returns Array de roles
   */
  async getRoles() {
    const consulta = `
        SELECT "clave"
              ,"descripcion"
        FROM "ROLES"
        WHERE "estatus" = 'AC'
        `;

    try {
      await this.extraeCliente();

      const resultado = await this.client.query(consulta);
      this.client.release(false);

      if (resultado.rowCount > 0)
        return resultado.rows;
      else
        return [];

    } catch (err) {
      incidentService.nuevoIncidente(
        101000,
        "roles.module.js - getRoles",
        `${err}
        ${consulta}`
      );
      return {};
    }
  }

};
