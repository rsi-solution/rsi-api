const { CommonModule } = require("./CommonModule");
const incidentService = require("../../services/incident.service");
const { pool } = require("../../db/pool");

module.exports = class UtilModule extends CommonModule {

  /**
   *  Metodo encargado de obtener la estampa de tiempo
   * de la base de datos.
   * 
   * @returns String de estampa de tiempo
   */
  async getTimestamp() {
    const consulta = `
        SELECT current_timestamp as timestamp;
    `;

    try {
      await this.extraeCliente();

      const resultado = await this.client.query(consulta);
      this.client.release(false);

      if (resultado.rowCount > 0)
        return resultado.rows[0].timestamp;
      else
        return null;

    } catch (err) {
      incidentService.nuevoIncidente(
        101000,
        "util.module.js - getTimestamp",
        `${err}
        ${consulta}`
      );
      return {};
    }
  }

  /**
   *  Metodo encargado de obtener la estampa de tiempo
   * de la base de datos.
   * 
   * @returns String de estampa de tiempo
   */
   async getTimestampFormateado() {
    const consulta = `
      SELECT to_char(current_timestamp, 'YYYY-MM-DD') as timestamp
    `;

    try {
      await this.extraeCliente();

      const resultado = await this.client.query(consulta);
      this.client.release(false);

      if (resultado.rowCount > 0)
        return resultado.rows[0].timestamp;
      else
        return null;

    } catch (err) {
      incidentService.nuevoIncidente(
        101000,
        "util.module.js - getTimestamp",
        `${err}
        ${consulta}`
      );
      return {};
    }
  }

  /**
   *  Metodo que almacena un archivo en la base de datos
   * y regresa la entidad de archivo que incluye ya el ID y el URL
   * 
   * @param {*} file Archivo subido
   * @param {string} filename Nombre del archivo
   * @param {string} mimetype Mime-type del archivo
   * @param {*} req Request Http
   * @param {boolean} doCommit Realiza commit en BD
   * @returns Url del archivo
   */
  async subeArchivo(file, filename, mimetype, req, doCommit = true, url = null) {
    let resultado = null;
    let consulta = null;
    let urlBaseDatos = null;
    
    // Si no se pasa una URL como arg. se sube bajo el servidor local
    if (!url) {
      consulta = `
        INSERT INTO "archivos"
            ("id"
            ,"filename"
            ,"mimetype"
            ,"url"
            ,"file")
          VALUES
            (NEXTVAL('SEQ_ARCHIVOS')
            ,$1
            ,$2
            ,$4 || '/v1/archivo/' || CURRVAL('SEQ_ARCHIVOS')
            ,$3)
        RETURNING "id", "url"
      `;
      urlBaseDatos = `${ req.protocol }://${ req.get('host') }`;

    } else {
      consulta = `
        INSERT INTO "archivos"
            ("id"
            ,"filename"
            ,"mimetype"
            ,"url"
            ,"file")
          VALUES
            (NEXTVAL('SEQ_ARCHIVOS')
            ,$1
            ,$2
            ,$4
            ,$3)
        RETURNING "id", "url"
      `;
      urlBaseDatos = url;

    }

    try {
      await this.extraeCliente(true);

      const fileHex = (file && file.buffer) ? new Buffer(file.buffer, 'base64')
                      : null;

      const resultadoConsulta = await this.client.query(consulta, [
        filename,
        mimetype,
        fileHex,
        urlBaseDatos
      ]);

      if (resultadoConsulta.rowCount > 0) {
        const row = resultadoConsulta.rows[0];
        
        resultado = {
          id: row.id,
          filename: filename,
          mimetype: mimetype,
          url: row.url
        }
      }

    } catch (err) {
      incidentService.nuevoIncidente(
        101000,
        "util.module.js - subeArchivo",
        `${err}
        ${consulta}`
      );

    } finally {

      // Procesa transaccion de DB
      if (doCommit) {
        if (resultado)
          this.commit();
        else
          this.rollback();

        this.releaseClient(false);
      }
    }

    return resultado;
  }

  /**
   *  Metodo que consultar el buffer y el mimetype de un archivo
   * determinado.
   * 
   * @param {string} id Identificador interno del archivo
   * @returns Objeto {file: Buffer, mimetype: string}
   */
   async getArchivo(id) {
    const consulta = `
      SELECT "file", "mimetype"
      FROM "archivos"
      WHERE "id" = $1
    `;

    try {
      await this.extraeCliente();

      const resultado = await this.client.query(consulta, [
        id
      ]);
      this.client.release(false);

      if (resultado.rowCount > 0)
        return resultado.rows[0];
      else
        return null;

    } catch (err) {
      incidentService.nuevoIncidente(
        101000,
        "util.module.js - getArchivo",
        `${err}
        ${consulta}`
      );
      return null;
    }
  }

  /**
   *  Metodo encargado de insertar una nueva entidad
   * para un mensaje de contacto
   * 
   * @param entidad - Entidad de base de datos
   * @returns Verdadero si se inserto correctamente
   */
   async postMensajeContacto(entidad) {
    const consulta = `
        INSERT INTO "CONTACTO_MENSAJES"
            ("ID"
            ,"ID_USUARIO_CREADOR"
            ,"NOMBRE"
            ,"CORREO"
            ,"TELEFONO"
            ,"MENSAJE"
            ,"FECHA")
          VALUES
            (NEXTVAL('SEQ_CONTACTO_MENSAJES')
            ,$1
            ,$2
            ,$3
            ,$4
            ,$5
            ,CURRENT_TIMESTAMP);
        `;

    let isSuccess = true;

    try {
      await this.extraeCliente(true);

      // Actualiza datos de usuario
      const resultado = await this.client.query(consulta, [
        entidad.ID_USUARIO_CREADOR,
        entidad.NOMBRE,
        entidad.CORREO,
        entidad.TELEFONO,
        entidad.MENSAJE
      ]);

      if (resultado.rowCount < 1)
        throw new Error('Error al insertar la entidad en base de datos');

    } catch (err) {

      incidentService.nuevoIncidente(
        101000,
        "promocion.module.js - postMensajeContacto",
        `${err}
        ${consulta} ${ JSON.stringify(entidad) }`
      );

      isSuccess = false;

    } finally {

      // Procesa transaccion en base a resultado
      if (isSuccess)
        await this.commit();
      else
        await this.rollback();

      // Libera cliente del pool
      this.client.release(false);
    }

    return isSuccess;
  }

  /**
   *  Metodo encargado de insertar una nueva entidad
   * para una incidencia
   * 
   * @param entidad - Entidad de base de datos
   * @returns Verdadero si se inserto correctamente
   */
   async postIncidencia(entidad) {
    const consulta = `
        INSERT INTO "INCIDENCIAS"
            ("id"
            ,"modulo"
            ,"origen"
            ,"code"
            ,"mensaje"
            ,"stacktrace"
            ,"created_on"
            ,"created_by")
          VALUES
            (nextval('SEQ_INCIDENCIAS')
            ,$1
            ,$2
            ,$3
            ,$4
            ,$5
            ,current_timestamp
            ,'anon');
        `;

    let isSuccess = true;
    let clienteLocal;

    try {
      clienteLocal = await pool.connect();

      // Actualiza datos de usuario
      const resultado = await clienteLocal.query(consulta, [
        entidad.modulo,
        entidad.origen,
        entidad.code,
        entidad.mensaje,
        entidad.stacktrace
      ]);

      if (resultado.rowCount < 1)
        throw new Error('Error al insertar la entidad en base de datos');

    } catch (err) {

      console.error(err);
      isSuccess = false;

    } finally {

      // Procesa transaccion en base a resultado
      if (isSuccess)
        await clienteLocal.query('COMMIT');
      else
        await clienteLocal.query('ROLLBACK');

      // Libera cliente del pool
      clienteLocal.release(true);
    }

    return isSuccess;
  }

};

