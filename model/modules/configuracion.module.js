const { CommonModule } = require("./CommonModule");
const incidentService = require("../../services/incident.service");

module.exports = class ConfiguracionModule extends CommonModule {
    /**
    * Método encargado de obtener datos de configuracion
    * 
    * @param clave a buscar
    * @returns array con resultados
    */
    async getConfiguracion(clave) {

        const consulta = `
            select
                "clave",
                "descripcion",
                "valor1",
                "valor2",
                "valor3",
                "valor4",
                "valor5",
                "ultima_consulta",
                "tiempo_cache",
                "estatus",
                "created_on",
                "created_by",
                "modified_on",
                "modified_by"
            from
                "CONFIGURACION_APP" 
            where 
                clave = $1
        `;

        try {
            await this.extraeCliente(); // Extrae un cliente para ejecutar queries

            const resultado = await this.client.query(consulta, [clave]);
            this.client.release(false);

            if (resultado.rowCount > 0) {
                return resultado.rows;
            } else {
                return 'No hay resultados';
            }

        } catch (err) {

            incidentService.nuevoIncidente(
                101000,
                "configuracion.module.js - getTerminos",
                `${err} ${consulta}`
            );

            return [];

        }

    }
}