const { CommonModule } = require("./CommonModule");
const incidentService = require("../../services/incident.service");
const { RespuestaApiEntidad } = require("../../shared/interfaces/respuesta-api-entidad.interface");

module.exports = class FacturasModule extends CommonModule {

  /**
   *  Metodo encargado de insertar una nueva entidad (alerta)
   * 
   * @param entidad - Entidad de base de datos
   * @param {boolean} doCommit Realiza commit en BD
   * @param {Client} client Cliente de base de datos
   * @returns Verdadero si se inserto correctamente
   */
   async postFactura(entidad, client = null, doCommit = true) {
    const consulta = `
        INSERT INTO "FACTURAS"
            ("ID"
            ,"ID_USUARIO"
            ,"ID_ARCHIVO_PDF"
            ,"ID_ARCHIVO_XML"
            ,"ID_RENTA"
            ,"FECHA"
            ,"ESTATUS")
          VALUES
            (NEXTVAL('SEQ_FACTURAS')
            ,$1
            ,$2
            ,$3
            ,$4
            ,current_timestamp
            ,$5)
        RETURNING "ID";
        `;

    let resultadoType = new RespuestaApiEntidad();

    try {
      if (!client) await this.extraeCliente(true);
      else this.client = client;

      // Actualiza datos de usuario
      const resultado = await this.client.query(consulta, [
        entidad.ID_USUARIO,
        entidad.ID_ARCHIVO_PDF,
        entidad.ID_ARCHIVO_XML,
        entidad.ID_RENTA,
        entidad.ESTATUS
      ]);

      if (resultado.rowCount < 1) {
        throw new Error('Ha ocurrido un error al insertar la entidad');
      } else {
        entidad.ID = resultado.rows[0].ID;
        resultadoType.entidad = entidad;
      }

    } catch (err) {

      incidentService.nuevoIncidente(
        101000,
        "facturas.module.js - postFactura",
        `${err}
        ${consulta} ${ JSON.stringify(entidad) }`
      );

      resultadoType.estatus = 'ERROR';
      resultadoType.mensaje = 'Ha ocurrido un error desconocido al subir la factura. Contacte a soporte.';

    } finally {

      // Procesa transaccion en base a resultado
      if (doCommit) {
        if (resultadoType.estatus === 'OK')
          await this.commit();
        else
          await this.rollback();

        this.releaseClient(false);
      }
    }

    return resultadoType;
  }

  /**
   *  Metodo encargado de consultar las entidades de
   * facturacion dado un criterio
   * 
   * @param criterio - Criterio de busqueda
   * @param page Pagina actual
   * @returns Entidad encontrada. Null en caso de error
   */
   async getFacturas(criterio, page = 1, releaseClient = true, columnaCriterio = 'ID_USUARIO') {
    const OFFSET_MULTIPLICADOR = parseInt(process.env.FILAS_X_PAGINA);
    const offsetAlto = page * OFFSET_MULTIPLICADOR;
    const offsetBajo = offsetAlto - OFFSET_MULTIPLICADOR;
    let resultado = new RespuestaApiEntidad();

    // Consultas SQL
    const consulta = `
        SELECT "ID"
          ,FACT."ID_USUARIO"
          ,FACT."ID_RENTA"
          ,FACT."FECHA"
          ,FACT."ESTATUS"
          ,APDF."id"              AS "APDF_ID"
          ,APDF."filename"        AS "APDF_FILENAME"
          ,APDF."mimetype"        AS "APDF_MIMETYPE"
          ,APDF."url"             AS "APDF_URL"
          ,AXML."id"              AS "AXML_ID"
          ,AXML."filename"        AS "AXML_FILENAME"
          ,AXML."mimetype"        AS "AXML_MIMETYPE"
          ,AXML."url"             AS "AXML_URL"
        FROM "FACTURAS" FACT
        JOIN "archivos" APDF ON (APDF."id" = FACT."ID_ARCHIVO_PDF")
        JOIN "archivos" AXML ON (AXML."id" = FACT."ID_ARCHIVO_XML")
        WHERE FACT."${columnaCriterio}" = $1
        AND FACT."ESTATUS" = 'AC'
        ORDER BY FACT."FECHA" DESC
        LIMIT CAST($2 AS BIGINT)
        OFFSET CAST($3 AS BIGINT);
    `;

    try {
      await this.extraeCliente();

      const resultadoConsulta = await this.client.query(consulta, [
        criterio,
        offsetAlto,
        offsetBajo
      ]);

      if (resultadoConsulta.rowCount > 0)
        resultado.entidad = formateaRowsFactura(resultadoConsulta.rows);
      else
        resultado.entidad = [];

    } catch (err) {

      incidentService.nuevoIncidente(
        101000,
        "facturacion.module.js - getFacturas",
        `${err}
        ${consulta} ${ criterio }`
      );

      resultado.estatus = 'ERROR';
      resultado.mensaje = 'Ha ocurrido un error inesperado. Contacta a soporte.';

    } finally {

      // Liberar cliente del pool
      if (releaseClient) this.releaseClient(false);
    }

    return resultado;
  }

};

/**
 *  Metodo encargado de transformar filas de un resultado de una consulta
 * de facturas a una entidad ordenada para la App.
 * 
 * @param {Array} rows Filas resultado de una consulta de Facturas
 */
const formateaRowsFactura = (rows = []) => {

  let resultado = [];

  if (rows && rows.length > 0) {

    // Itera cada factura
    for (let index = 0; index < rows.length; index++) {
      const row = rows[index];

      resultado.push({
        ID: row.ID,
        ID_USUARIO: row.ID_USUARIO,
        ID_RENTA: row.ID_RENTA,
        FECHA: row.FECHA,
        ESTATUS: row.ESTATUS,
        PDF: {
          id: row.APDF_ID,
          filename: row.APDF_FILENAME,
          mimetype: row.APDF_MIMETYPE,
          url: row.APDF_URL,
          file: null
        },
        XML: {
          id: row.AXML_ID,
          filename: row.AXML_FILENAME,
          mimetype: row.AXML_MIMETYPE,
          url: row.AXML_URL,
          file: null
        }
      });
    }

  }

  return resultado;

}