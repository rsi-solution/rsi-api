const { sha256 } = require("js-sha256")

const { CommonModule } = require("./CommonModule");
const incidentService = require("../../services/incident.service");
const { RespuestaApi } = require("../../shared/interfaces/respuesta-api.interface");

module.exports = class PromocionesModule extends CommonModule {

  /**
   *  Metodo encargado de obtener una promocion
   * via coincidencia
   * 
   * @param criterio - Criterio de busqueda
   * @param page - Pagina actual
   * @returns Array de entidades encontradas. Null en caso de error
   */
  async getPromociones(criterio, page = 1) {
    const OFFSET_MULTIPLICADOR = parseInt(process.env.FILAS_X_PAGINA);
    const offsetAlto = page * OFFSET_MULTIPLICADOR;
    const offsetBajo = offsetAlto - OFFSET_MULTIPLICADOR;

    let resultadosArray = [];

    // Consultas SQL
    const consulta = `
            SELECT "ID"
            ,"ID_USUARIO"
            ,"TIPO"
            ,"DESCRIPCION"
            ,"VALOR1"
            ,"VALOR2"
            ,"VALOR3"
            ,"VALOR4"
            ,"FECHA_INICIO"
            ,"FECHA_FIN"
            ,"ESTATUS"
          FROM "PROMOCIONES"
          WHERE UPPER("DESCRIPCION") LIKE '%' || UPPER($1) || '%'
          OR UPPER("ID") LIKE '%' || UPPER($1) || '%'
          OR "ID_USUARIO" IN (
            SELECT "id"
            FROM "USUARIOS"
            WHERE UPPER("nombre_completo") LIKE '%' || UPPER($1) || '%'
            OR UPPER("email") LIKE '%' || UPPER($1) || '%'
          )
          OR "TIPO" IN (
            SELECT "valor"
            FROM "LISTAS_VALORES"
            WHERE "tipo_lov_clave" = 'TIPO_PROMO'
            AND UPPER("descripcion") LIKE '%' || UPPER($1) || '%'
          )
          ORDER BY "ID"
          LIMIT CAST($2 AS BIGINT)
          OFFSET CAST($3 AS BIGINT);
      `;

    try {
      await this.extraeCliente();

      const resultado = await this.client.query(consulta, [
        criterio,
        offsetAlto,
        offsetBajo
      ]);

      if (resultado.rowCount > 0)
        resultadosArray = resultado.rows;

    } catch (err) {

      incidentService.nuevoIncidente(
        101000,
        "promociones.module.js - getPromociones",
        `${err}
        ${consulta} ${ criterio }`
      );
      resultadosArray = null;

    } finally {

      // Liberar cliente del pool
      this.client.release(false);
    }

    return resultadosArray;
  }

  /**
   *  Metodo encargado de obtener una promocion vigente y valida
   * via coincidencia directa
   * 
   * @param criterio - Criterio de busqueda
   * @returns Entidad encontrada. Null en caso de error
   */
   async getPromocion(criterio, releaseClient = true) {
    let resultado = null;

    // Consultas SQL
    const consulta = `
            SELECT "ID"
            ,"ID_USUARIO"
            ,"TIPO"
            ,"DESCRIPCION"
            ,"VALOR1"
            ,"VALOR2"
            ,"VALOR3"
            ,"VALOR4"
            ,"FECHA_INICIO"
            ,"FECHA_FIN"
            ,"ESTATUS"
          FROM "PROMOCIONES"
          WHERE UPPER("ID") = UPPER($1)
          AND ("ESTATUS" = 'AC'
                OR "VALOR2" = 'wo-exp')
          AND CURRENT_TIMESTAMP >= "FECHA_INICIO"
          AND ("FECHA_FIN" IS NULL
          		OR CURRENT_TIMESTAMP < "FECHA_FIN");
      `;

    try {
      await this.extraeCliente();

      const resultadoEntidad = await this.client.query(consulta, [
        criterio
      ]);

      if (resultadoEntidad.rowCount > 0)
        resultado = resultadoEntidad.rows[0];

    } catch (err) {

      incidentService.nuevoIncidente(
        101000,
        "promociones.module.js - getPromocion",
        `${err}
        ${consulta} ${ criterio }`
      );
      resultado = null;

    } finally {

      // Liberar cliente del pool
      if (releaseClient) this.client.release(false);
    }

    return resultado;
  }

  /**
   *  Metodo encargado de actualizar una promocion
   * a estatus US (usado) si es de un solo uso (one-time).
   * 
   * @param id - Identificador interno del registro
   * @param idRenta - Identificador interno del registro de la renta donde se uso la promocion
   * @returns Respuesta API
   */
   async cancelaPromocion(id, doCommit = true) {
    const consulta = `
          UPDATE "PROMOCIONES"
          SET
            "CAMPO1"                = to_char(current_timestamp, 'DD-MM-YYYY HH24:MI:SS'),
            "ESTATUS"               = 'US'
          WHERE "ID" = $1
          AND "VALOR2" = 'one-time';
        `;

    const consultaTipoPromo = `
        SELECT "VALOR2"
        FROM "PROMOCIONES"
        WHERE "ID" = $1;
    `;

    let respuesta = new RespuestaApi();

    try {
      await this.extraeCliente(true);

      // Verifica clase de promocion
      const resultadoVerficacion = await this.client.query(consultaTipoPromo, [id]);

      if (resultadoVerficacion.rowCount > 0) {
        // Verifica que no sea promocion sin expiracion
        if (resultadoVerficacion.rows && resultadoVerficacion.rows[0] && resultadoVerficacion.rows[0].VALOR2 !== 'wo-exp') {
          // Cancela promocion
          const resultado = await this.client.query(consulta, [id]);

          if (resultado.rowCount < 1) {
            respuesta.estatus = 'ERROR';
            respuesta.mensaje = 'El código de promoción ' + id + " no existe.";
          }
        }

      } else {
        respuesta.estatus = 'ERROR';
        respuesta.mensaje = 'El código de promoción ' + id + " no existe.";
      }

    } catch (err) {

      respuesta.estatus = 'ERROR';
      respuesta.mensaje = 'Ha ocurrido un error al procesar las promociones. Contacte a soporte.';

      incidentService.nuevoIncidente(
        101000,
        "promocion.module.js - cancelaPromocion",
        `${err}
        ${consulta} ${ id }`
      );
    } finally {

      // Procesa transaccion en base a resultado
      if (doCommit) {
        if (respuesta.estatus === 'OK')
          await this.commit();
        else
          await this.rollback();
      }

      // Libera cliente del pool
      this.client.release(false);
    }

    console.log(id, respuesta.estatus);

    return respuesta;
  }

  /**
   *  Metodo encargado de actualizar una entidad de promocion.
   * 
   * @param entidad - Entidad de base de datos
   * @param id - Identificador interno del registro
   * @returns Verdadero si se actualizo correctamente
   */
  async putPromocion(entidad, id) {
    const consulta = `
          UPDATE "PROMOCIONES"
          SET
            "ID_USUARIO"            = $2,
            "TIPO"                  = $3,
            "DESCRIPCION"           = $4,
            "VALOR1"                = $5,
            "VALOR2"                = $6,
            "VALOR3"                = $7,
            "VALOR4"                = $8,
            "FECHA_INICIO"          = TO_TIMESTAMP($9, 'YYYY-MM-DD HH24:MI'),
            "FECHA_FIN"             = TO_TIMESTAMP($10, 'YYYY-MM-DD HH24:MI'),
            "ESTATUS"               = $11
          WHERE "ID" = $1;
        `;

    let isSuccess = true;

    try {
      await this.extraeCliente(true);

      // Actualiza datos de usuario
      const resultado = await this.client.query(consulta, [
        id,
        entidad.id_usuario,
        entidad.tipo,
        entidad.descripcion,
        entidad.valor1,
        entidad.valor2,
        entidad.valor3,
        entidad.valor4,
        entidad.fecha_inicio,
        entidad.fecha_fin,
        entidad.estatus
      ]);

      if (resultado.rowCount < 1)
        throw new Error('No se ha actualizado la entidad');

    } catch (err) {

      isSuccess = false;

      incidentService.nuevoIncidente(
        101000,
        "promocion.module.js - putPromocion",
        `${err}
        ${consulta} ${ JSON.stringify(entidad) } ${ id }`
      );
    } finally {

      // Procesa transaccion en base a resultado
      if (isSuccess)
        await this.commit();
      else
        await this.rollback();

      // Libera cliente del pool
      this.client.release(false);
    }

    return isSuccess;
  }

  /**
   *  Metodo encargado de eliminar una promocion
   * 
   * @param id - Identificador interno de la promocion
   * @returns Verdadero si la eliminacion fue correcta
   */
  async deletePromocion(id) {
    const consulta = `
          DELETE FROM "PROMOCIONES"
          WHERE "ID" = $1;
        `;

    let isSuccess = true;

    try {
      await this.extraeCliente(true);

      const resultado = await this.client.query(consulta, [id]);

      if (resultado.rowCount < 1)
        throw new Error('No se ha eliminado la promocion');

    } catch (err) {
      isSuccess = false;
      
      incidentService.nuevoIncidente(
        101000,
        "promocion.module.js - deletePromocion",
        `${err}
        ${consulta} ${ id }`
      );
    } finally {

      // Procesa transaccion en base a resultado
      if (isSuccess)
        await this.commit();
      else
        await this.rollback();

      // Libera cliente del pool
      this.client.release(false);
    }

    return isSuccess;
  }

  /**
   *  Metodo encargado de insertar una nueva entidad
   * 
   * @param entidad - Entidad de base de datos
   * @param id - Identificador interno del registro
   * @returns Verdadero si se inserto correctamente
   */
   async postPromocion(entidad) {
    const consulta = `
          INSERT INTO "PROMOCIONES" (
            "ID",
            "ID_USUARIO",
            "TIPO",
            "DESCRIPCION",
            "VALOR1",
            "VALOR2",
            "VALOR3",
            "VALOR4",
            "FECHA_INICIO",
            "FECHA_FIN",
            "ESTATUS"
          ) VALUES (
            "genera_codigo_promocion" ($1),
            $1,
            $2,
            $3,
            $4,
            $5,
            $6,
            $7,
            TO_TIMESTAMP($8, 'YYYY-MM-DD HH24:MI'),
            TO_TIMESTAMP($9, 'YYYY-MM-DD HH24:MI'),
            $10
          )
          RETURNING "ID";
        `;

    let codigo = null;

    try {
      await this.extraeCliente(true);

      // Actualiza datos de usuario
      const resultado = await this.client.query(consulta, [
        entidad.id_usuario,
        entidad.tipo,
        entidad.descripcion,
        entidad.valor1,
        entidad.valor2,
        entidad.valor3,
        entidad.valor4,
        entidad.fecha_inicio,
        entidad.fecha_fin,
        entidad.estatus
      ]);

      if (resultado.rowCount > 0)
        codigo = resultado.rows[0].ID;

    } catch (err) {

      incidentService.nuevoIncidente(
        101000,
        "promocion.module.js - postPromocion",
        `${err}
        ${consulta} ${ JSON.stringify(entidad) }`
      );
    } finally {

      // Procesa transaccion en base a resultado
      if (codigo)
        await this.commit();
      else
        await this.rollback();

      // Libera cliente del pool
      this.client.release(false);
    }

    return codigo;
  }

};