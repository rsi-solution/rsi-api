const { CommonModule } = require("./CommonModule");
const incidentService = require("../../services/incident.service");

module.exports = class LovModule extends CommonModule {

    /**
     *  Metodo encargado de regresar los lista valores
     * en funcion del tipo clave lista valor.
     * 
     * @param tipo - Clave del tipo lista valor
     * @returns Array de LOV
     */
    async getLovPorTipo(tipo) {
        const consulta = `
                SELECT "tipo_lov_clave"
                    ,"valor"
                    ,"descripcion"
                    ,"estatus"
                FROM "LISTAS_VALORES"
                WHERE "tipo_lov_clave" = $1
                AND "estatus" = 'AC'
            `;

        try {
            await this.extraeCliente();

            const resultado = await this.client.query(consulta, [tipo]);
            this.client.release(false);

            if (resultado.rowCount > 0) {
                return resultado.rows
            } else {
                return []
            }

        } catch (err) {
            incidentService.nuevoIncidente(
                101000,
                "lov.module.js - getLovPorTipo",
                `${err}
                ${consulta} ${tipo}`
            );
            return [];
        }
    }
}

