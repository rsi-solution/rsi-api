const { sha256 } = require("js-sha256")

const { CommonModule } = require("./CommonModule");
const incidentService = require("../../services/incident.service");

module.exports = class UsuariosModule extends CommonModule {

  /**
   *  Metodo encargado de obtener un usuario,
   * ya sea por su telefono, email o llave.
   * 
   * @param id - Telefono, email o llave del usuario
   * @returns Entidad de usuario
   */
  async getUsuario(id, exactMatch = true, page = 1, columna, comportamientoEspecial) {
    const OFFSET_MULTIPLICADOR = parseInt(process.env.FILAS_X_PAGINA);
    const offsetAlto = page * OFFSET_MULTIPLICADOR;
    let offsetBajo = offsetAlto - OFFSET_MULTIPLICADOR;
    let filtrosEspeciales = "";

    if (comportamientoEspecial) {
      const k = comportamientoEspecial.k;
      const v = comportamientoEspecial.v;

      if (k && v) {
        if (k === 'rol') {
          filtrosEspeciales += `
          AND USUA.id IN (SELECT ROUS."usuario_id"
          FROM "ROLES_USUARIOS" ROUS
          JOIN "ROLES" ROLE ON (ROLE."clave" = ROUS."rol_clave")
          WHERE ROUS."usuario_id" = USUA."id"
          AND ROLE."clave" IN (
          `;

          const roles = JSON.parse(v); // Array de roles

          roles.forEach(rol => {
            if (rol == 'ADMIN') filtrosEspeciales += `'EMP', 'CLIENTE'`;
            if (rol == 'EMP') filtrosEspeciales += `'ADMIN'`;
            if (rol == 'CLIENTE') filtrosEspeciales += `'ADMIN'`;
          });

          filtrosEspeciales += '))';
        }
      } else {
        throw new Error('El objeto de comportamiento especial, debe componerse de k y v.')
      }
    }

    let consulta = `
          SELECT
            id,
            email,
            telefono,
            nombre_completo,
            ultimo_inicio_sesion,
            foto_uri,
            fcm_token,
            estatus,
            created_on,
            created_by,
            modified_on,
            modified_by,
            rfc
          FROM "USUARIOS"
          WHERE ${columna} = $1
          ORDER BY nombre_completo
          LIMIT CAST($2 AS BIGINT)
          OFFSET CAST($3 AS BIGINT);
        `;

    if (!exactMatch) {
      consulta = `
          SELECT
          USUA.id,
          USUA.email,
          USUA.telefono,
          USUA.nombre_completo,
          USUA.ultimo_inicio_sesion,
          USUA.foto_uri,
          USUA.fcm_token,
          USUA.estatus,
          USUA.created_on,
          USUA.created_by,
          USUA.modified_on,
          USUA.modified_by,
          USUA.rfc
          FROM "USUARIOS" USUA
          WHERE UPPER(USUA.email) LIKE '%' || UPPER($1) || '%'
          OR UPPER(USUA.nombre_completo) LIKE '%' || UPPER($1) || '%'
          OR (
            SELECT 1
            FROM "ROLES_USUARIOS" ROUS
            JOIN "ROLES" ROLE ON (ROLE."clave" = ROUS."rol_clave")
            WHERE ROUS."usuario_id" = USUA."id"
            AND UPPER(ROLE."descripcion") LIKE '%' || UPPER($1) || '%'
            LIMIT 1
          ) IS NOT NULL
          OR $1 IS NULL
          ${filtrosEspeciales}
          ORDER BY USUA.nombre_completo
          LIMIT CAST($2 AS BIGINT)
          OFFSET CAST($3 AS BIGINT);
        `;
    }

    try {
      await this.extraeCliente();

      const resultado = await this.client.query(consulta, [
        id,
        offsetAlto,
        offsetBajo
      ]);

      let usuariosArray = [];

      if (resultado.rowCount > 0) {
        if (exactMatch) {
          let usuario = resultado.rows[0];
          usuario.roles = await this.getRolesXUsuarioId(usuario.id, false);

          usuariosArray.push(usuario);
        } else {

          for (let i = 0; i < resultado.rows.length; i++) {
            const usuario = resultado.rows[i];
            usuario.roles = await this.getRolesXUsuarioId(usuario.id, false);
            usuariosArray.push(usuario);
          }
        }

        this.client.release(false);
        return usuariosArray;
      }

    } catch (err) {
      incidentService.nuevoIncidente(
        101000,
        "usuario.module.js - getUsuario",
        `${err}
        ${consulta} ${id}`
      );

      this.client.release(false);
      return {};
    }
  }

  async getRolesXUsuarioId(id, instanciaCliente = true) {
    const consulta = `
          SELECT ROUS."rol_clave" as clave,
                  ROLE."descripcion" as descripcion
          FROM "ROLES_USUARIOS" ROUS
          JOIN "ROLES" ROLE ON (ROLE."clave" = ROUS."rol_clave")
          WHERE ROUS."usuario_id" = $1;
        `;

    try {
      if (instanciaCliente)
        await this.extraeCliente();

      const resultado = await this.client.query(consulta, [id]);

      if (instanciaCliente)
        this.client.release();

      if (resultado.rowCount > 0)
        return resultado.rows;
      else
        return [];

    } catch (err) {
      incidentService.nuevoIncidente(
        101000,
        "usuario.module.js - getRolesXUsuarioId",
        `${err}
        ${consulta} ${id}`
      );
      return {};
    }
  }


  /**
   *  Metodo encargado de obtener un usuario,
   * por identificador
   * 
   * @param id - Id
   * @returns Entidad de usuario
   */
  async getUsuarioXId(id) {
    const consulta = `
          SELECT
            USUA.id,
            USUA.email,
            USUA.telefono,
            USUA.nombre_completo,
            USUA.ultimo_inicio_sesion,
            USUA.foto_uri,
            USUA.fcm_token,
            USUA.estatus,
            USUA.created_on,
            USUA.created_by,
            USUA.modified_on,
            USUA.modified_by,
            USUA.rfc
            FROM "USUARIOS" USUA
          WHERE id = $1
        `;

    try {
      await this.extraeCliente();

      const resultado = await this.client.query(consulta, [id]);
      this.client.release(false);

      if (resultado.rowCount > 0)
        return resultado.rows[0];
      else
        return {}

    } catch (err) {
      incidentService.nuevoIncidente(
        101000,
        "usuario.module.js - getUsuarioXId",
        `${err}
        ${consulta} ${id}`
      );
      return {};
    }
  }

  /**
   *  Metodo encargado de regresar todos los roles en un
   * array.
   * 
   * @returns Array de roles
   */
  async getRoles() {
    const consulta = `
        SELECT "clave"
              ,"descripcion"
        FROM "ROLES"
        WHERE "estatus" = 'AC'
        `;

    try {
      await this.extraeCliente();

      const resultado = await this.client.query(consulta);
      this.client.release(false);

      if (resultado.rowCount > 0)
        return resultado.rows;
      else
        return [];

    } catch (err) {
      incidentService.nuevoIncidente(
        101000,
        "usuario.module.js - getRoles",
        `${err}
        ${consulta}`
      );
      return {};
    }
  }

  /**
   *  Metodo encargado de actualizar un usuario,
   * dado su telefono, email o llave.
   * La llave tendra que venir desencriptada.
   * 
   * @param usuario - Entidad de usuario
   * @param id - Email del usuario
   * @returns Verdadero si se actualizo correctamente
   */
  async updateUsuario(usuario, id) {
    const consulta = `
          UPDATE "USUARIOS"
          SET
            email                   = $1,
            telefono                = $2,
            llave                   = COALESCE ($3, llave),
            nombre_completo         = $4,
            ultimo_inicio_sesion    = $5,
            foto_uri                = $6,
            fcm_token               = $7,
            estatus                 = $8,
            modified_on             = $9,
            modified_by             = $10,
            rfc                     = $11
          WHERE email = $12
          RETURNING id;
        `;

    const consultaDeleteRoles = `
        DELETE FROM "ROLES_USUARIOS"
        WHERE "usuario_id" = $1;
    `;

    const consultaInsertRol = `
        INSERT INTO "ROLES_USUARIOS" (
          "usuario_id",
          "rol_clave"
        ) VALUES (
          $1,
          $2
        );
    `;

    let isSuccess = true;

    try {
      await this.extraeCliente(true);

      if (usuario.llave)
        usuario.llave = sha256(usuario.llave)

      // Actualiza datos de usuario
      let resultado = await this.client.query(consulta, [
        usuario.email,
        usuario.telefono,
        usuario.llave,
        usuario.nombre_completo,
        usuario.ultimo_inicio_sesion,
        usuario.foto_uri,
        usuario.fcm_token,
        usuario.estatus,
        usuario.modified_on,
        usuario.modified_by,
        usuario.rfc,
        id
      ]);

      if (resultado.rowCount > 0) {

        const identificador = resultado.rows[0].id;

        // Elimina roles actuales
        resultado = await this.client.query(consultaDeleteRoles, [identificador]);

        if (usuario.roles) {

          // Inserta roles
          for (let index = 0; index < usuario.roles.length; index++) {
            const rol = usuario.roles[index];
            resultado = await this.client.query(consultaInsertRol, [identificador, rol.clave]);

            if (resultado.rowCount < 1) throw new Error('Error al actualizar el rol. objeto: ' + JSON.stringify(rol));
          }
        }
      } else {
        isSuccess = false;
      }

    } catch (err) {

      isSuccess = false;

      incidentService.nuevoIncidente(
        101000,
        "usuario.module.js - updateUsuario",
        `${err}
        ${consulta} ${JSON.stringify(usuario)} ${id}`
      );
    } finally {

      // Procesa transaccion en base a resultado
      if (isSuccess)
        await this.commit();
      else
        await this.rollback();

      // Libera cliente del pool
      this.client.release(false);
    }

    return isSuccess;
  }

  /**
   *  Metodo encargado de eliminar a un usuario,
   * ya sea por su telefono, email o llave.
   * 
   * @param id - Email del usuario
   * @returns Verdadero si la eliminacion fue correcta
   */
  async deleteUsuario(id) {
    const consulta = `
          DELETE FROM "USUARIOS"
          WHERE email = $1;
        `;

    const consultaDeleteRoles = `
        DELETE FROM "ROLES_USUARIOS"
        WHERE "usuario_id" = (
          SELECT id
          FROM "USUARIOS"
          WHERE "email" = $1
        );
    `;

    let isSuccess = true;

    try {
      await this.extraeCliente(true);

      let resultado = await this.client.query(consultaDeleteRoles, [id]);
      resultado = await this.client.query(consulta, [id]);

      if (resultado.rowCount < 1) {
        throw new Error('No se ha eliminado el usuario');
      }

    } catch (err) {
      isSuccess = false;

      incidentService.nuevoIncidente(
        101000,
        "usuario.module.js - deleteUsuario",
        `${err}
        ${consulta} ${id}`
      );
    } finally {

      // Procesa transaccion en base a resultado
      if (isSuccess)
        await this.commit();
      else
        await this.rollback();

      // Libera cliente del pool
      this.client.release(false);
    }

    return isSuccess;
  }

  async getUsuariosFcmTokensByRole(rol, releaseClient = true) {
    const consulta = `
        select u.fcm_token 
        from "USUARIOS" u
        join "ROLES_USUARIOS" ru on (ru.usuario_id = u.id)
        where u.fcm_token is not null
        and ru.rol_clave = $1`;

    try {
      await this.extraeCliente();

      const resultado = await this.client.query(consulta, [rol]);

      if (resultado.rowCount > 0) {
        return resultado.rows.map(row => row.fcm_token);
      }
      else
        return null;

    } catch (err) {
      incidentService.nuevoIncidente(
        101000,
        "usuario.module.js - getUsuariosFcmTokensByRole",
        `${err}
        ${consulta} ${rol}`
      );
      return null;
    } finally {
      // Libera cliente del pool
      if (releaseClient) this.client.release(false);
    }
  }

}


