const { CommonModule } = require("./CommonModule");
const incidentService = require("../../services/incident.service");

module.exports = class ChatModule extends CommonModule {

  /**
   *  Metodo encargado de obtener mensajes de una conversacion.
   * Si total comparativo es menor que conteo de rows actuales, solo regresa
   * los rows faltantes.
   * 
   * @param id - Identificador interno
   * @param page - Pagina actual
   * @returns Array de entidades encontradas. Null en caso de error
   */
  async getConversacionMensajes(id, page = 1, hash) {
    const OFFSET_MULTIPLICADOR = parseInt(process.env.FILAS_X_PAGINA);
    const offsetAlto = page * OFFSET_MULTIPLICADOR;
    const offsetBajo = (offsetAlto - OFFSET_MULTIPLICADOR);

    let resultadosArray = [];

    // Consultas SQL
    const consulta = `
      SELECT * FROM (
          SELECT "ID_CONVERSACION"
            ,"ID_USUARIO_CREADOR"
            ,"ID_USUARIO_RECEPTOR"
            ,"TIPO_CONTENIDO"
            ,"CONTENIDO"
            ,"FECHA"
            ,"ESTATUS"
          FROM "CONVERSACIONES_MENSAJES"
          WHERE "ID_CONVERSACION" = $1
          AND ("CAMPO1" = $4 OR $4 IS NULL)
          ORDER BY "FECHA" DESC
          LIMIT CAST($2 AS BIGINT)
          OFFSET CAST($3 AS BIGINT)
        ) AS "mensajes"
        ORDER BY "mensajes"."FECHA" ASC
      `;

    try {
      await this.extraeCliente();

      const resultado = await this.client.query(consulta, [
        id,
        offsetAlto,
        offsetBajo,
        hash
      ]);

      if (resultado.rowCount > 0)
        resultadosArray = resultado.rows;

    } catch (err) {

      incidentService.nuevoIncidente(
        101000,
        "chat.module.js - getConversacionMensajes",
        `${err}
        ${consulta} ${ id }`
      );
      resultadosArray = null;

    } finally {

      // Liberar cliente del pool
      this.client.release(false);
    }

    return resultadosArray;
  }

  /**
   *  Metodo encargado de obtener conversaciones
   * en funcion de un usuario dado
   * 
   * @param criterio - Criterio de busqueda
   * @param page - Pagina actual
   * @returns Array de entidades encontradas. Null en caso de error
   */
   async getConversaciones(criterio, page = 1) {
    const OFFSET_MULTIPLICADOR = parseInt(process.env.FILAS_X_PAGINA);
    const offsetAlto = page * OFFSET_MULTIPLICADOR;
    const offsetBajo = offsetAlto - OFFSET_MULTIPLICADOR;

    let resultadosArray = [];

    // Consultas SQL
    const consulta = `
          SELECT CONV."ID"
            ,CONV."ID_USUARIO_CREADOR"
            ,CONV."ID_USUARIO_RECEPTOR"
            ,CONV."ESTATUS"
            ,CASE
              WHEN CONV."ID_USUARIO_CREADOR" = $1 THEN USRE."nombre_completo"
              ELSE USCR."nombre_completo"
            END AS "NOMBRE_CONTRAPARTE"
            ,CASE
              WHEN CONV."ID_USUARIO_CREADOR" = $1 THEN USRE."foto_uri"
              ELSE USCR."foto_uri"
            END AS "FOTO_URI_CONTRAPARTE"
            ,(
              SELECT
                CASE
                  WHEN COME."TIPO_CONTENIDO" = 'TO' THEN SUBSTR(COME."CONTENIDO", 1, 200)
                  WHEN COME."TIPO_CONTENIDO" = 'PR' THEN '(Promoción)'
                  WHEN COME."TIPO_CONTENIDO" = 'IM' THEN '(Foto)'
                  ELSE COME."CONTENIDO"
                END AS ULTIMO_MENSAJE
              FROM "CONVERSACIONES_MENSAJES" COME
              WHERE COME."ID_CONVERSACION" = CONV."ID"
              AND COME."ESTATUS" = 'AC'
              ORDER BY COME."FECHA" DESC
              FETCH FIRST ROW ONLY
            ) AS "ULTIMO_MENSAJE"
            ,CONV."MODIFIED_ON" AS "FECHA_ULTIMO_MENSAJE"
          FROM "CONVERSACIONES" CONV
          JOIN "USUARIOS" USCR ON (USCR."id" = CONV."ID_USUARIO_CREADOR" AND USCR."estatus" = 'AC')
          JOIN "USUARIOS" USRE ON (USRE."id" = CONV."ID_USUARIO_RECEPTOR" AND USRE."estatus" = 'AC')
          WHERE CONV."ID_USUARIO_CREADOR" = $1
          OR CONV."ID_USUARIO_RECEPTOR" = $1
          AND CONV."ESTATUS" = 'AC'
          ORDER BY CONV."CREATED_ON" DESC
          LIMIT CAST($2 AS BIGINT)
          OFFSET CAST($3 AS BIGINT);
      `;

    try {
      await this.extraeCliente();

      const resultado = await this.client.query(consulta, [
        criterio,
        offsetAlto,
        offsetBajo
      ]);

      if (resultado.rowCount > 0)
        resultadosArray = resultado.rows;

    } catch (err) {

      incidentService.nuevoIncidente(
        101000,
        "promociones.module.js - getConversaciones",
        `${err}
        ${consulta} ${ criterio }`
      );
      resultadosArray = null;

    } finally {

      // Liberar cliente del pool
      this.client.release(false);
    }

    return resultadosArray;
  }

  /**
   *  Metodo encargado de obtener el hash de cambio para una determinada
   * conversacion. Esto se usa para detectar si hay mensajes nuevos.
   * 
   * @param id - Identificador interno
   * @returns Array de entidades encontradas. Null en caso de error
   */
   async getConversacionHash(id) {
    let resultadosArray = {};

    // Consultas SQL
    const consulta = `
        SELECT "CHANGE_HASH"
        FROM "CONVERSACIONES"
        WHERE "ID" = $1
      `;

    try {
      await this.extraeCliente();

      const resultado = await this.client.query(consulta, [
        id
      ]);

      if (resultado.rowCount > 0)
        resultadosArray = resultado.rows[0];

    } catch (err) {

      incidentService.nuevoIncidente(
        101000,
        "chat.module.js - getConversacionHash",
        `${err}
        ${consulta} ${ id }`
      );
      resultadosArray = null;

    } finally {

      // Liberar cliente del pool
      this.client.release(false);
    }

    return resultadosArray;
  }

  /**
   *  Metodo encargado de obtener una conversacion
   * 
   * @param id - Identificador interno
   * @returns Array de entidades encontradas. Null en caso de error
   */
   async getConversacion(id) {
    let resultadosArray = [];

    // Consultas SQL
    const consulta = `
        SELECT "ID"
          ,"ID_USUARIO_CREADOR"
          ,"ID_USUARIO_RECEPTOR"
          ,"ESTATUS"
          ,"CHANGE_HASH"
        FROM "CONVERSACIONES"
        WHERE "ID" = $1
      `;

    try {
      await this.extraeCliente();

      const resultado = await this.client.query(consulta, [
        id
      ]);

      if (resultado.rowCount > 0)
        resultadosArray = resultado.rows;

    } catch (err) {

      incidentService.nuevoIncidente(
        101000,
        "chat.module.js - getConversacion",
        `${err}
        ${consulta} ${ id }`
      );
      resultadosArray = null;

    } finally {

      // Liberar cliente del pool
      this.client.release(false);
    }

    return resultadosArray;
  }

  /**
   *  Metodo encargado de insertar una nueva entidad
   * 
   * @param entidad - Entidad de base de datos
   * @param id - Identificador interno del registro
   * @returns Verdadero si se inserto correctamente
   */
   async postConversacion(entidad) {
    const consulta = `
      INSERT INTO "CONVERSACIONES"
          ("ID"
          ,"ID_USUARIO_CREADOR"
          ,"ID_USUARIO_RECEPTOR"
          ,"ESTATUS"
          ,"CREATED_BY")
        VALUES
          (nextval('SEQ_CONVERSACIONES')
          ,$1
          ,$2
          ,$3
          ,(SELECT "email" FROM "USUARIOS" WHERE "id" = $1))
          RETURNING "ID";
        `;

    let id = null;

    try {
      await this.extraeCliente(true);

      // Obtener un administrador de manera aleatoria
      if (entidad.ID_USUARIO_RECEPTOR == '-1') {
        const idAdminAleatorio = await this.getIdAleatorioAdmin(false);

        if (!idAdminAleatorio)
          throw new Error('No se han encontrado administradores');
        else
          entidad.ID_USUARIO_RECEPTOR = idAdminAleatorio;
      }

      // Actualiza datos de usuario
      const resultado = await this.client.query(consulta, [
        entidad.ID_USUARIO_CREADOR,
        entidad.ID_USUARIO_RECEPTOR,
        entidad.ESTATUS
      ]);

      if (resultado.rowCount > 0)
        id = resultado.rows[0].ID;

    } catch (err) {

      incidentService.nuevoIncidente(
        101000,
        "chat.module.js - postConversacion",
        `${err}
        ${consulta} ${ JSON.stringify(entidad) }`
      );
    } finally {

      // Procesa transaccion en base a resultado
      if (id)
        await this.commit();
      else
        await this.rollback();

      // Libera cliente del pool
      this.client.release(false);
    }

    return id;
  }

  /**
   *  Metodo encargado de obtener un identificador le
   * 
   * @param entidad - Entidad de base de datos
   * @param id - Identificador interno del registro
   * @returns Verdadero si se inserto correctamente
   */
  async getIdAleatorioAdmin(releaseClient = true) {
    let res = null;

    // Consultas SQL
    const consulta = `
          SELECT
            USUA."id"
          FROM "USUARIOS" USUA
          JOIN "ROLES_USUARIOS" ROUS ON (ROUS."usuario_id" = USUA."id")
          WHERE ROUS."rol_clave" = 'ADMIN'
      `;

    try {
      await this.extraeCliente();

      const resultado = await this.client.query(consulta);

      if (resultado.rowCount > 0) {
        const rows = resultado.rows;
        const aleatorio = Math.floor(Math.random() * ( (rows.length + 1) - 1)) + 1;

        return rows[aleatorio-1].id;
      }

    } catch (err) {

      incidentService.nuevoIncidente(
        101000,
        "chat.module.js - getIdAleatorioAdmin",
        `${err}
        ${consulta}`
      );
      res = null;

    } finally {

      // Liberar cliente del pool
      if (releaseClient) this.client.release(false);
    }

    return res;
  }

  /**
   *  Metodo encargado de insertar una nueva entidad
   * 
   * @param entidad - Entidad de base de datos
   * @param id - Identificador interno del registro
   * @returns Verdadero si se inserto correctamente
   */
   async postConversacionMensaje(entidad) {
    const consulta = `
      INSERT INTO "CONVERSACIONES_MENSAJES"
          ("ID_CONVERSACION"
          ,"ID_USUARIO_CREADOR"
          ,"TIPO_CONTENIDO"
          ,"CONTENIDO"
          ,"FECHA"
          ,"ESTATUS"
          ,"CAMPO1"
          ,"ID_USUARIO_RECEPTOR")
        VALUES
          ($1
          ,$2
          ,$3
          ,$4
          ,CURRENT_TIMESTAMP
          ,$5
          ,SUBSTRING (CAST(EXTRACT(MILLISECONDS FROM CURRENT_TIMESTAMP) AS VARCHAR), 1, 6)
          ,$6
          )
          RETURNING "CAMPO1";
        `;

    const consultaUpdateConversacion = `
        UPDATE "CONVERSACIONES"
        SET "MODIFIED_ON" = CURRENT_TIMESTAMP
        WHERE "ID" = $1;
    `;

    let isSuccess = true;

    try {
      await this.extraeCliente(true);

      // Actualiza datos de usuario
      let resultado = await this.client.query(consulta, [
        entidad.ID_CONVERSACION,
        entidad.ID_USUARIO_CREADOR,
        entidad.TIPO_CONTENIDO,
        entidad.CONTENIDO,
        entidad.ESTATUS,
        entidad.ID_USUARIO_RECEPTOR
      ]);

      if (resultado.rowCount > 0) {
          // Actualiza ultima fecha de mensaje para la conversacion
          resultado = await this.client.query(consultaUpdateConversacion, [
            entidad.ID_CONVERSACION
          ]);
      } else {
        throw new Error('Error al insertar la entidad');
      }

    } catch (err) {

      incidentService.nuevoIncidente(
        101000,
        "chat.module.js - postConversacionMensaje",
        `${err}
        ${consulta} ${ JSON.stringify(entidad) }`
      );

      isSuccess = false;

    } finally {

      // Procesa transaccion en base a resultado
      if (isSuccess)
        await this.commit();
      else
        await this.rollback();

      // Libera cliente del pool
      this.client.release(false);
    }

    return isSuccess;
  }

  /**
   *  Metodo encargado de regresar el total de notificaciones
   * 
   * @returns Numero de notificaciones totales
   */
   async countNotificaciones() {
    let resultado = null;

    // Consultas SQL
    const consulta = `
        SELECT COUNT(1) AS "TOTAL"
        FROM "ALERTAS"
        WHERE (
          ("TIPO_CONTENIDO" = 'NO' AND "ROL_CLAVE" = 'ADMIN')
          OR ("TIPO_CONTENIDO" = 'TO' AND "ROL_CLAVE" IS NULL)
        )
        AND "ESTATUS" = 'AC'
        LIMIT 9
      `;

    try {
      await this.extraeCliente();

      const resultadoConsulta = await this.client.query(consulta);

      resultado = resultadoConsulta.rows[0].TOTAL;

    } catch (err) {

      incidentService.nuevoIncidente(
        101000,
        "chat.module.js - countNotificaciones",
        `${err}
        ${consulta}`
      );
      resultado = null;

    } finally {

      // Liberar cliente del pool
      this.client.release(false);
    }

    return resultado;
  }
  
  /**
   *  Metodo encargado de obtener las alertas dado un rol,
   * si no se especifica ninguno o si rolClave es ADMIN, se obtienen
   * todos los activos.
   * 
   * @param rolClave - Clave del rol
   * @param page - Pagina actual
   * @param modo Modo de consulta. ALERTAS = Solo obtiene alertas, NOTIF. = Solo obtiene notificaciones
   * @returns Array de entidades encontradas. Null en caso de error
   */
   async getAlertas(rolClave, page = 1, modo = 'ALERTAS') {
    let OFFSET_MULTIPLICADOR = 0;

    if (modo === 'ALERTAS')     OFFSET_MULTIPLICADOR = parseInt(process.env.FILAS_X_PAGINA);
    else if (modo === 'NOTIF')  OFFSET_MULTIPLICADOR = 3;

    const offsetAlto = page * OFFSET_MULTIPLICADOR;
    const offsetBajo = (offsetAlto - OFFSET_MULTIPLICADOR);

    let resultadosArray = [];
    let filtroPersonalizado = '';

    if (modo === 'ALERTAS') {
      // Se filtra por clave de rol. Los admin. ven todas las alertas
      if (rolClave && rolClave !== 'ADMIN') {
        filtroPersonalizado = `AND "ROL_CLAVE" = '${ rolClave }'`;
      }
      filtroPersonalizado += ` AND "TIPO_CONTENIDO" <> 'NO' `;
      
    } else if (modo === 'NOTIF') {
      // Se filtran solamente alertas marcadas como notificaciones
      filtroPersonalizado = `AND "ROL_CLAVE" = '${ rolClave }'
                              AND "TIPO_CONTENIDO" = 'NO'`;
    }

    // Consultas SQL
    const consulta = `
          SELECT "ID"
            ,"TIPO_CONTENIDO"
            ,"ID_USUARIO_CREADOR"
            ,"CONTENIDO"
            ,"FECHA"
            ,"ROL_CLAVE"
            ,"ESTATUS"
          FROM "ALERTAS"
          WHERE "ESTATUS" = 'AC'
          ${filtroPersonalizado}
          OR "ROL_CLAVE" IS NULL
          ORDER BY "FECHA" DESC
          LIMIT CAST($1 AS BIGINT)
          OFFSET CAST($2 AS BIGINT);
      `;

    try {
      await this.extraeCliente();

      const resultado = await this.client.query(consulta, [
        offsetAlto,
        offsetBajo
      ]);

      if (resultado.rowCount > 0)
        resultadosArray = resultado.rows;

    } catch (err) {

      incidentService.nuevoIncidente(
        101000,
        "chat.module.js - getAlertas",
        `${err}
        ${consulta} ${ id }`
      );
      resultadosArray = null;

    } finally {

      // Liberar cliente del pool
      this.client.release(false);
    }

    return resultadosArray;
  }

  /**
   *  Metodo encargado de insertar una nueva entidad (alerta)
   * 
   * @param entidad - Entidad de base de datos
   * @returns Verdadero si se inserto correctamente
   */
   async postAlerta(entidad, releaseCliente = true) {
    const consulta = `
        INSERT INTO "ALERTAS"
            ("ID"
            ,"TIPO_CONTENIDO"
            ,"ID_USUARIO_CREADOR"
            ,"CONTENIDO"
            ,"FECHA"
            ,"ROL_CLAVE"
            ,"ESTATUS")
          VALUES
            (nextval('SEQ_ALERTAS')
            ,$1
            ,$2
            ,$3
            ,CURRENT_TIMESTAMP
            ,$4
            ,$5);
        `;

    let isSuccess = true;

    try {
      await this.extraeCliente(true);

      // Actualiza datos de usuario
      const resultado = await this.client.query(consulta, [
        entidad.TIPO_CONTENIDO,
        entidad.ID_USUARIO_CREADOR,
        entidad.CONTENIDO,
        entidad.ROL_CLAVE,
        entidad.ESTATUS
      ]);

      if (resultado.rowCount < 1)
        throw new Error('Ha ocurrido un error al insertar la entidad');

    } catch (err) {

      incidentService.nuevoIncidente(
        101000,
        "chat.module.js - postAlerta",
        `${err}
        ${consulta} ${ JSON.stringify(entidad) }`
      );

      isSuccess = false;

    } finally {

      if (releaseCliente) {
        // Procesa transaccion en base a resultado
        if (isSuccess)
          await this.commit();
        else
          await this.rollback();

        // Libera cliente del pool
        this.client.release(false);
      }
    }

    return isSuccess;
  }

};