const { CommonModule } = require("./CommonModule");
const incidentService = require("../../services/incident.service");
const UsuariosModule = require("./usuarios.module");
const pagosService = require('../../services/pagos.service');

module.exports = class LoginModule extends CommonModule {
  /**
   * Verifica que verifica si el email y contraseña son correctos y
   * complementa la información del usuario.
   *
   * @param email
   * @param llave
   * @param fcmToken
   * @returns Boolean, regresa true si es correcto
   */
  async loginUsuario(email, llave, fcmToken = null) {
    const consulta = `
            SELECT
              id,
              telefono,
              nombre_completo,
              foto_uri,
              fcm_token,
              estatus,
              rfc,
              stripe_key
            FROM "USUARIOS"
            WHERE email = $1
            AND llave = $2
            AND estatus = 'AC'
        `;
    let resultado = null;

    try {
      await this.extraeCliente();

      const usuariosModule = new UsuariosModule();
      const resultadoQuery = await this.client.query(consulta, [
        email, llave
      ]);

      if (resultadoQuery.rowCount > 0) {

        let row = resultadoQuery.rows[0];

        // Actualiza token en caso de que sea diferente
        if (fcmToken && row.fcm_token !== fcmToken)
          await this.actualizaFcmToken(row.id, fcmToken, false);

        resultado = {
          id: row.id,
          email: email,
          telefono: row.telefono,
          nombre_completo: row.nombre_completo,
          foto_uri: row.foto_uri,
          fcm_token: fcmToken,
          estatus: row.estatus,
          roles: await usuariosModule.getRolesXUsuarioId( row.id ),
          rfc: row.rfc,
          stripe_key: row.stripe_key
        };

      }

    } catch (err) {
      incidentService.nuevoIncidente(
        101000,
        "login.module - loginUsuario",
        `${err}
        ${consulta} ${ email, llave }`
      );
      return null;

    } finally {
      this.client.release(false);
    }

    return resultado;
  }

  /**
   *  Metodo encargado de registrar a un usuario en persistencia.
   *
   * @param usuario Entidad de usuario
   * @returns Null si todo es correcto, mensaje si ocurrio un error
   */
  async registroUsuario(usuario) {
    const consulta = `
            INSERT INTO "USUARIOS" (
            id, email, telefono, llave, nombre_completo, foto_uri, estatus, created_by, rfc)
            VALUES (nextval('SEQ_USUARIOS'), $1, $2, $3, $4, $5, $6, $7, $8)
            RETURNING id;
        `;
    const consultaInsertRol = `
        INSERT INTO "ROLES_USUARIOS" (
          "usuario_id",
          "rol_clave"
        ) VALUES (
          $1,
          $2
        );
    `;

    const consultaAddStripe = `
        UPDATE "USUARIOS"
          SET stripe_key = $1
        WHERE id = $2
    `;

    let resultMessage = null;

    try {
        await this.extraeCliente(true);

        let resultado = await this.client.query(consulta, [
          usuario.email,
          usuario.telefono,
          usuario.llave,
          usuario.nombre_completo,
          usuario.foto_uri,
          usuario.estatus,
          usuario.created_by,
          usuario.rfc
        ]);

        if (resultado.rowCount > 0) {
          const identificador = resultado.rows[0].id;

          // Registra roles
          if (usuario.roles) {
            for (let index = 0; index < usuario.roles.length; index++) {
              const rol = usuario.roles[index];

              resultado = await this.client.query(consultaInsertRol, [identificador, rol.clave]);

              if (resultado.rowCount < 1) throw new Error('Error al actualizar el rol. objeto: ' + JSON.stringify(rol));
            }

            if(usuario.roles.find(o => o.clave === 'CLIENTE')){
              const key = await pagosService.createCustomer(usuario.email, usuario.nombre_completo, usuario.telefono);

              resultado = await this.client.query(consultaAddStripe, [key.id, identificador]);

              if (resultado.rowCount < 1) throw new Error('Error al añadir llave de stripe');
            }
          }

        } else {
          resultMessage = "Error al registrar el usuario, contacte al administrador";
        }

    } catch (err) {
        incidentService.nuevoIncidente(
            101001,
            "Usuario.entity.js - registroUsuario",
            `${err}
            ${consulta} ${JSON.stringify(usuario)}`
          );

        if (err.code === "23505")
          resultMessage = "El usuario que intenta registrar ya existe. Verifique los datos"
        else
          resultMessage = "Error al registrar el usuario, contacte al administrador"

    } finally {

      // Procesa transaccion en base a resultado
      if (!resultMessage)
        await this.commit();
      else
        await this.rollback();

      // Libera cliente del pool
      this.client.release(false);
    }

    return resultMessage;
  }

  /**
   *  Metodo encargado de restablecer el password si es que
   * ambos hash de llaves coinciden
   * 
   * @param usuario - Interface de usuario login
   * @param nuevaLlave - Llave nueva
   */
  async restableceLlave(usuario, nuevaLlave) {
    const consulta = `
        UPDATE "USUARIOS"
        SET llave = $1
        WHERE llave = $2
        AND email = $3;
    `;

    try {
        await this.extraeCliente();

        const resultado = await this.client.query(consulta, [
          nuevaLlave,
          usuario.llave,
          usuario.email
        ]);
        this.client.release(false);
    
        return resultado.rowCount > 0
    } catch (err) {
        incidentService.nuevoIncidente(
            101004,
            "Usuario.entity.js - restableceLlave",
            `${err}
            ${consulta} ${JSON.stringify(usuario)}`
          );
        return false
    }
  }

  /**
   *  Metodo encargado de actualizar el FCM token de
   * una entidad de usuario
   * 
   * @param id - Identificador interno del registro
   * @param fcmToken - Token de Firebase Cloud Messaging
   * @param releaseClient - Indica si se liberara el cliente de bd
   * @returns Verdadero si se actualizo correctamente
   */
   async actualizaFcmToken(id, fcmToken, releaseClient = true) {
    const consulta = `
          UPDATE "USUARIOS"
          SET
            "fcm_token"             = $2
          WHERE "id"                = $1;
        `;

    let isSuccess = true;

    try {
      await this.extraeCliente(true);

      // Actualiza datos de usuario
      const resultado = await this.client.query(consulta, [
        id,
        fcmToken
      ]);

      if (resultado.rowCount < 1)
        throw new Error('No se ha podido actualizar el token');

    } catch (err) {

      isSuccess = false;

      incidentService.nuevoIncidente(
        101000,
        "promocion.module.js - putPromocion",
        `${err}
        ${consulta} ${ JSON.stringify(entidad) } ${ id }`
      );
    } finally {

      // Procesa transaccion en base a resultado
      if (isSuccess)
        await this.commit();
      else
        await this.rollback();

      // Libera cliente del pool
      if (releaseClient) this.client.release(false);
    }

    return isSuccess;
  }
};
