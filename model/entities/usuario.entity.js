module.exports = class Usuario {
    id
    email
    telefono
    llave
    nombre_completo
    ultimo_inicio_sesion
    foto
    foto_uri
    fcm_token
    estatus
    created_on
    created_by
    modified_on
    modified_by
    roles = []

    constructor() {}
}