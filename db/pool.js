const { Pool } = require("pg");
const { log } = require("../services/logger.service")

const MAXIMOS_INTENTOS_CONEXION_BD = 10;
const SLEEP_INTENTO_CONEXION_MS = 10000;

const verificaConexionBd = () => {
    return pool.connect()
    .then(client => {
        client.query('SELECT 1');
        client.release(false);

        log.info('Conexion exitosa a base de datos');
    });
}

const reintentaConexion = () => {
    let intentoActual = 1;

    log.info('Intentando conexion con base de datos');

    verificaConexionBd()
        .catch(err => {
            log.error(`Error al conectarse a base de datos `, err);

            if (++intentoActual === MAXIMOS_INTENTOS_CONEXION_BD) {
                throw err;
            }

            setTimeout(() => reintentaConexion(), SLEEP_INTENTO_CONEXION_MS);
        });
}

let connectionString = null;
let dbConfig = null;

// Obtiene la cadena de conexion desde los args.
// Si no lo encuentra, toma el que se encuentre en .env
const args = process.argv.slice(2);

if (args.includes('-c')) {
    const argIndex = args.indexOf('-c') + 1;
    connectionString = args[argIndex];
} else {
    connectionString = process.env.CONNECTION_STRING;
}

if (args.includes('-no-ssl')) {
    dbConfig = {
        connectionString: connectionString,
        connectionTimeoutMillis: 10000,
        max: 10000
    }
} else {
    dbConfig = {
        connectionString: connectionString,
        ssl: {
            rejectUnauthorized: false
        },
        connectionTimeoutMillis: 10000,
        max: 10000
    }
}

const pool = new Pool(dbConfig)

log.info('Pool de clientes iniciado. ', dbConfig)

reintentaConexion();

// Add listeners
pool.on('error', (err, client) => {
    log.error('Error en base de datos. Detalles: ', err.stack)
    reintentaConexion();
})

exports.pool = pool