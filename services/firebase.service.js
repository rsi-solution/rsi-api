const admin = require("firebase-admin");
const serviceAccount = require("../adminsdk.json");
const {log} = require("./logger.service");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
});

/**
 *  Metodo encargado de enviar una notificacion push
 * a traves del servicio de Google Cloud Messaging.
 *
 * @param userToken - Token FCM del dispositivo
 * @param payload - Payload con informacion adicional (data) e informacion de la notificacion (notification)
 */
exports.enviaPushNotificationFCM = async (userToken, payload = {}) => {
  return admin
          .messaging()
          .sendToDevice(
            userToken,
            payload
          );
};