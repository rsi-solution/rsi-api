const aws = require('aws-sdk')
aws.config.update({
  region: process.env.AWS_REGION,
});
const ses = new aws.SES()

exports.enviaEmail = async (toEmails = [], sender, subject, content) => {
  const params = {
    Destination: {
      ToAddresses: toEmails,
    },
    Message: {
      Body: {
        Html: {
          Charset: "UTF-8",
          Data: content,
        },
      },
      Subject: {
        Charset: "UTF-8",
        Data: subject,
      },
    },
    Source: sender,
  };

  return ses.sendEmail(params).promise();
};

exports.verificaEmail = async (email) => {
  return ses
    .verifyEmailIdentity({
      EmailAddress: email,
    })
    .promise();
};
