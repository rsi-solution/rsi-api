const aws = require("aws-sdk");
aws.config.update({
  region: process.env.AWS_REGION,
});
const sns = new aws.SNS();

exports.suscribir = async (telefono) => {
  const params = {
    Protocol: "sms",
    TopicArn: process.env.SMS_SUBSCRIBE_TOPIC,
    Endpoint: telefono,
    ReturnSubscriptionArn: false,
  };

  return sns.subscribe(params).promise();
};

exports.confirmarSuscripcion = async (token) => {
  const params = {
    Token: token,
    TopicArn: process.env.SMS_SUBSCRIBE_TOPIC,
  };

  return sns.confirmSubscription(params).promise();
};
