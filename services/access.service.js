const {log} = require('./logger.service')

exports.nuevoAcceso = (req, res) => {
    log.info(`IP: ${ req.socket.remoteAddress } - ${ req.baseUrl }${ req.url } - Status ${ res.statusCode } @ ${ new Date() }`)
}