const SimpleLogger = require('simple-node-logger')

const manager = require('simple-node-logger').createLogManager()

const log = manager.createLogger('rsi')
log.setLevel('all')

exports.log = log