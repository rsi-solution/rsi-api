const UtilModule = require('../model/modules/util.module')
const {log} = require('./logger.service')

exports.nuevoIncidente = async (codigo, origen, detallesObjeto, nivel = 'error') => {
    log.log(nivel, `#${ codigo } @ ${ origen }
    ${ detallesObjeto }`)

    // Persiste en base de datos
    const entidadIncidente = {
        modulo: origen,
        origen: origen,
        code: codigo,
        mensaje: 'Error',
        stacktrace: detallesObjeto
    };

    const modulo = new UtilModule();
    await modulo.postIncidencia(entidadIncidente);
}