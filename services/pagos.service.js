const UtilModule = require('../model/modules/util.module');
const MaquinasModule = require('../model/modules/maquinas.module');
const PromocionesModule = require('../model/modules/promociones.module');
const incidentService = require("./incident.service");
const { RespuestaApiEntidad } = require('../shared/interfaces/respuesta-api-entidad.interface');
const {RespuestaApi} = require('../shared/interfaces/respuesta-api.interface');
const UsuariosModule = require('../model/modules/usuarios.module');
const {log} = require('../services/logger.service');

const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY, {
  apiVersion: '2022-08-01',
});

exports.listPaymentMethods = async(customerKey) => {
  const list =  await stripe.customers.listPaymentMethods(
    customerKey,
    {type: 'card'}
  );

  return list.data;
}

exports.retrievePaymentMethod = async (idPaymentMethod) => {
  return await stripe.paymentMethods.retrieve(
    idPaymentMethod
  );
}

/**
 * Método para actualizar la información de un método de pago
 * NOTA: No permite cambiar el número de tarjeta, solo fecha de vencimiento
 * 
 * @param {*} idPaymentMethod id del metodo de pago a cambiar
 * @param {*} body body con informacion a cambiar (exp_month o/y exp_year) 
 * @returns 
 */
exports.updatePaymentMethod = async (idPaymentMethod, body) => {
  let resultado = new RespuestaApi();

  log.info(`Updating payment method ${idPaymentMethod}`);

  if(idPaymentMethod && body){
    try{
      const result = await stripe.paymentMethods.update(
        idPaymentMethod,
        { card : {
            exp_month: body.exp_month,
            exp_year: body.exp_year
          }
        }
      );
    }catch(err){
      resultado = procesaEstatusPago({
        status: 'failed',
        failure_code: err.raw.code
      })
    }  
  } else {
    resultado.estatus = 'ERROR';
    resultado.mensaje = 'No se ha podido guardar el método de pago, contacta a soporte'; 
  }

  return resultado;
}

exports.detachPaymentMethod = async (idPaymentMethod) => {
  log.info(`Detaching payment method ${idPaymentMethod}`);
  return await stripe.paymentMethods.detach( idPaymentMethod );
}

exports.postMetodoPago = async (customer, token) => {
  let resultado = new RespuestaApi();
  const correlation = Math.floor(Math.random() * 9999);
  
  log.info(`Creando metodo pago para customer ${customer} - ${token} - correlation ${correlation}`);

  if(customer && token){
    const metodoPago = await generaMetodoPago(token);

    log.info(`Metodo generado en stripe, adjuntando a customer, correlation ${correlation}`);

    const result =  await attachPaymentMethodToCustomer(metodoPago, customer);

    if(result.error){
      log.error(`Error al adjuntar metodo de pago a customer, correlation ${correlation} ${result.error}`);
      resultado.estatus = 'ERROR';
      resultado.mensaje = 'No se ha podido guardar el método de pago, contacta a soporte';  
    }
  } else {
    log.error(`Error al crear metodo pago, no hay argumentos suficientes, correlation ${correlation} ${result.error}`);
    resultado.estatus = 'ERROR';
    resultado.mensaje = 'No se ha podido guardar el método de pago, contacta a soporte';
  }

  return resultado;

}

exports.createCustomer = async ( email, nombre_completo, telefono ) => {
    return await stripe.customers.create({
      email: email,
      name: nombre_completo,
      phone: telefono
  })
}

exports.calculaPago = async (entidadRenta) => {

  let resultado = new RespuestaApiEntidad();
  const moduloUtil = new UtilModule();
  const correlation = Math.floor(Math.random() * 9999);

  try {

    log.info(`Calculando pago, correlation ${correlation} ${JSON.stringify(entidadRenta)}`)

    resultado.entidad = [];

    let importeXPeriodo = 0.0;
    let importePromociones = 0.0;
    let importeImpuesto = 0.0; // FIXME: Obtener impuesto/tax desde stripe
    let importeTotal = 0.0;

    // Calcula importe por el periodo seleccionado
    importeXPeriodo = await calculaImporteXPeriodo(entidadRenta.ID_MAQUINA, entidadRenta.DURACION);

    // Calcula importe de promociones
    try {
      importePromociones = await calculaImportePromociones(importeXPeriodo, entidadRenta.CODIGOS_PROMOCION);

      // Informar que se han aplicado promociones
      if (importePromociones < 0.0)
        resultado.mensaje = `Promoción aplicada ($ ${importePromociones})`;

    } catch (error) {
      resultado.estatus = 'ERROR_PROMO';
      resultado.mensaje = error.message;
    }
    

    // Calcula importe final
    importeTotal = Math.round(importeXPeriodo + importePromociones + importeImpuesto);
    log.info(`Pago calculado, correlation ${correlation} importe total $${importeTotal}`);

    // Agrega entidad de pago
    resultado.entidad.push({
      fecha: await moduloUtil.getTimestampFormateado(),
      tipo_pago: 'DE',
      concepto: 'RENTA DE MAQUINARIA',
      importe: importeTotal,
      naturaleza: 'C',
      cantidad: 1,
      estatus: 'PA'
    });

  } catch (err) {

    incidentService.nuevoIncidente(
      -101000,
      "pagos.service.js - calculaPago",
      `${err}
      ${JSON.stringify(entidadRenta)} ${correlation}`
      );

    resultado.estatus = 'ERROR';
    resultado.mensaje = 'Ha ocurrido un error desconocido al calcular el pago. Por favor contacta a soporte.';

  }

  return resultado;
};

exports.pagaStripe = async (token, idCustomer, idPaymentMethod, entidadRenta) => {
  let resultado = new RespuestaApiEntidad();
  const moduloUsuario = new UsuariosModule();
  const correlation = Math.floor(Math.random() * 9999);

  log.info(`Inicia pago stripe, correlation ${correlation} token ${token} customer ${idCustomer} id usuario ${entidadRenta.USUARIO_ID}`)

  const { email } = await moduloUsuario.getUsuarioXId(entidadRenta.USUARIO_ID);

  const entidadPago = entidadRenta.PAGOS[0];

  // Al no pasar ID metodo de pago, generar y adjuntar nuevo metodo de pago
  if (!idPaymentMethod) {
    const idMetodoPago = await generaMetodoPago(token);
    const metodoPagoAttached = await attachPaymentMethodToCustomer(idMetodoPago, idCustomer);
    idPaymentMethod = metodoPagoAttached.id;
  }

  log.info(`payment method id ${idPaymentMethod} correlation ${correlation}`);

  const paymentIntent = await generaIntencionPago(idCustomer, idPaymentMethod, entidadPago, `Renta de maquinaria`, email);

  if (paymentIntent.id && paymentIntent.client_secret) {

    log.info(`Payment exitoso correlation ${correlation}`);

    resultado.entidad = {
      id: paymentIntent.id,
      client_secret: paymentIntent.client_secret
    };

  } else {
    log.error(`Error en payment, los parametros no se regresaron completos desde Stripe, correlation ${correlation}`);
    resultado.estatus = 'ERROR';
    resultado.mensaje = 'No se ha podido concluir con el pago, contacta a soporte';
  }

  return resultado;
}

exports.confirmaPagoStripe = async (id, entidad) => {
  let resultado = new RespuestaApi();
  let moduloMaquina = new MaquinasModule();
  let moduloPromociones = new PromocionesModule();
  const correlation = Math.floor(Math.random() * 9999);

  log.info(`Confirma pago stripe, correlation ${correlation} payment intent ${id}`);

  entidad.PAYMENT_INTENT = id; // Complementa entidad
  resultado = await iniciaRentaYMataPromos(entidad, moduloMaquina, moduloPromociones, correlation);

    // Verifica si se cancelaron correctamente las promos
  if (resultado.estatus === 'OK') {

    // Consulta pago a Stripe
    const pagoStripe = await obtenerPagoStripe(id);

    resultado = procesaEstatusPago(pagoStripe.charges.data[0]);
    log.info(`Resultado confirmacion pago stripe ${JSON.stringify(resultado)} correlation ${correlation}`);
    
    // Verifica si se impacto correctamente 
    if (resultado.estatus === 'OK') {
      // Confirma post de renta y actualizaciones de promos
      await moduloMaquina.commit();
    } else {
      await moduloMaquina.rollback();
    }
    
  } else {
      await moduloPromociones.rollback();
  }

  return resultado;
}

exports.confirmaPagoPaypal = async (entidad) => {
  let resultado = new RespuestaApi();
  let moduloMaquina = new MaquinasModule();
  let moduloPromociones = new PromocionesModule();
  const correlation = Math.floor(Math.random() * 9999);

  log.info(`Inicia confirmacion pago paypal, correlation ${correlation}`);  

  resultado = await iniciaRentaYMataPromos(entidad, moduloMaquina, moduloPromociones, correlation);

    // Verifica si se cancelaron correctamente las promos
  if (resultado.estatus === 'OK') {
    await moduloMaquina.commit();
    resultado.mensaje = 'Pago exitoso y renta agendada.';
    log.info(`Pago exitoso paypal ${correlation}`);
  } else {
    await moduloMaquina.rollback();
    log.info(`Error en pago por paypal ${correlation}`);
  }

  return resultado;
}

/**
 *  Metodo que adjunta en Stripe el metodo de pago con el customer
 * 
 * @param {string} idMetodoPago ID del metodo de pago
 * @param {string} idCustomer ID del customer
 * @returns 
 */
const attachPaymentMethodToCustomer = async (idMetodoPago, idCustomer) => {
  return await stripe.paymentMethods.attach(
    idMetodoPago,
    {customer: idCustomer}
  );
}

/**
 *  Metodo encargado de persistir la Renta y cancelar
 * promociones, si es que fueron usadas.
 * 
 * @param {object} entidad Entidad de MaquinaRenta
 * @param {MaquinasModule} moduloMaquina Modulo
 * @param {PromocionesModule} moduloPromociones Modulo
 * @returns ResultadoAPI, si todo bien, estatus = OK
 */
const iniciaRentaYMataPromos = async (entidad, moduloMaquina, moduloPromociones, correlation = "") => {
  let resultado = new RespuestaApi();
  let confirmacionPersistencia = new RespuestaApiEntidad();

  log.info(`Iniciando renta, correlation ${correlation} maquina ${entidad.ID_MAQUINA} usuario ${entidad.USUARIO_ID}`);

  confirmacionPersistencia = await moduloMaquina.postRenta(entidad, false);

  if (confirmacionPersistencia.estatus === 'OK') {

    if (entidad.CODIGOS_PROMOCION && entidad.CODIGOS_PROMOCION.length > 0) {

      // Actualiza promociones a usadas
      for (let index = 0; index < entidad.CODIGOS_PROMOCION.length; index++) {
        const codigoPromo = entidad.CODIGOS_PROMOCION[index];
        
        log.info(`Cancelando promocion ${codigoPromo} correlation ${correlation}`);

        const resultadoCancelaPromo = await moduloPromociones.cancelaPromocion(codigoPromo, confirmacionPersistencia.entidad.ID, false);

        if (resultadoCancelaPromo.estatus === 'ERROR') {
          resultado.estatus = 'ERROR';
          resultado.mensaje = resultadoCancelaPromo.mensaje;
        }

      }
    }
  } else {
    resultado.estatus = 'ERROR';
    resultado.mensaje = confirmacionPersistencia.mensaje;
  }

  return resultado;
}

const procesaEstatusPago = (chargeData) => {
  const status = chargeData.status;
  const failureCode = chargeData.failure_code;
  let resultado = new RespuestaApi();
  
  if (status === 'failed') {

    resultado.estatus = 'ERROR';

    if (failureCode === 'account_country_invalid_address')
      resultado.mensaje = 'El pais del negocio no corresponde con el pais de la cuenta.';
    else if (failureCode === 'account_error_country_change_requires_additional_steps')
      resultado.mensaje = 'Se requieren pasos adicionales para el cargo. Por favor contacte a soporte.';
    else if (failureCode === 'account_invalid')
      resultado.mensaje = 'La cuenta es invalida. Por favor contacte a soporte.';
    else if (failureCode === 'account_number_invalid')
      resultado.mensaje = 'La el numero de cuenta es invalido. Por favor contacte a soporte.';
    else if (failureCode === 'amount_too_large')
      resultado.mensaje = 'El monto de cobro es demasiado grande. Por favor contacte a soporte para otras formas de pagar.';
    else if (failureCode === 'amount_too_small')
      resultado.mensaje = 'El monto de cobro es demasiado pequeño. Por favor contacte a soporte para otras formas de pagar.';
    else if (failureCode === 'api_key_expired')
      resultado.mensaje = 'El API key se encuentra expirado. Por favor contacte a soporte para otras formas de pagar.';
    else if (failureCode === 'authentication_required')
      resultado.mensaje = 'Se requiere autenticacion. Por favor contacte a soporte para otras formas de pagar.';
    else if (failureCode === 'bank_account_declined')
      resultado.mensaje = 'El banco ha rechazado el pago.';
    else if (failureCode === 'card_decline_rate_limit_exceeded')
      resultado.mensaje = 'El limite de intentos de pago con esta tarjeta se ha excedido. Por favor espere 24hrs. para volver a intentar.';
    else if (failureCode === 'card_declined')
      resultado.mensaje = 'La tarjeta ha sido rechazada.';
    else if (failureCode === 'charge_already_refunded')
      resultado.mensaje = 'La pago ya ha sido devuelto anteriormente.';
    else if (failureCode === 'coupon_expired')
      resultado.mensaje = 'El cupon se encuentra expirado.';
    else if (failureCode === 'email_invalid')
      resultado.mensaje = 'Su email es valido, por favor contacte a soporte.';
    else if (failureCode === 'expired_card')
      resultado.mensaje = 'La tarjeta se encuentra expirada, intente con otra.';
    else if (failureCode === 'incorrect_cvc' || failureCode === 'invalid_cvc')
      resultado.mensaje = 'El CVC es incorrecto, por favor verifiquelo.';
    else if (failureCode === 'incorrect_number' || failureCode === 'invalid_number')
      resultado.mensaje = 'El numero de tarjeta es incorrecto, por favor verifiquelo.';
    else if (failureCode === 'insufficient_funds')
      resultado.mensaje = 'La tarjeta no cuenta con fondos suficientes para el pago.';
    else if (failureCode === 'invalid_card_type')
      resultado.mensaje = 'La tarjeta capturada no es valida para pagos.';
    else if (failureCode === 'invalid_charge_amount')
      resultado.mensaje = 'El monto a pagar es invalido, por favor contacte a soporte.';
    else if (failureCode === 'invalid_expiry_month' || failureCode === 'invalid_expiry_year')
      resultado.mensaje = 'La fecha de vencimiento no es valida, por favor verifiquela.';
    else if (failureCode === 'no_account')
      resultado.mensaje = 'La cuenta de banco no se ha localizado. Por favor contacte a soporte.';
    else if (failureCode === 'payment_intent_action_required')
      resultado.mensaje = 'La intencion de pago requiere una accion. Por favor intentelo nuevamente.';
    else if (failureCode === 'payment_intent_authentication_failure')
      resultado.mensaje = 'La autenticacion ha fallado. Por favor intentelo nuevamente.';
    else if (failureCode === 'payment_intent_payment_attempt_expired')
      resultado.mensaje = 'El tiempo de espera se ha agotado. Por favor intentelo nuevamente.';
    else if (failureCode === 'payment_intent_payment_attempt_expired')
      resultado.mensaje = 'El tiempo de espera se ha agotado. Por favor intentelo nuevamente.';
    else if (failureCode === 'payment_method_bank_account_blocked')
      resultado.mensaje = 'Ha ocurrido un error con nuestras cuentas. Por favor contacte a soporte.';
    else if (failureCode === 'processing_error')
      resultado.mensaje = 'Ha ocurrido un error mientras procesabamos tu tarjeta, intenta de nuevo con otra tarjeta.';
    else if (failureCode === 'refer_to_customer')
      resultado.mensaje = 'Detectamos que tu pago ha sido detenido. Por favor contacta a soporte';
    else if (failureCode === 'return_intent_already_processed')
      resultado.mensaje = 'Este pago ya ha sido procesado anteriormente.';
    else if (failureCode === 'url_invalid')
      resultado.mensaje = 'Pago invalido. Por favor intenta nuevamente o contacta a soporte.';
    else
      resultado.mensaje = 'Ha ocurrido un error desconocido. Por favor intenta nuevamente o contacta a soporte.';

    incidentService.nuevoIncidente(
      11000,
      "pagos.service.js - procesaEstatusPago",
      `Ha ocurrido un error al procesar un pago. Datos del cargo Stripe: 
      ${ JSON.stringify(chargeData) }`
    );

  } else if (status === 'succeeded') {
    resultado.estatus = 'OK';
    resultado.mensaje = 'Pago exitoso y renta agendada.';
  }

  return resultado;
}

const obtenerPagoStripe = async (id) => {
  return await stripe.paymentIntents.retrieve(
    id
  );
}

const confirmaPagoStripe = async (id) => {
  return await stripe.paymentIntents.confirm(
    id
  );
}

const generaIntencionPago = async (customer, payment_method, entidadPago, descripcion, email) => {
  return await stripe.paymentIntents.create({
    amount: parseFloat(entidadPago.importe) * 100,
    currency: 'MXN',
    payment_method_types: ['card'],
    payment_method: payment_method,
    customer: customer,
    description: descripcion,
    receipt_email: email
  });
}

/**
 * 
 * @param {String} token Opcional, token de la captura de datos bancarios, esto es para cuando no se selecciona metodo de pago
 * @param {String} idMetodoPago Opcional, ID del metodo de pago, esto es para cuando seleccionan un metodo de pago
 * @returns ID del metodo de pago
 */
const generaMetodoPago = async (token) => {

  const metodoPagoObject = await stripe.paymentMethods.create({
    type: 'card',
    card: { token }
  });

  return metodoPagoObject.id;
}

const calculaImporteXPeriodo = async (id_maquina, duracion) => {
  const moduloMaquinas = new MaquinasModule();
  let importeXPeriodo = 0.0;
  const entidadMaquinaArray = await moduloMaquinas.getMaquinas(id_maquina, 'AC', true, 1, true);
  const entidadMaquina = (entidadMaquinaArray && entidadMaquinaArray.length > 0) ? entidadMaquinaArray[0] : null;

  if (entidadMaquina) {
  
    if (duracion === '1 day')          importeXPeriodo = (entidadMaquina.COSTO_X_DIA) ? parseFloat(entidadMaquina.COSTO_X_DIA) : -99.0;
    else if (duracion === '1 week')    importeXPeriodo = (entidadMaquina.COSTO_X_SEMANA) ? parseFloat(entidadMaquina.COSTO_X_SEMANA) : -99.0;
    else if (duracion === '1 month')   importeXPeriodo = (entidadMaquina.COSTO_X_MES) ? parseFloat(entidadMaquina.COSTO_X_MES) : -99.0;
    else if (duracion === '14 days')   importeXPeriodo = (entidadMaquina.COSTO_X_CATORCENA) ? parseFloat(entidadMaquina.COSTO_X_CATORCENA) : -99.0;
    else if (duracion === '1 weekend')   importeXPeriodo = (entidadMaquina.COSTO_X_FIN_SEMANA) ? parseFloat(entidadMaquina.COSTO_X_FIN_SEMANA) : -99.0;
    else importeXPeriodo = -99.0;

    if (importeXPeriodo === -99.0) 
      throw new Error('Esta maquina no cuenta con su importe configurado para la duracion, cambie la duracion de la renta o contacte a soporte.');

  } else {
    throw new Error('No se ha encontrado la maquina con el ID ' + entidadRenta.ID_MAQUINA);
  }

  return importeXPeriodo;
}

const calculaImportePromociones = async (importeXPeriodo = 0.0, arrayCodigosPromociones = []) => {
  let resultado = 0.0;
  const moduloPromos = new PromocionesModule();

  if (arrayCodigosPromociones && arrayCodigosPromociones.length > 0) {

    // Recorrer y consultar la entidad de promocion
    for (let index = 0; index < arrayCodigosPromociones.length; index++) {

      const codigoPromo = arrayCodigosPromociones[index];
      const entidadPromo = await moduloPromos.getPromocion(codigoPromo, true);

      // Calcula importe de promocion dado su tipo
      if (entidadPromo) {
        const tipo = entidadPromo.TIPO;
        const valor1 = entidadPromo.VALOR1;

        // Descuento de porcentaje sobre la renta total
        if (tipo === 'TO') resultado -= importeXPeriodo * ( parseFloat(valor1) / 100 );
        else resultado -= parseFloat(valor1);

      } else {
        throw new Error('La promoción ya no aplica o no existe.');
      }
    }
  }

  return resultado;
}